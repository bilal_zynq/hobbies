<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Badge Requests</h4>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Badge Title</th>
                                    <th>Badge Image</th>
                                    <th>Status</th>
                                    <?php if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value->BadgeRequestID;?>">
                                            <td><?php echo $value->FullName; ?></td>
                                            <td><?php echo $value->Title; ?></td>
                                            <td><img src="<?php echo base_url(); ?>/<?php echo $value->Image; ?>" style="width: 40px !important;"></td>
                                            <td><?php echo $value->Status; ?></td>
                                             <?php if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanDelete')){?> 
                                            <td>
                                                <?php if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                    <a href="<?php echo base_url('cms/'.$ControllerName.'/view/'.$value->BadgeRequestID);?>" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                               
                                                <?php if(checkUserRightAccess(51,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->BadgeRequestID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>