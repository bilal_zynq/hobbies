<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Badge Request Details</h4>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="BadgeRequestID"
                                   value="<?php echo $result[0]->BadgeRequestID; ?>">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="FullName">User Name</label>
                                        <?php echo $result[0]->FullName; ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Status">Request Status</label>
                                        <select id="Status" class="selectpicker" data-style="select-with-transition" required
                                                name="Status">
                                            <?php
                                            if ($result[0]->Status == 'Pending')
                                            { ?>
                                                <option value="Pending"  <?php echo($result[0]->Status == 'Pending' ? 'selected' : ''); ?> disabled>Pending</option>
                                            <?php }
                                            ?>
                                            <option value="Approved" <?php echo($result[0]->Status == 'Approved' ? 'selected' : ''); ?>>
                                                Approved
                                            </option>
                                            <option value="Rejected" <?php echo($result[0]->Status == 'Rejected' ? 'selected' : ''); ?>>
                                                Rejected
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title">Badge Title</label>
                                        <?php echo $result[0]->Title; ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Image">Badge Image</label>
                                        <td><img src="<?php echo base_url(); ?>/<?php echo $result[0]->Image; ?>" style="width: 50px !important;"></td>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description">Badge Description
                                            (Requirements)</label>
                                        <?php echo $result[0]->Description; ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if ($attachments)
                            { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Attachments">Attachments</label>
                                        <br>
                                        <?php
                                        foreach ($attachments as $attachment) { ?>
                                            <div class="col-md-2">
                                                <a data-fancybox="gallery"
                                                   href="<?php echo base_url() . '/' . $attachment->Attachment; ?>"><img
                                                            src="<?php echo base_url() . '/' . $attachment->Attachment; ?>">
                                                </a>
                                            </div>
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>
                    <?php }
                    ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                            <?php echo lang('submit'); ?>
                                        </button>
                                        <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>">
                                            <button type="button" class="btn btn-default waves-effect m-l-5">
                                                <?php echo lang('back'); ?>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            <!-- end content-->
        </div>
        <!--  end card  -->
    </div>
    <!-- end col-md-12 -->
</div>
<!-- end row -->
</div>
</div>