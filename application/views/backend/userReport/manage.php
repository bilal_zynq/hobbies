<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">User Reports</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="FilterType">Filter User By</label>
                                    <select id="FilterType" class="selectpicker" data-style="select-with-transition" name="FilterType" onchange="searchFields(this.value);">
                                        <option value="0" selected disabled>Please select filter type</option>
                                        <option value="1">City</option>
                                        <option value="2">Search by name</option>
                                        <option value="3">Search by email</option>
                                        <option value="4">Search by date joined</option>
                                        <option value="5">Type of user accounts</option>
                                        <option value="6">Users with no. of badges</option>
                                        <option value="7">Suspended Users</option>
                                        <option value="8">Most active users (by posts)</option>
                                        <option value="9">Most active users (by comments)</option>
                                        <option value="10">Number of groups joined (more to less)</option>
                                        <option value="11">Number of groups joined (less to more)</option>
                                        <option value="12">Number of groups created (more to less)</option>
                                        <option value="13">Number of groups created (less to more)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 city_field" style="display: none;">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="CityDD">City</label>
                                    <select id="CityDD" class="selectpicker" data-style="select-with-transition">
                                        <?php
                                        foreach ($cities as $city)
                                        { ?>
                                            <option value="<?php echo $city->CityID; ?>"><?php echo $city->Title; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 search_by_name_field" style="display: none;">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group label-floating">
                                            <label for="Name" class="control-label">Name</label>
                                            <input type="text" id="Name" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 search_by_email_field" style="display: none;">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group label-floating">
                                            <label for="Email" class="control-label">Email</label>
                                            <input type="text" id="Email" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 search_by_date_joined_field" style="display: none;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label for="date_joined" class="control-label">Date Joined</label>
                                            <input type="text" id="date_joined" class="form-control custom_datepicker" value="<?php echo date('d-m-Y'); ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 search_by_user_account" style="display: none;">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="AccountType">Account Type</label>
                                    <select id="AccountType" class="selectpicker" data-style="select-with-transition">
                                        <?php
                                        foreach ($account_types as $account_type)
                                        { ?>
                                            <option value="<?php echo $account_type->TypeID; ?>"><?php echo $account_type->Title; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 search_by_badge" style="display: none;">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="BadgeType">Badge Type</label>
                                    <select id="BadgeType" class="selectpicker" data-style="select-with-transition">
                                        <?php
                                        foreach ($badges as $badge)
                                        { ?>
                                            <option value="<?php echo $badge->BadgeID; ?>"><?php echo $badge->Title; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 no_of_members_field" style="display: none;">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="NoOfMembers">Max no. of members in group</label>
                                    <select id="NoOfMembers" class="selectpicker" data-style="select-with-transition">
                                        <option value="10">10</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="200">200</option>
                                        <option value="500">500</option>
                                        <option value="1000">1000</option>
                                        <option value="10000">10000</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 groups_created_btw_specific_period" style="display: none;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label for="from_date" class="control-label">From Date</label>
                                            <input type="text" id="from_date" class="form-control custom_datepicker" value="<?php echo date('d-m-Y'); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label for="to_date" class="control-label">To Date</label>
                                            <input type="text" id="to_date" class="form-control custom_datepicker" value="<?php echo date('d-m-Y'); ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 groups_created_on_specific_date" style="display: none;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label for="date" class="control-label">Date</label>
                                            <input type="text" id="date" class="form-control custom_datepicker" value="<?php echo date('d-m-Y'); ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary waves-effect waves-light" type="button" onclick="getResults();"><?php echo lang('submit');?></button>
                            </div>
                        </div>
                        <div class="material-datatables my-table">
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
<script>
    function searchFields(val) {
        $( "#error-message" ).hide();
        $('.city_field').hide();
        $('.search_by_name_field').hide();
        $('.search_by_email_field').hide();
        $('.search_by_date_joined_field').hide();
        $('.search_by_user_account').hide();
        $('.search_by_badge').hide();

        if (val == 1)
        {
            $('.city_field').show();
        }
        if (val == 2)
        {
            $('.search_by_name_field').show();
        }
        if (val == 3)
        {
            $('.search_by_email_field').show();
        }
        if (val == 4)
        {
            $('.search_by_date_joined_field').show();
        }
        if (val == 5)
        {
            $('.search_by_user_account').show();
        }
    }

    function getResults() {
        var filter_type = $('#FilterType').val();
        if (filter_type > 0)
        {
            $.blockUI({
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });

            var city_id = $('#CityDD').val();
            var name = $('#Name').val();
            var email = $('#Email').val();
            var date_joined = $('#date_joined').val();
            var account_type = $('#AccountType').val();
            //var badge_type = $('#BadgeType').val();

            $.ajax({
                type: "POST",
                url: base_url + 'cms/UserReport/filterUsers',
                data: {
                    'filter_type': filter_type,
                    'city_id': city_id,
                    'name': name,
                    'email': email,
                    'date_joined': date_joined,
                    'account_type': account_type,
                },
                success: function (result) {
                    $('.my-table').html(result);
                    $( "#error-message" ).fadeOut( 10000, function() {
                        $( "#error-message" ).hide();
                    });
                },
                complete: function () {
                    $.unblockUI();
                }
            });
        }else{
            alert('Please first select group filter type')
        }
    }
</script>