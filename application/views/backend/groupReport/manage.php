<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Group Reports</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="FilterType">Filter Group By</label>
                                    <select id="FilterType" class="selectpicker" data-style="select-with-transition" name="FilterType" onchange="searchFields(this.value);">
                                        <option value="0" selected disabled>Please select filter type</option>
                                        <option value="1">City</option>
                                        <option value="2">No. of members</option>
                                        <option value="3">Sort recently active to older</option>
                                        <option value="4">Sort older to recently active</option>
                                        <option value="5">Groups creating b/w specific period</option>
                                        <option value="6">Groups created on specific date</option>
                                        <option value="7">Suspended groups</option>
                                        <option value="8">Reported groups</option>
                                        <option value="9">Number of admins</option>
                                        <option value="10">Top active groups</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 city_field" style="display: none;">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="CityDD">City</label>
                                    <select id="CityDD" class="selectpicker" data-style="select-with-transition">
                                        <?php
                                        foreach ($cities as $city)
                                        { ?>
                                            <option value="<?php echo $city->CityID; ?>"><?php echo $city->Title; ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 no_of_members_field" style="display: none;">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="NoOfMembers">Max no. of members in group</label>
                                    <select id="NoOfMembers" class="selectpicker" data-style="select-with-transition">
                                        <option value="10">10</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="200">200</option>
                                        <option value="500">500</option>
                                        <option value="1000">1000</option>
                                        <option value="10000">10000</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 groups_created_btw_specific_period" style="display: none;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label for="from_date" class="control-label">From Date</label>
                                            <input type="text" id="from_date" class="form-control custom_datepicker" value="<?php echo date('d-m-Y'); ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label for="to_date" class="control-label">To Date</label>
                                            <input type="text" id="to_date" class="form-control custom_datepicker" value="<?php echo date('d-m-Y'); ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 groups_created_on_specific_date" style="display: none;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label for="date" class="control-label">Date</label>
                                            <input type="text" id="date" class="form-control custom_datepicker" value="<?php echo date('d-m-Y'); ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary waves-effect waves-light" type="button" onclick="getResults();"><?php echo lang('submit');?></button>
                            </div>
                        </div>
                        <div class="material-datatables my-table">
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
<script>
function searchFields(val) {
    $( "#error-message" ).hide();
    $('.city_field').hide();
    $('.no_of_members_field').hide();
    $('.groups_created_btw_specific_period').hide();
    $('.groups_created_on_specific_date').hide();

    if (val == 1)
    {
        $('.city_field').show();
    }
    if (val == 2)
    {
        $('.no_of_members_field').show();
    }
    if (val == 5)
    {
        $('.groups_created_btw_specific_period').show();
    }
    if (val == 6)
    {
        $('.groups_created_on_specific_date').show();
    }
}

function getResults() {
    var filter_type = $('#FilterType').val();
    if (filter_type > 0)
    {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        var city_id = $('#CityDD').val();
        var no_of_members = $('#NoOfMembers').val();
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        var date = $('#date').val();

        $.ajax({
            type: "POST",
            url: base_url + 'cms/GroupReport/filterGroups',
            data: {
                'filter_type': filter_type,
                'city_id': city_id,
                'no_of_members': no_of_members,
                'from_date': from_date,
                'to_date': to_date,
                'date': date,
            },
            success: function (result) {
                $('.my-table').html(result);
                $( "#error-message" ).fadeOut( 8000, function() {
                    $( "#error-message" ).hide();
                });
            },
            complete: function () {
                $.unblockUI();
            }
        });
    }else{
        alert('Please first select group filter type')
    }
}
</script>