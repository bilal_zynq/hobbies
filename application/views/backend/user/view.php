<?php
$option = '';
if(!empty($roles)){ 
    foreach($roles as $role){ 
        $option .= '<option value="'.$role->RoleID.'" '.((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '').'>'.$role->Title.' </option>';
    } }
$option2 = '';
if(!empty($activities)){ 
    foreach($activities as $activity){ 
        $option2 .= '<option value="'.$activity->ActivityID.'" '.((isset($result[0]->ActivityID) && $result[0]->ActivityID == $activity->ActivityID) ? 'selected' : '').'>'.$activity->Title.' </option>';
    } }     
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        if($key == 0){
         $common_fields = '<div class="row"><div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="RoleID'.$key.'">'.lang('choose_user_role').'</label>
                                                                <select id="RoleID'.$key.'" class="form-control" required="" disabled>
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div><div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="ActivityID'.$key.'">'.lang('choose_activity').'</label>
                                                                <select id="ActivityID'.$key.'" class="form-control" required="" disabled>
                                                                    '.$option2.'
                                                                </select>
                                                            </div>
                                                        </div></div>';
        $common_fields2 = ' <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Email">'.lang('email').'<span class="text-danger">*</span></label>
                                                                <input type="text" disabled  parsley-trigger="change" required  class="form-control" id="Email" value="'.((isset($result[0]->Email)) ? $result[0]->Email : '').'">
                                                            </div>
                                                        </div>'; 
        $common_fields3 = '<div class="row"><div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label class="control-label" for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div></div>';
       
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                     

                                                   
                                                    
                                                     '.$common_fields.'
                                                      <div class="row">
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Title'.$key.'">'.lang('name').'<span class="text-danger">*</span></label>
                                                                <input type="text" disabled parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                         '.$common_fields2.'
                                                    </div>
                                                    
                                                   '.$common_fields3.'
                                                    

                                                 

                                                


                        </div>';
        
        
        
        
        
    }
}


?>


<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Languages</h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
