<div class="content">
    <div class="container-fluid">


        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">people</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Users</p>
                        <h3 class="card-title"><?php echo $total_users; ?></h3>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="rose">
                        <i class="material-icons">equalizer</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Groups</p>
                        <h3 class="card-title"><?php echo $total_groups; ?></h3>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Posts</p>
                        <h3 class="card-title"><?php echo $total_posts; ?></h3>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-chart">
                    <div class="card-header" data-background-color="rose">
                        <div class="ct-chart" id="maxPostGroupChart"></div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title" style="text-align: center;">Top 10 Groups with Most Posts</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-chart">
                    <div class="card-header" data-background-color="green">
                        <div class="ct-chart" id="maxMembersGroupChart"></div>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title" style="text-align: center;">Top 10 Groups with Most Members</h4>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function () {
        var labelsArr = [];
        var valsArr = [];
        <?php
        $i = 0;
        foreach ($group_posts as $value)
        { ?>
        var str = "<?php echo $value['Title']; ?>";
        var res = str.replace("'", "\'");
        labelsArr.push(res);
        valsArr.push('<?php echo $value['group_posts_count']; ?>');
        <?php $i++; }
        ?>
        var dataWebsiteViewsChart = {
            labels: labelsArr,
            series: [
                valsArr
            ]
        };
        var optionsWebsiteViewsChart = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: 200,
            chartPadding: {top: 0, right: 5, bottom: 0, left: 0}
        };
        var responsiveOptions = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 10,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];
        var websiteViewsChart = Chartist.Bar('#maxPostGroupChart', dataWebsiteViewsChart, optionsWebsiteViewsChart, responsiveOptions);

        //start animation for the Emails Subscription Chart
        md.startAnimationForBarChart(websiteViewsChart);

        var labelsArr1 = [];
        var valsArr1 = [];
        <?php
        $i = 0;
        foreach ($group_members as $value)
        { ?>
        var str1 = "<?php echo $value['Title']; ?>";
        var res1 = str1.replace("'", "\'");
        labelsArr1.push(res1);
        valsArr1.push('<?php echo $value['group_members_count']; ?>');
        <?php $i++; }
        ?>
        var dataWebsiteViewsChart2 = {
            labels: labelsArr1,
            series: [
                valsArr1
            ]
        };
        var optionsWebsiteViewsChart2 = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: 200,
            chartPadding: {top: 0, right: 5, bottom: 0, left: 0}
        };
        var responsiveOptions2 = [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 10,
                axisX: {
                    labelInterpolationFnc: function (value) {
                        return value[0];
                    }
                }
            }]
        ];
        var websiteViewsChart2 = Chartist.Bar('#maxMembersGroupChart', dataWebsiteViewsChart2, optionsWebsiteViewsChart2, responsiveOptions2);

        //start animation for the Emails Subscription Chart
        md.startAnimationForBarChart(websiteViewsChart2);
    });
</script>