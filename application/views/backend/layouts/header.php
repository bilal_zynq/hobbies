<!doctype html>
<html lang="en">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130453120-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-130453120-1');
    </script>

    <meta charset="utf-8"/>
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/backend/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/backend/images/favicon.png" type="image/x-icon">
    <!-- App title -->
    <title><?php echo $site_setting->SiteName; ?></title>
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>assets/backend/plugins/summernote/summernote.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo base_url(); ?>assets/backend/css/material-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/demo.css" rel="stylesheet"/>
    <!--     Fonts and icons     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/font-awesome.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/backend/css/google-roboto-300-700.css" rel="stylesheet"/>
    <!--   Core JS Files   -->
    <script src="<?php echo base_url(); ?>assets/backend/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/material.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/perfect-scrollbar.jquery.min.js"
            type="text/javascript"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css"/>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js"></script>


</head>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var delete_msg = '<?php echo lang('are_you_sure');?>';
</script>
<style>
    .validate_error {
        border: 1px solid red;
    }

    #validation-msg {
        visibility: hidden;
        min-width: 250px;
        margin-left: -125px;
        text-align: center;
        border-radius: 2px;
        padding: 16px;
        position: fixed;
        z-index: 9999;
        left: 50%;
        top: 80px;
        font-size: 17px;
    }

    #validation-msg.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 4.5s;
        animation: fadein 0.5s, fadeout 0.5s 4.5s;
    }

    .panel-default{
        border: none !important;
    }

</style>
<body>
<!--<div class="alert <?php /*if($this->session->flashdata('message')){ echo 'alert-danger show'; } */ ?>" id="validation-msg">
                        <?php /*if($this->session->flashdata('message')){

                            echo $this->session->flashdata('message');


                        } 
                        */ ?>
     <?php /*if($this->session->flashdata('message')){ */ ?>
        <script>
        
        $( document ).ready(function() {
                 setTimeout(function() {
                            $('#validation-msg').removeClass('alert-danger show alert-success');
                        }, 4000);
        });
        
        </script>
        <?php /*} */ ?>
    </div>-->
<div class="wrapper">
        
  
  