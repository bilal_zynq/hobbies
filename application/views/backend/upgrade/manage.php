<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#pill1" data-toggle="tab" aria-expanded="true">Pending</a>
                            </li>
                            <li class="">
                                <a href="#pill2" data-toggle="tab" aria-expanded="false">Approved</a>
                            </li>
                            <li class="">
                                <a href="#pill3" data-toggle="tab" aria-expanded="false">Rejected</a>
                            </li>
                        </ul>
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <div class="toolbar">

                        </div>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active" id="pill1">
                                <h4 class="card-title">Pending</h4>
                                <table id="" class="datatable table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>UserName</th>
                                        <th>Status</th>

                                        <?php if(checkUserRightAccess(54,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                            <th><?php echo lang('actions');?></th>
                                        <?php } ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($pending_requests){
                                        foreach($pending_requests as $value){ ?>

                                            <tr id="<?php echo $value->AccountUpgradeRequestID; ?>">
                                                <td><a href="<?php echo base_url().'cms/user/view/'.base64_encode($value->UserID); ?>" target="_blank"><?php echo $value->FullName; ?></a></td>
                                                <td><?php echo $value->Status; ?></td>
                                                <?php if (checkUserRightAccess(54, $this->session->userdata['admin']['UserID'], 'CanEdit')) { ?>
                                                    <td>

                                                        <a href="javascript:void(0)" class="btn-icon edit approved_p" data-id="<?php echo $value->AccountUpgradeRequestID; ?>">
                                                            <button class="btn btn-sm btn-success">Approve</button>
                                                        </a>

                                                        <a href="javascript:void(0)" class="btn-icon edit rejected_p" data-id="<?php echo $value->AccountUpgradeRequestID; ?>">
                                                            <button class="btn btn-sm btn-danger">Reject</button>
                                                        </a>

                                                    </td>
                                                <?php } ?>
                                            </tr>
                                            <?php

                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>

                            <div class="material-datatables tab-pane" id="pill2">
                                <h4 class="card-title">Approved</h4>
                                <table id="" class="datatable table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>UserName</th>
                                        <th>Status</th>


                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($approved_requests){
                                        foreach($approved_requests as $value){ ?>

                                            <tr id="<?php echo $value->AccountUpgradeRequestID; ?>">
                                                <td><a href="<?php echo base_url().'cms/user/view/'.base64_encode($value->UserID); ?>" target="_blank"><?php echo $value->FullName; ?></a></td>
                                                <td><?php echo $value->Status; ?></td>


                                            </tr>
                                            <?php

                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>

                            <div class="material-datatables tab-pane" id="pill3">
                                <h4 class="card-title">Rejected</h4>
                                <table id="" class="datatable table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>

                                        <th>UserName</th>
                                        <th>Status</th>


                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($rejected_requests){
                                        foreach($rejected_requests as $value){ ?>

                                            <tr id="<?php echo $value->AccountUpgradeRequestID; ?>">
                                                <td><a href="<?php echo base_url().'cms/user/view/'.base64_encode($value->UserID); ?>" target="_blank"><?php echo $value->FullName; ?></a></td>
                                                <td><?php echo $value->Status; ?></td>


                                            </tr>
                                            <?php

                                        }

                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $(document).ready(function() {
        $('table.datatable').DataTable();
    } );
</script>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>

<script>

    $(document).ready(function() {




        $('.approved_p').on('click',function(){
            if (confirm("Are you sure you want to approve this request?")) {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });

                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/upgrade/action',
                    data: {
                        'id': $(this).attr('data-id'),
                        'form_type': 'approved_new'
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function (result) {

                        if (result.error != false) {
                            showError(result.error);
                        } else {
                            showSuccess(result.success);
                            $('#'+$(this).attr('data-id')).remove();
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }




                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
                return true;
            } else {
                return false;
            }

        });

        $('.rejected_p').on('click',function(){
            if (confirm("Are you sure you want to reject this request?")) {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });

                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/upgrade/action',
                    data: {
                        'id': $(this).attr('data-id'),
                        'form_type': 'rejected'
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function (result) {

                        if (result.error != false) {
                            showError(result.error);
                        } else {
                            showSuccess(result.success);
                            $('#'+$(this).attr('data-id')).remove();
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }




                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
                return true;
            } else {
                return false;
            }

        });

    });

</script>