<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
$images = '';
$videos = '';
if (!empty($site_images)) {
    
    $images = '<div class="row"><div class="col-md-12">';
foreach($site_images as $img){
    $images .= '<div class="col-md-2"><img src="'.base_url($img['Image']).'" width="100px" height="100px"></div>';
}
 $images .= '</div></div>';
    
}
if (!empty($site_videos)) {
    foreach($site_videos as $vkey=>$vid){
        $videos .= '<div class="row fieldGroup">
                    <div class="input-group col-md-12">
                        <div class="col-md-6">';
                        if($vkey == 0)
                        {
                            $videos .= '<label>Videos</label>';
                        }
                                                   
                        $videos .= '<input type="text" name="VideoURL[]" class="form-control" value="'.$vid['Video'].'" placeholder="video link" '.($vkey == 0 ? 'required' : '').'/>
                        </div>';
                        
                    $videos .= '</div>
                </div>';

    }
}
if(!empty($languages)){
    foreach($languages as $key => $language){

        $common_fields_af_comp_name = '';
        $common_fields_bff_title = '';
        $common_fields = '';
        $common_fields2 = '';
        $category_dropdown = '';
        $sub_category_dropdown = '';
        $country_dropdown = '';
        $state_dropdown = '';
        $city_dropdown = '';
        $common_no_fields = '';
        $common_check_fields = '';
        if($key == 0){
            
           
          
   
        $common_fields_af_comp_name = '<div class="row"><div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="UserName">Posted By</label>
                                        <input type="text"  required  class="form-control" id="UserName" value="'.$result['FullName'].'">
                                    </div>
                                </div></div>';
        $common_fields_bff_title = '<div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="ShortDescription">Post Text</label>
                                                                <textarea class="form-control" name="ShortDescription" id="ShortDescription">'.$result['PostText'].'</textarea>
                                                            </div>
                                                        </div>
                                                    </div>';

        
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                   
                                                    <div class="row">
                                                     
                                                        
                                                        '.$common_fields_af_comp_name.'
                                                    </div>
                                                   
                                                    <div class="row">
                                                        
                                                        '.$common_fields_bff_title.'
                                                        
                                                    </div>
                                                    
                                                        

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group m-b-20">
                                                                <label>Images</label><br>
                                                                <input type="file" name="Image[]" id="filer_input1" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        '.$images.'
                                                    </div>
                                                    '.$videos.'
                                                   
                                                   
                                                    
                                                   

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>


<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title"><?php echo lang('view');?></h5>
                    </div>
                    
                    <div class="card-content">
                        <div class="row">
                             <?php if(count($languages) > 1){?>
                            <div class="col-md-2">
                                
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                               
                            </div>
                            <div class="col-md-12">
                              <?php }else{ ?>
                                <div class="col-md-12">
                                  <?php } ?>
                                    <div class="tab-content">
                                        <?php echo $lang_data; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

