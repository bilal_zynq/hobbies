<?php
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
$images = '';
if (!empty($EmailFiles)) {

    $images .= '<div class="form-group clearfix">
       <div class="col-sm-12 padding-left-0 padding-right-0">
          <div class="jFiler jFiler-theme-dragdropbox">
             <div class="jFiler-items jFiler-row">
                <ul class="jFiler-items-list jFiler-items-grid">';
                    foreach($EmailFiles as $img){
                   $images .= '<li class="jFiler-item ArticleImage" data-jfiler-index="1" style="" id="img-'.$img['SiteImageID'].'">
                      <div class="jFiler-item-container">
                         <div class="jFiler-item-inner">
                            <div class="jFiler-item-thumb">
                               <div class="jFiler-item-status"></div>
                               <div class="jFiler-item-info">                                                              </div>
                               <div class="jFiler-item-thumb-image"><img src="' . (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png')) . '" draggable="false" style="width: 30% !important; height: 30% !important;"></div>
                            </div>
                            <div class="jFiler-item-assets jFiler-row">
                               <ul class="list-inline pull-left">
                                  <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                               </ul>
                               <ul class="list-inline pull-right">
                                  <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-id="'. $img['SiteImageID'] .'" data-image-path="'. $img['ImageName'] .'"></a></li>
                               </ul>
                            </div>
                         </div>
                      </div>
                   </li>';
                     } 
                   
$images .= '</ul>
             </div>
          </div>
       </div>
    </div>';

    
}
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        if ($key == 0) {
            $common_fields = '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label class="control-label" for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>';
            $common_fields2 = '<div class="row">
                                        <div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating">
                                        <legend>'.lang('Image').'</legend>
                                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail">
                                                <img src="../../assets/img/image_placeholder.jpg" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                            <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="EmailImage[]" multiple="multiple"/>
                                                    </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                    <div class="row m-t-10 m-b-10">
                                        '.$images.'
                                    </div>';
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                    
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title' . $key . '">' . lang('title') . '</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                                <input type="hidden" name="OldTitle" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                            </div>
                                                        </div>
                                                        ' . $common_fields . '
                                                        
                                                        
                                                    </div>
                                                    '. $common_fields2 .'
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Heading' . $key . '">' . lang('Heading') . '</label>
                                                                <input type="text" name="Heading" parsley-trigger="change" required  class="form-control" id="Heading' . $key . '" value="' . ((isset($result[$key]->Heading)) ? $result[$key]->Heading : '') . '">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description">'. lang('Description') .'</label><br>
                                                                <textarea class="form-control" id="Description" name="Description" value="">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
    }
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Languages</h5>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="DeleteArticleImage" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading"><?php echo lang('delete'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> <?php echo lang('AreYouSureDeleteFile'); ?></div>
            </div>
            <div class="modal-footer ">
                <a type="button" class="btn btn-success delete_url" ><span class="glyphicon glyphicon-ok-sign"></span> <?php echo lang('Yes'); ?></a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> <?php echo lang('No'); ?></button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/plugins/parsleyjs/parsley.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
$(document).on('click', '.remove_image', function () {
    var image_id       = $(this).attr('data-image-id');
    var image_path       = $(this).attr('data-image-path');
    $(".delete_url").attr('data-modal-image-id', image_id);
    $(".delete_url").attr('data-modal-image-path', image_path);
    $('#DeleteArticleImage').modal('show');
});

tinymce.init({
    selector: 'textarea',
    theme : "modern",
    mode: "exact",
    theme_advanced_toolbar_location : "top",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
    + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
    + "bullist,numlist,outdent,indent",
    theme_advanced_buttons2 : "link,unlink,anchor,image,separator,"
    +"undo,redo,cleanup,code,separator,sub,sup,charmap",
    theme_advanced_buttons3 : "",
    height:"250px",
    setup: function (editor) {
        editor.on('change', function () {
            tinymce.triggerSave();
        });
    }
});

$(document).on('click', '.delete_url', function () {
    var image_id       = $(this).attr('data-modal-image-id');
    var image_path       = $(this).attr('data-modal-image-path');
    var $this = $(this);
    $.ajax({
            type: "POST",
            url: '<?php echo base_url() . 'cms/' . $ControllerName . '/DeleteImage'; ?>',
            data: {
                image_path: image_path,
                image_id: image_id
            },
            success: function (result) {
                $.unblockUI;
                if (result.error != false) {
                    $("#img-"+image_id).remove();
                }



            },
            complete: function () {
                $('#DeleteArticleImage').modal('hide');
                $.unblockUI();
            }
        });
});
</script>
