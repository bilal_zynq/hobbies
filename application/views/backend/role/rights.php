<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                                                    <h4 class="page-title"><?php echo lang($ControllerName.'_rights'); ?></h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                        
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
			</div>
                        <!-- end row -->


                         <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                        <input type="hidden" name="form_type" value="save_rights">
                                        
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ParentID"><?php echo lang('select_role');?></label>
                                        <select id="RoleID" class="form-control roles" required="" name="RoleID">
                                            <?php foreach($roles as $value){ ?>
                                                    <option value="<?php echo $value->RoleID; ?>" <?php echo ($value->RoleID == $RoleID ? 'selected' : ''); ?>><?php echo $value->Title;?></option>
                                           <?php  } ?>
                                             
                                            
                                        </select>
                                    </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                     <h4 class="m-t-0 header-title"><?php echo lang('rights');?></h4>
                                   
                                    
                                    
                                        <table id="custom-table"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th><div class="checkbox">
                                                        <input id="all_check" type="checkbox"  data-parsley-multiple="all_check">
                                                        <label for="all_check"> &nbsp; </label>
                                                </div>
                                            </th>
                                            <th><?php echo lang('title');?></th>
                                            <th><?php echo lang('view');?></th>
                                            <th><?php echo lang('add');?></th>
                                            <th><?php echo lang('edit');?></th>
                                            <th><?php echo lang('delete');?></th>
                                        </tr>
                                        
                                        </thead>
                                        <tbody>
                                    <?php if($results){
                                                foreach($results as $value){ ?>
                                                    <tr id="<?php echo $value->ModuleRightID;?>" >
                                                        <td><div class="checkbox">
                                                                    <input id="all-<?php echo $value->ModuleRightID;?>" class= "all horizontal" data-id = "<?php echo $value->ModuleRightID;?>" type="checkbox" data-parsley-multiple="all-<?php echo $value->ModuleRightID;?>">
                                                                    <label for="all-<?php echo $value->ModuleRightID;?>"> &nbsp; </label>
                                                        </div></td>    
                                                        <td><?php echo $value->ModuleTitle; ?></td>
                                                        <td><div class="checkbox">
                                                                <input id="view-<?php echo $value->ModuleRightID;?>" class= "all horizontal-<?php echo $value->ModuleRightID;?>" <?php echo ($value->CanView == 1 ? 'checked' : '' );?> type="checkbox" data-parsley-multiple="view-<?php echo $value->ModuleRightID;?>" name="CanView[<?php echo $value->ModuleRightID;?>]">
                                                                    <label for="view-<?php echo $value->ModuleRightID;?>"> &nbsp; </label>
                                                                </div></td>
                                                        <td><div class="checkbox">
                                                                    <input id="add-<?php echo $value->ModuleRightID;?>" class= "all horizontal-<?php echo $value->ModuleRightID;?>" <?php echo ($value->CanAdd == 1 ? 'checked' : '' );?> type="checkbox" data-parsley-multiple="add-<?php echo $value->ModuleRightID;?>" name="CanAdd[<?php echo $value->ModuleRightID;?>]">
                                                                    <label for="add-<?php echo $value->ModuleRightID;?>"> &nbsp; </label>
                                                                </div></td>

                                                        <td><div class="checkbox">
                                                                    <input id="edit-<?php echo $value->ModuleRightID;?>" class= "all horizontal-<?php echo $value->ModuleRightID;?>" <?php echo ($value->CanEdit == 1 ? 'checked' : '' );?> type="checkbox" data-parsley-multiple="edit-<?php echo $value->ModuleRightID;?>" name="CanEdit[<?php echo $value->ModuleRightID;?>]">
                                                                    <label for="edit-<?php echo $value->ModuleRightID;?>"> &nbsp; </label>
                                                                </div></td>


                                                        <td>

                                                            <div class="checkbox">
                                                                    <input id="delete-<?php echo $value->ModuleRightID;?>" class= "all horizontal-<?php echo $value->ModuleRightID;?>" <?php echo ($value->CanDelete == 1 ? 'checked' : '' );?> type="checkbox" data-parsley-multiple="delete-<?php echo $value->ModuleRightID;?>" name="CanDelete[<?php echo $value->ModuleRightID;?>]">
                                                                    <label for="delete-<?php echo $value->ModuleRightID;?>"> &nbsp; </label>
                                                                </div>
                                                        </td> 
                                                    </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                        <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            <?php echo lang('submit');?>
                                                        </button>
                                                        <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                          <?php echo lang('back');?>
                                                        </button>
                                                        </a>
                                        </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </form>
                                         

                    </div> <!-- container -->

                </div> <!-- content -->
<script src="<?php echo base_url();?>assets/backend/js/module.js"></script>            