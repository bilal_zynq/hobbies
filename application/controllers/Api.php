<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

date_default_timezone_set('UTC');

class Api extends REST_Controller
{
    public $language = 'EN';

    function __construct()
    {
        parent::__construct();
        $this->methods['user_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 5000; // 50 requests per hour per user/key
        $this->load->model('User_model');
        $this->load->model('User_text_model');

        $this->load->model('Api_model');
        $this->load->model('Category_model');
        $this->load->model('Model_general');
        $this->load->model('Group_model');
        $this->load->model('Group_text_model');
        $this->load->model('Group_member_model');
        $this->load->model('Report_group_model');

        $this->load->model('Post_model');
        $this->load->model('Post_text_model');
        $this->load->model('Post_image_model');
        $this->load->model('Post_video_model');
        $this->load->model('Post_comment_model');
        $this->load->model('Post_comment_image_model');
        $this->load->model('Post_like_model');
        $this->load->model('Post_report_model');
        $this->load->model('Post_share_model');
        $this->load->model('Report_comment_model');
        $this->load->model('Group_invite_model');
        $this->load->model('User_notification_model');
        //$this->load->model('Report_post_model');

        $this->load->model('Badge_model');
        $this->load->model('Badge_request_model');
        $this->load->model('Badge_request_attachments_model');
        $this->load->model('Suggested_hobbies_model');

        $this->load->model('Follower_model');

        $this->load->model('Chat_model');
        $this->load->model('Chat_message_model');
        $this->load->model('Chat_group_model');
        $this->load->model('Chat_group_joinee_model');


        $this->load->model('City_model');
        $this->load->model('Type_model');
        $this->load->model('Upgrade_account_model');
        $this->load->model('Type_text_model');
        $this->load->model('User_type_model');
        $this->load->model('User_type_text_model');

        // if UserID is coming in an API then we are checking if that user exists or not
        if (isset($_REQUEST['UserID']) && $_REQUEST['UserID'] > 0) {
            if (!$this->checkUserExist($_REQUEST['UserID'])) {
                $this->response([
                    'status' => FALSE,
                    'message' => "User not found \n المستخدم ليس موجود"
                ], REST_Controller::HTTP_OK);
            }
        }

        if (isset($_REQUEST['Language'])) {
            $this->language = $_REQUEST['Language'];
        }

    }

    public function test_get()
    {
        $this->sendWelcomeEmail(77, 'test123123');
        exit();
        echo 'herse';
        $post_data = $this->input->get('abc');
        dump($_REQUEST);
        $fields = $this->User_model->getFields();
        print_rm($fields);
        exit;
    }

    public function postTest_post()
    {
        $post_data = $this->input->get();
        dump($post_data);
    }

    public function testPusher_get()
    {
        $message = array('name' => 'bilal', 'message' => 'test message');
        pusher($message, true);
    }

    public function signUp_post()
    {
        $post_data = $this->input->post(); // full_name, email, password, type (company, designer, visitor), designation (for designer type), industry (for company type), city, image, device_type, device_token
        if (!empty($post_data)) {
            if (isset($post_data['Email'])) {
                $user = $this->User_model->getWithMultipleFields(array('Email' => $post_data['Email']), true);
                if ($user) {
                    // sending back error
                    $this->response([
                        'status' => FALSE,
                        'message' => "Email address already exist \n عنوان البريد الإلكتروني موجود بالفعل"
                    ], REST_Controller::HTTP_OK);
                }
            }

            $parent_tbl_data = $this->User_model->getFields();
            $child_tbl_data = $this->User_text_model->getFields();
            $save_parent_data = array();
            $save_child_data = array();

            foreach ($post_data as $key => $value) {

                if (in_array($key, $parent_tbl_data)) {
                    $save_parent_data[$key] = $value;


                } else if (in_array($key, $child_tbl_data)) {
                    $save_child_data[$key] = $value;
                }

            }


            $password = '';
            if (isset($save_parent_data['Password'])) {

                $password = $save_parent_data['Password'];
                $save_parent_data['Password'] = md5($save_parent_data['Password']);
            }


            $save_parent_data['UpdatedAt'] = $save_parent_data['CreatedAt'] = time();
            $save_parent_data['RoleID'] = 2;
            if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {

                $file_name = $this->uploadImage("Image", "uploads/users/");
                $file_name = "uploads/users/" . $file_name;
            } else {
                $file_name = "uploads/user-default.png";
            }

            if (isset($_FILES['CompressedImage']) && $_FILES['CompressedImage']['name'] != '') {

                $compressed_file_name = $this->uploadImage("CompressedImage", "uploads/users/compressed/");
                $compressed_file_name = "uploads/users/compressed/" . $compressed_file_name;
            } else {
                $compressed_file_name = "uploads/user-default.png";
            }

            $save_parent_data['Image'] = $file_name;
            $save_parent_data['CompressedImage'] = $compressed_file_name;

            // setting default cover Image
            $save_parent_data['CoverImage'] = "uploads/cover-default.png";
            $save_parent_data['CompressedCoverImage'] = "uploads/cover-default.png";

            $insert_id = $this->User_model->save($save_parent_data);
            if ($insert_id > 0) {
                $save_child_data['UserID'] = $insert_id;

                $save_child_data['SystemLanguageID'] = 1;
                $this->User_text_model->save($save_child_data);
                $this->sendWelcomeEmail($insert_id, $password);
                $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $insert_id . '');
                $this->response([
                    'status' => TRUE,
                    'user_info' => NullToEmpty($user[0])
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "User signup failed. Please try again \n فشل اشتراك المستخدم. حاول مرة اخرى"
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function updateProfile_post()
    {
        $post_data = $this->input->post(); // UserID, FullName, Email, Password, Type (company, designer, visitor), Designation (for designer type), Industry (for company type), City, Image, CompressedImage, CoverImage, CompressedCoverImage, Gender, Bio, DeviceType, DeviceToken
        if (!empty($post_data)) {
            if (isset($post_data['Email'])) {
                $user = $this->User_model->getWithMultipleFields(array('Email' => $post_data['Email']), true);
                if ($user) {
                    // sending back error
                    $this->response([
                        'status' => FALSE,
                        'message' => "Email address already exist \n عنوان البريد الإلكتروني موجود بالفعل"
                    ], REST_Controller::HTTP_OK);
                }
            }

            $parent_tbl_data = $this->User_model->getFields();
            $child_tbl_data = $this->User_text_model->getFields();
            $save_parent_data = array();
            $save_child_data = array();

            foreach ($post_data as $key => $value) {

                if (in_array($key, $parent_tbl_data)) {
                    $save_parent_data[$key] = $value;


                } else if (in_array($key, $child_tbl_data)) {
                    $save_child_data[$key] = $value;
                }

            }

            $password = '';
            if (isset($save_parent_data['Password'])) {

                $password = $save_parent_data['Password'];
                $save_parent_data['Password'] = md5($save_parent_data['Password']);
            }

            $save_parent_data['UpdatedAt'] = time();


            if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {

                $file_name = $this->uploadImage("Image", "uploads/users/");
                $save_parent_data['Image'] = "uploads/users/" . $file_name;
            }
            if (isset($_FILES['CompressedImage']) && $_FILES['CompressedImage']['name'] != '') {

                $compressed_file_name = $this->uploadImage("CompressedImage", "uploads/users/compressed/");
                $save_parent_data['CompressedImage'] = "uploads/users/compressed/" . $compressed_file_name;
            }
            if (isset($_FILES['CoverImage']) && $_FILES['CoverImage']['name'] != '') {

                $compressed_file_name = $this->uploadImage("CoverImage", "uploads/users/");
                $save_parent_data['CoverImage'] = "uploads/users/" . $compressed_file_name;
            }
            if (isset($_FILES['CompressedCoverImage']) && $_FILES['CompressedCoverImage']['name'] != '') {

                $compressed_file_name = $this->uploadImage("CompressedCoverImage", "uploads/users/compressed/");
                $save_parent_data['CompressedCoverImage'] = "uploads/users/compressed/" . $compressed_file_name;
            }
            $update_by = array();
            $update_by['UserID'] = $post_data['UserID'];
            $this->User_model->update($save_parent_data, $update_by);
            if ($post_data['UserID'] > 0) {
                $update_by['SystemLanguageID'] = 1;
                $this->User_text_model->update($save_child_data, $update_by);
                $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $post_data['UserID'] . '');
                $AccountType = $this->Type_text_model->getWithMultipleFields(array('TypeID' => $user[0]['AccountTypeID']));
                $user[0]['AccountType'] = $AccountType->Title;

                if (isset($post_data['Language'])) {
                    $language = $post_data['Language'];
                } else {
                    $language = 'EN';
                }

                $where = 'cities.CityID = ' . $user[0]['CityID'] . ' AND system_languages.ShortCode = "' . $this->language . '"';
                $city = $this->City_model->getJoinedData(true, 'CityID', $where);

                $user[0]['City'] = (isset($city[0]['Title']) && $city[0]['Title'] != '' ? $city[0]['Title'] : '');

                $this->response([
                    'status' => TRUE,
                    'user_info' => NullToEmpty($user[0])
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "Update profile failed. Please try again \n فشل تحديث الملف الشخصي. حاول مرة اخرى"
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function login_post()
    {
        $data = $this->input->post(); // email, password, device_type, device_token
        if (!empty($data)) {
            $data_fetch = array();
            $data_fetch['Email'] = $data['Email'];
            $data_fetch['Password'] = md5($data['Password']);
            $user = $this->User_model->getWithMultipleFields($data_fetch, true);
            if ($user) {
                if (isset($data['DeviceType']) || isset($data['DeviceToken'])) {
                    $update_user = array();
                    $update_user_by = array();
                    $update_user_by['UserID'] = $user['UserID'];

                    if (isset($data['DeviceType'])) {
                        $update_user['DeviceType'] = $data['DeviceType'];
                        $user['DeviceType'] = $data['DeviceType'];
                    }

                    if (isset($data['DeviceToken'])) {
                        $update_user['DeviceToken'] = $data['DeviceToken'];
                        $user['DeviceToken'] = $data['DeviceToken'];
                    }

                    $this->User_model->update($update_user, $update_user_by);
                }

                if ($user['IsActive'] == 'no') {
                    $this->response([
                        'status' => FALSE,
                        'message' => "Your account is suspended. Please contact admin for further details and action \n تم تعليق حسابك. يرجى الاتصال المشرف لمزيد من التفاصيل والإجراءات"
                    ], REST_Controller::HTTP_OK);
                } else {
                    $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $user['UserID'] . '');
                    $AccountType = $this->Type_text_model->getWithMultipleFields(array('TypeID' => $user[0]['AccountTypeID']));
                    $user[0]['AccountType'] = $AccountType->Title;

                    if (isset($data['Language'])) {
                        $language = $data['Language'];
                    } else {
                        $language = 'EN';
                    }

                    $where = 'cities.CityID = ' . $user[0]['CityID'] . ' AND system_languages.ShortCode = "' . $language . '"';
                    $city = $this->City_model->getJoinedData(true, 'CityID', $where);

                    $user[0]['City'] = (isset($city[0]['Title']) && $city[0]['Title'] != '' ? $city[0]['Title'] : '');

                    $this->response([
                        'status' => TRUE,
                        'user_info' => NullToEmpty($user[0])
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "Email or password incorrect \n البريد الإلكتروني أو كلمة المرور غير صحيحة"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

    }

    public function getUserDetails_get()
    {
        //$post_data = $this->input->get(); // user_id
        $post_data = $_REQUEST; // user_id
        if (!empty($post_data)) {
            $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $post_data['UserID'] . '');
            if ($user) {
                $AccountType = $this->Type_text_model->getWithMultipleFields(array('TypeID' => $user[0]['AccountTypeID']));
                $user[0]['AccountType'] = $AccountType->Title;

                if (isset($post_data['Language'])) {
                    $language = $post_data['Language'];
                } else {
                    $language = 'EN';
                }

                $where = 'cities.CityID = ' . $user[0]['CityID'] . ' AND system_languages.ShortCode = "' . $this->language . '"';
                $city = $this->City_model->getJoinedData(true, 'CityID', $where);

                $user[0]['City'] = (isset($city[0]['Title']) && $city[0]['Title'] != '' ? $city[0]['Title'] : '');

                $this->response([
                    'status' => TRUE,
                    'user_info' => NullToEmpty($user[0])
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "User not found for this ID \n لم يتم العثور على المستخدم لهذا المعرف"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getProfile_get()
    {
        $post_data = $this->input->get(); // UserID, LoginUserID, Language
        if (!empty($post_data)) {
            $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $post_data['UserID'] . '');
            if ($user) {

                // send this LoginUserID if viewing other user's profile
                if (isset($post_data['LoginUserID'])) {
                    $user[0]['total_following'] = $this->Follower_model->getTotalFollowing($post_data['UserID']);
                    $user[0]['total_follower'] = $this->Follower_model->getTotalFollower($post_data['UserID']);
                    $check_is_following = array();
                    $check_is_following['Follower'] = $post_data['LoginUserID'];
                    $check_is_following['Following'] = $post_data['UserID'];
                    $is_following = $this->Follower_model->getWithMultipleFields($check_is_following);
                    if ($is_following) {
                        $user[0]['Follower'] = 1;
                    } else {
                        $user[0]['Follower'] = 0;
                    }

                    $check_is_follow = array();
                    $check_is_follow['Follower'] = $post_data['UserID'];
                    $check_is_follow['Following'] = $post_data['LoginUserID'];
                    $is_follow = $this->Follower_model->getWithMultipleFields($check_is_follow);
                    if ($is_follow) {
                        $user[0]['Following'] = 1;
                    } else {
                        $user[0]['Following'] = 0;
                    }
                }

                $AccountType = $this->Type_text_model->getWithMultipleFields(array('TypeID' => $user[0]['AccountTypeID']));
                $user[0]['AccountType'] = $AccountType->Title;

                $user[0]['TotalGroupJoined'] = $this->Group_member_model->totalGroupJoined($post_data['UserID']);
                $user[0]['TotalGroupCreated'] = (string)$this->Group_model->getRowsCount(array('UserID' => $post_data['UserID']));

                $badges = $this->Badge_request_model->getUserApprovedBadges($post_data['UserID']);
                $user[0]['Badges'] = $badges;

                if (isset($post_data['Language'])) {
                    $language = $post_data['Language'];
                } else {
                    $language = 'EN';
                }

                $where = 'cities.CityID = ' . $user[0]['CityID'] . ' AND system_languages.ShortCode = "' . $this->language . '"';
                $city = $this->City_model->getJoinedData(true, 'CityID', $where);
                $user[0]['City'] = (isset($city[0]['Title']) && $city[0]['Title'] != '' ? $city[0]['Title'] : '');

                $this->response([
                    'status' => TRUE,
                    'user' => NullToEmpty($user[0])
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "User not found for this ID \n لم يتم العثور على المستخدم لهذا المعرف"
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function deletePost_post()
    {
        $post_data = $_REQUEST; // PostID, GroupID, UserID
        if (!empty($post_data)) {
            if ($this->checkIfUserIsAdmin($post_data['GroupID'], $post_data['UserID'])) {
                unset($post_data['GroupID']);
                unset($post_data['UserID']);
                $this->Post_model->delete($post_data);
                $this->Post_text_model->delete($post_data);
                $this->Post_report_model->delete($post_data);
                $this->Post_comment_model->delete($post_data);
                $this->Post_comment_image_model->delete(array('PostID' => $post_data['PostID']));
                $this->Post_image_model->delete($post_data);
                $this->Post_like_model->delete($post_data);
                //  $this->Post_save_model->delete($post_data);
                $this->Post_share_model->delete($post_data);
                $this->Post_video_model->delete($post_data);
                $this->response([
                    'status' => TRUE,
                    'message' => "Post deleted \n تم حذف المشاركة"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => "User is not an admin of this group \n المستخدم ليس مسؤولاً عن هذه المجموعة"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }


    public function deletePostComment_post()
    {
        $post_data = $_REQUEST; // CommentID
        if (!empty($post_data)) {
            //$deleted_by = array();
            //$deleted_by['PostCommentId'] = $post_data['CommentID'];
            $this->Post_comment_model->delete($post_data);
            $this->Post_comment_image_model->delete($post_data);

            $this->response([
                'status' => TRUE,
                'message' => "Comment deleted \n التعليق المحذوفة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }

    public function reportComment_post()
    {
        $post_data = $this->input->post(); // PostID, UserID, PostCommentID, CommentReportReason, CommentReportedOn
        if (!empty($post_data)) {
            $this->Report_comment_model->save($post_data);
            $this->response([
                'status' => TRUE,
                'message' => "Post comment reported successfully \n تم نشر تعليق بنجاح"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }

    public function categories_get()
    {

        $post_data = $this->input->get();
        if (isset($post_data['Language'])) {
            $language = $post_data['Language'];
        } else {
            $language = 'EN';
        }
        if (isset($post_data['CategoryID'])) {
            $where = 'categories.ParentID =' . $post_data['CategoryID'] . ' AND system_languages.ShortCode = "' . $this->language . '"';
        } else {
            $where = 'categories.ParentID = 0 AND system_languages.ShortCode = "' . $language . '"';
        }

        $categories = $this->Category_model->getJoinedData(true, 'CategoryID', $where);

        $this->response([
            'status' => TRUE,
            'categories' => $categories
        ], REST_Controller::HTTP_OK);


    }


    public function cities_get()
    {

        $post_data = $_REQUEST;
        if (isset($post_data['Language'])) {
            $language = $post_data['Language'];
        } else {
            $language = 'EN';
        }
        $where = 'system_languages.ShortCode = "' . $this->language . '"';

        $cities = $this->City_model->getJoinedData(true, 'CityID', $where);

        $this->response([
            'status' => TRUE,
            'cities' => $cities
        ], REST_Controller::HTTP_OK);


    }


    public function account_types_get()
    {

        // $post_data = $this->input->get();


        $account_types = $this->Type_model->getJoinedData(true, 'TypeID');

        $this->response([
            'status' => TRUE,
            'account_types' => $account_types
        ], REST_Controller::HTTP_OK);


    }

    public function user_types_get()
    {

        // $post_data = $this->input->get();


        $user_types = $this->User_type_model->getJoinedData(true, 'User_TypeID');

        $this->response([
            'status' => TRUE,
            'user_types' => $user_types
        ], REST_Controller::HTTP_OK);


    }


    public function sendPackageUpgradeRequest_post()
    {
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {

            if ($this->checkUserExist($post_data['UserID'])) {


                $package_detail = $this->Type_model->get($post_data['TypeID'], true, 'TypeID');
                if ($package_detail) {

                    $post_data['Status'] = 'pending';
                    $file_name = '';
                    $post_data['AmountToBePaid'] = $package_detail['Price'];

                    if (isset($_FILES['ReceiptImage']) && $_FILES['ReceiptImage']['name'] != '') {
                        $file_name = 'uploads/images/' . $this->uploadImage('ReceiptImage', 'uploads/images/');

                    }
                    $post_data['ReceiptImage'] = $file_name;

                    $post_data['CreatedAt'] = date('Y-m-d');


                    $insert_id = $this->Upgrade_account_model->save($post_data);


                    if ($insert_id > 0) {


                        $this->response([
                            'status' => TRUE,
                            'message' => "Upgradation request sent successfully.Please wait admin approval \n تم إرسال طلب الترقية بنجاح.يرجى الانتظار موافقة المشرف"
                        ], REST_Controller::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => FALSE,
                            'message' => "Something went wrong \n هناك خطأ ما"
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                    }

                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => "Requested Package doesn't exist \n الحزمة المطلوبة غير موجودة"
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }


            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "User not found \n المستخدم ليس موجود"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

    }

    public function createGroup_post()
    {
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {

            if ($this->checkUserExist($post_data['UserID'])) {

                $parent_tbl_data = $this->Group_model->getFields();
                //print_rm($parent_tbl_data);
                $child_tbl_data = $this->Group_text_model->getFields();
                $save_parent_data = array();
                $save_child_data = array();


                foreach ($post_data as $key => $value) {

                    if (in_array($key, $parent_tbl_data)) {
                        $save_parent_data[$key] = $value;


                    } else if (in_array($key, $child_tbl_data)) {
                        $save_child_data[$key] = $value;
                    }

                }


                $save_parent_data['IsActive'] = 1;
                $file_name = '';
                $compressed_file_name = '';
                if (isset($_FILES['GroupImage']) && $_FILES['GroupImage']['name'] != '') {
                    $file_name = 'uploads/groups/' . $this->uploadImage('GroupImage', 'uploads/groups/');
                }

                if (isset($_FILES['GroupCompressImage']) && $_FILES['GroupCompressImage']['name'] != '') {
                    $compressed_file_name = 'uploads/groups/' . $this->uploadImage('GroupCompressImage', 'uploads/groups/');

                }
                $save_parent_data['GroupImage'] = $file_name;
                $save_parent_data['GroupCompressImage'] = $compressed_file_name;

                $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = time();
                $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $post_data['UserID'];

                $insert_id = $this->Group_model->save($save_parent_data);


                if ($insert_id > 0) {

                    $save_child_data['GroupID'] = $insert_id;
                    $save_child_data['SystemLanguageID'] = 1;

                    $this->Group_text_model->save($save_child_data);

                    $group_member_data = array();
                    $group_member_data['UserID'] = $post_data['UserID'];
                    $group_member_data['GroupID'] = $insert_id;
                    $group_member_data['IsAdmin'] = 'yes';
                    $group_member_data['CreatedAt'] = time();
                    $this->Group_member_model->save($group_member_data);


                    $group_post_data = array();
                    $group_post_data['GroupID'] = $insert_id;
                    $group_post_data['UserID'] = $post_data['UserID'];
                    $group_post_data['CreatedAt'] = time();
                    $group_post_data['UpdatedAt'] = time();
                    $group_post_data['CreatedBy'] = $post_data['UserID'];
                    $group_post_data['UpdatedBy'] = $post_data['UserID'];
                    $group_post_data['PinnedPost'] = 1;

                    $post_id = $this->Post_model->save($group_post_data);
                    if ($post_id > 0) {
                        $group_post_text_data = array();
                        $group_post_text_data['PostID'] = $post_id;
                        $group_post_text_data['PostText'] = "Welcome to the group. Feel free to discuss anything here \n مرحبا بك في المجموعة. لا تتردد في مناقشة أي شيء هنا";
                        $group_post_text_data['SystemLanguageID'] = 1;
                        $this->Post_text_model->save($group_post_text_data);
                    }


                    $group = $this->Group_model->getGroupsDate('groups.GroupID = ' . $insert_id . '');
                    $this->response([
                        'status' => TRUE,
                        'group_info' => NullToEmpty($group[0])
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => "Something went wrong \n هناك خطأ ما"
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "User not found \n المستخدم ليس موجود"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function updateGroup_post()
    {
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {
            $parent_tbl_data = $this->Group_model->getFields();
            $child_tbl_data = $this->Group_text_model->getFields();
            $save_parent_data = array();
            $save_child_data = array();

            foreach ($post_data as $key => $value) {

                if (in_array($key, $parent_tbl_data) && $key != 'GroupID') {
                    $save_parent_data[$key] = $value;


                } else if (in_array($key, $child_tbl_data) && $key != 'GroupID') {
                    $save_child_data[$key] = $value;
                }

            }


            if (isset($_FILES['GroupImage']) && $_FILES['GroupImage']['name'] != '') {
                $save_parent_data['GroupImage'] = 'uploads/groups/' . $this->uploadImage('GroupImage', 'uploads/groups/');
            }
            if (isset($_FILES['GroupCompressImage']) && $_FILES['GroupCompressImage']['name'] != '') {
                $save_parent_data['GroupCompressImage'] = 'uploads/groups/' . $this->uploadImage('GroupCompressImage', 'uploads/groups/');
            }
            $save_parent_data['UpdatedAt'] = time();
            $save_parent_data['UpdatedBy'] = $post_data['UserID'];

            $this->Group_model->update($save_parent_data, array('GroupID' => $post_data['GroupID']));


            $update_by = array();
            $update_by['GroupID'] = $post_data['GroupID'];
            $update_by['SystemLanguageID'] = 1;
            $this->Group_text_model->update($save_child_data, $update_by);


            $group = $this->Group_model->getGroupsDate('groups.GroupID = ' . $post_data['GroupID'] . '');
            $this->response([
                'status' => TRUE,
                'group_info' => NullToEmpty($group[0])
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function createPost_post()
    {
        $post_data = $this->input->post(); // type, text, link, user_id, image_count, video_count, image1, image2, compressed_image1, compressed_image2, video1, video2, video_thumb1, video_thumb2 etc.., CreatedAt, UpdatedAt
        if (!empty($post_data)) {
            if ($this->checkIfUserIsAdmin($post_data['GroupID'], $post_data['UserID'])) {
                $parent_tbl_data = $this->Post_model->getFields();
                $child_tbl_data = $this->Post_text_model->getFields();
                $save_parent_data = array();
                $save_child_data = array();

                foreach ($post_data as $key => $value) {

                    if (in_array($key, $parent_tbl_data)) {
                        $save_parent_data[$key] = $value;


                    } else if (in_array($key, $child_tbl_data)) {
                        $save_child_data[$key] = $value;
                    }

                }

                $inserted_id = $this->Post_model->save($save_parent_data);
                if ($inserted_id > 0) {
                    if (isset($save_child_data['PostText']) && $save_child_data['PostText'] != '') {
                        $save_child_data['PostID'] = $inserted_id;
                        $save_child_data['SystemLanguageID'] = 1;
                        $this->Post_text_model->save($save_child_data);
                    }
                    // uploading images and videos for post
                    $images_count = (isset($post_data['ImageCount']) && $post_data['ImageCount'] > 0 ? $post_data['ImageCount'] : 0);
                    $videos_count = (isset($post_data['VideoCount']) && $post_data['VideoCount'] > 0 ? $post_data['VideoCount'] : 0);
                    $image_extensions = array("jpeg", "jpg", "png", "gif", "JPEG", "JPG", "PNG", "GIF");
                    $video_extensions = array("MP4", "mp4", "3gp", "3GP", "mov", "MOV", "MPEG4", "mpeg4");


                    ///////////// here
                    for ($i = 1; $i <= $images_count; $i++) {
                        $file_name = $_FILES["Image" . $i]["name"];
                        $file_tmp = $_FILES["Image" . $i]["tmp_name"];
                        $compressed_file_name = $_FILES["CompressedImage" . $i]["name"];
                        $compressed_file_tmp = $_FILES["CompressedImage" . $i]["tmp_name"];
                        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                        $compressed_ext = pathinfo($compressed_file_name, PATHINFO_EXTENSION);

                        $filename = basename($file_name, $ext);
                        $newFileName = $filename . $i . time() . "." . $ext;
                        move_uploaded_file($file_tmp, "uploads/posts/images/" . $newFileName);

                        // for compressed image
                        $compressed_filename = basename($compressed_file_name, $compressed_ext);
                        $compressed_newFileName = $compressed_filename . $i . time() . "." . $compressed_ext;
                        move_uploaded_file($compressed_file_tmp, "uploads/posts/images/compressed/" . $compressed_newFileName);

                        $post_image_data['PostID'] = $inserted_id;
                        $post_image_data['Image'] = "uploads/posts/images/" . $newFileName;
                        $post_image_data['CompressedImage'] = "uploads/posts/images/compressed/" . $compressed_newFileName;
                        $this->Post_image_model->save($post_image_data);
                    }

                    for ($i = 1; $i <= $videos_count; $i++) {
                        // for video
                        $video_file_name = $_FILES["Video" . $i]["name"];
                        $video_file_tmp = $_FILES["Video" . $i]["tmp_name"];
                        $video_thumb_file_name = $_FILES["VideoThumb" . $i]["name"];
                        $video_thumb_file_tmp = $_FILES["VideoThumb" . $i]["tmp_name"];
                        $video_ext = pathinfo($video_file_name, PATHINFO_EXTENSION);
                        $video_thumb_ext = pathinfo($video_thumb_file_name, PATHINFO_EXTENSION);

                        // for video
                        $video_filename = basename($video_file_name, $video_ext);
                        $video_newFileName = $video_filename . $i . time() . "." . $video_ext;
                        move_uploaded_file($video_file_tmp, "uploads/posts/videos/" . $video_newFileName);

                        // for video thumb
                        $video_thumb_filename = basename($video_thumb_file_name, $video_thumb_ext);
                        $video_thumb_newFileName = $video_thumb_filename . $i . time() . "." . $video_thumb_ext;
                        move_uploaded_file($video_thumb_file_tmp, "uploads/posts/videos/thumbs/" . $video_thumb_newFileName);

                        // saving in db
                        $post_video_data['PostID'] = $inserted_id;
                        $post_video_data['Video'] = "uploads/posts/videos/" . $video_newFileName;
                        $post_video_data['VideoThumb'] = "uploads/posts/videos/thumbs/" . $video_thumb_newFileName;
                        $this->Post_video_model->save($post_video_data);
                    }

                    //$post = $this->Post_model->getJoinedDataWithOtherTable(true, 'PostID', 'posts_text', 'posts.PostID = ' . $inserted_id . '');
                    $post = $this->Post_model->getLeftJoinedDataWithOtherTable(true, 'PostID', 'posts_text', 'posts.PostID = ' . $inserted_id . '');
                    $post = NullToEmpty($post[0]);
                    $post_images = $this->Post_image_model->getMultipleRows(array('PostID' => $inserted_id), true);
                    $post_videos = $this->Post_video_model->getMultipleRows(array('PostID' => $inserted_id), true);
                    $post['post_images'] = ($post_images ? NullToEmpty($post_images) : array());
                    $post['post_videos'] = ($post_videos ? NullToEmpty($post_videos) : array());

                    // updating last activity time for group
                    $group_data['LastActivityAt'] = time();
                    $this->Group_model->update($group_data, array('GroupID' => $post_data['GroupID']));

                    $this->response([
                        'status' => TRUE,
                        'post' => $post
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => "Post not added. Please try again \n لم تتم إضافة المشاركة. حاول مرة اخرى"
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "User is not an admin of this group \n المستخدم ليس مسؤولاً عن هذه المجموعة"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    /*public function getPostDetail_get()
    {
        $post_data = $_REQUEST; // PostID, Start, Limit
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $post = $this->Post_model->getLeftJoinedDataWithOtherTable(true, 'PostID', 'posts_text', 'posts.PostID = ' . $post_data['PostID'] . '');
            $post = NullToEmpty($post[0]);
            $post_comments = $this->Post_comment_model->postComments($post['PostID'], $limit, $start);
            $post_likes = $this->Post_like_model->postLikes($post['PostID']);
            $post_images = $this->Post_image_model->getMultipleRows(array('PostID' => $post['PostID']), true);
            $post_videos = $this->Post_video_model->getMultipleRows(array('PostID' => $post['PostID']), true);
            $post['post_comments'] = ($post_comments ? NullToEmpty($post_comments) : array());
            $post['post_likes'] = ($post_likes ? NullToEmpty($post_likes) : array());
            $post['post_comments_count'] = ($post_comments ? count($post_comments) : array());
            $post['post_likes_count'] = ($post_likes ? count($post_likes) : array());
            $post['post_images'] = ($post_images ? NullToEmpty($post_images) : array());
            $post['post_videos'] = ($post_videos ? NullToEmpty($post_videos) : array());
            $this->response([
                'status' => TRUE,
                'post' => $post
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }*/

    public function getPostDetail_get()
    {
        $post_data = $_REQUEST; // PostID, UserID, Start, Limit
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $post = $this->Post_model->postsDetail($post_data['PostID'], $post_data['UserID']);
            $post = NullToEmpty($post[0]);

            $check_is_group_member = array();
            $check_is_group_member['GroupID'] = $post['GroupID'];
            $check_is_group_member['UserID'] = $post_data['UserID'];


            $member_data = $this->Group_member_model->getWithMultipleFields($check_is_group_member);
            if ($member_data) {
                $post['IsMember'] = 'yes';
                $post['IsAdmin'] = $member_data->IsAdmin;
            } else {
                $post['IsMember'] = 'no';
                $post['IsAdmin'] = 'no';
            }

            $post_comments = $this->Post_comment_model->postComments($post['PostID'], $limit, $start);

            $post_likes = $this->Post_like_model->postLikes($post['PostID']);
            $post_images = $this->Post_image_model->getMultipleRows(array('PostID' => $post['PostID']), true);
            $post_videos = $this->Post_video_model->getMultipleRows(array('PostID' => $post['PostID']), true);
            $post['post_comments_count'] = ($post_comments ? count($post_comments) : 0);
            $post['post_likes_count'] = ($post_likes ? count($post_likes) : 0);
            $post['post_images'] = ($post_images ? NullToEmpty($post_images) : array());
            $post['post_videos'] = ($post_videos ? NullToEmpty($post_videos) : array());
            $this->response([
                'status' => TRUE,
                'post' => $post
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /*public function postComment_post()
    {
        $post_data = $this->input->post(); // PostID, UserID, Comment, CreatedAt, UpdatedAt
        if (!empty($post_data)) {
            $images_count = (isset($post_data['ImageCount']) && $post_data['ImageCount'] > 0 ? $post_data['ImageCount'] : 0);
            unset($post_data['ImageCount']);
            $inserted_id = $this->Post_comment_model->save($post_data);
            if ($inserted_id > 0) {
                // saving comment image
                for ($i = 1; $i <= $images_count; $i++) {
                    $file_name = $_FILES["Image" . $i]["name"];
                    $file_tmp = $_FILES["Image" . $i]["tmp_name"];
                    $compressed_file_name = $_FILES["CompressedImage" . $i]["name"];
                    $compressed_file_tmp = $_FILES["CompressedImage" . $i]["tmp_name"];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $compressed_ext = pathinfo($compressed_file_name, PATHINFO_EXTENSION);

                    $filename = basename($file_name, $ext);
                    $newFileName = $filename . $i . time() . "." . $ext;
                    move_uploaded_file($file_tmp, "uploads/posts/images/" . $newFileName);

                    // for compressed image
                    $compressed_filename = basename($compressed_file_name, $compressed_ext);
                    $compressed_newFileName = $compressed_filename . $i . time() . "." . $compressed_ext;
                    move_uploaded_file($compressed_file_tmp, "uploads/posts/images/compressed/" . $compressed_newFileName);

                    $post_comment_image_data['CommentID'] = $inserted_id;
                    $post_comment_image_data['PostID'] = $post_data['PostID'];
                    $post_comment_image_data['Image'] = "uploads/posts/images/" . $newFileName;
                    $post_comment_image_data['CompressedImage'] = "uploads/posts/images/compressed/" . $compressed_newFileName;
                    $this->Post_comment_image_model->save($post_comment_image_data);
                }

                $comment = $this->Post_comment_model->postComments($post_data['PostID'], false, false, $inserted_id);
                $comment = $comment[0];
                $post_images = $this->Post_comment_image_model->getMultipleRows(array('CommentID' => $comment['PostCommentID']), true);
                $comment['post_comment_images'] = ($post_images ? NullToEmpty($post_images) : array());
                $post_detail = $this->Post_model->get($post_data['PostID'], true, 'PostID');
                if ($post_detail['UserID'] !== $post_data['UserID']) {
                    // saving notification
                    $data['Type'] = 'comment';
                    $data['LoggedInUserID'] = $post_detail['UserID']; // the person whose post is this, who will receive this notification
                    $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                    $data['GroupID'] = 0;
                    $data['PostID'] = $post_data['PostID'];
                    $data['PostCommentID'] = $inserted_id;
                    $data['NotificationTypeID'] = 2;
                    $data['CreatedAt'] = time();
                    log_notification($data);
                    $user = $this->User_text_model->get($post_data['UserID'], true, 'UserID');
                    $notification_text = $user['FullName'] . " has commented on your post";
                    $res = sendNotification('Hobbies', $notification_text, $data, $post_detail['UserID']);
                }

                // send pusher call here
                pusher($comment, 'comment', 'Post_' . $post_data['PostID'], 'Post_' . $post_data['PostID'], false);

                $this->response([
                    'status' => TRUE,
                    'comment' => $comment
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => 'Comment not saved. Please try later.'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }*/

    public function postComment_post()
    {
        $post_data = $this->input->post(); // PostID, UserID, Comment, CreatedAt, UpdatedAt, MentionedUserID, MentionedUserName
        if (!empty($post_data)) {
            $images_count = (isset($post_data['ImageCount']) && $post_data['ImageCount'] > 0 ? $post_data['ImageCount'] : 0);
            unset($post_data['ImageCount']);
            $inserted_id = $this->Post_comment_model->save($post_data);
            if ($inserted_id > 0) {
                // saving comment image
                for ($i = 1; $i <= $images_count; $i++) {
                    $file_name = $_FILES["Image" . $i]["name"];
                    $file_tmp = $_FILES["Image" . $i]["tmp_name"];
                    $compressed_file_name = $_FILES["CompressedImage" . $i]["name"];
                    $compressed_file_tmp = $_FILES["CompressedImage" . $i]["tmp_name"];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $compressed_ext = pathinfo($compressed_file_name, PATHINFO_EXTENSION);

                    $filename = basename($file_name, $ext);
                    $newFileName = $filename . $i . time() . "." . $ext;
                    move_uploaded_file($file_tmp, "uploads/posts/images/" . $newFileName);

                    // for compressed image
                    $compressed_filename = basename($compressed_file_name, $compressed_ext);
                    $compressed_newFileName = $compressed_filename . $i . time() . "." . $compressed_ext;
                    move_uploaded_file($compressed_file_tmp, "uploads/posts/images/compressed/" . $compressed_newFileName);

                    $post_comment_image_data['CommentID'] = $inserted_id;
                    $post_comment_image_data['PostID'] = $post_data['PostID'];
                    $post_comment_image_data['Image'] = "uploads/posts/images/" . $newFileName;
                    $post_comment_image_data['CompressedImage'] = "uploads/posts/images/compressed/" . $compressed_newFileName;
                    $this->Post_comment_image_model->save($post_comment_image_data);
                }

                $comment = $this->Post_comment_model->postComments($post_data['PostID'], false, false, $inserted_id);
                $comment = $comment[0];
                $post_images = $this->Post_comment_image_model->getMultipleRows(array('CommentID' => $comment['PostCommentID']), true);
                $comment['CommentImage'] = ($post_images ? $post_images[0]['Image'] : '');
                $comment['CommentCompressedImage'] = ($post_images ? $post_images[0]['CompressedImage'] : '');
                $user_ids = explode(',', $comment['MentionedUserID']);
                $user_names = explode(',', $comment['MentionedUserName']);
                $users_info = array();
                $j = 0;
                foreach ($user_ids as $id) {

                    // saving notification
                    $data['Type'] = 'comment';
                    $data['LoggedInUserID'] = $id; // the person whose post is this, who will receive this notification
                    $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                    $data['GroupID'] = 0;
                    $data['PostID'] = $post_data['PostID'];
                    $data['PostCommentID'] = $inserted_id;
                    $data['NotificationTypeID'] = 10;
                    $data['CreatedAt'] = time();
                    log_notification($data);
                    $user = $this->User_text_model->get($post_data['UserID'], true, 'UserID');
                    $notification_text = $user['FullName'] . " has mentioned you in a comment";
                    $res = sendNotification('Hobbies', $notification_text, $data, $id);

                    //$users_info[] = $this->User_model->getUserInfoForComment($id);

                    $userInfoForComment = $this->User_model->getUserInfoForComment($id);
                    if (!empty($userInfoForComment)) {
                        // $users_info[$j] = $userInfoForComment;
                        $users_info[$j]['UserID'] = $userInfoForComment['UserID'];
                        $users_info[$j]['FullName'] = $userInfoForComment['FullName'];
                        $users_info[$j]['MentionedName'] = $user_names[$j];
                    }/*else{
                        $users_info[$j]['UserID'] = "";
                        $users_info[$j]['FullName'] = "";
                        $users_info[$j]['MentionedName'] = "";
                    }*/
                    $j++;
                }
                if (empty($users_info[0])) {
                    $comment['mentioed_users_info'] = array();
                } else {
                    $comment['mentioed_users_info'] = $users_info;
                }


                $post_detail = $this->Post_model->get($post_data['PostID'], true, 'PostID');
                if ($post_detail['UserID'] !== $post_data['UserID']) {
                    // saving notification
                    $data['Type'] = 'comment';
                    $data['LoggedInUserID'] = $post_detail['UserID']; // the person whose post is this, who will receive this notification
                    $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                    $data['GroupID'] = 0;
                    $data['PostID'] = $post_data['PostID'];
                    $data['PostCommentID'] = $inserted_id;
                    $data['NotificationTypeID'] = 2;
                    $data['CreatedAt'] = time();
                    log_notification($data);
                    $user = $this->User_text_model->get($post_data['UserID'], true, 'UserID');
                    $notification_text = $user['FullName'] . " has commented on your post";
                    $res = sendNotification('Hobbies', $notification_text, $data, $post_detail['UserID']);
                }

                // send pusher call here
                pusher($comment, 'comment', 'Post_' . $post_data['PostID'], 'Post_' . $post_data['PostID'], false);

                $this->response([
                    'status' => TRUE,
                    'comment' => $comment
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => "Comment not saved. Please try later \n التعليق لم يتم حفظه. يرجى المحاولة في وقت لاحق"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }


    public function getGroupData_get()
    {
        $post_data = $this->input->get(); // user_id
        if (!empty($post_data)) {

            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            if (isset($post_data['Language'])) {
                $language = $post_data['Language'];
            } else {
                $language = 'EN';
            }

            if ($this->checkUserExist($post_data['LoggedInUserID'])) {
                $group = $this->Group_model->getGroupsDate('groups.GroupID = ' . $post_data['GroupID'] . '');
                if (!empty($group)) {
                    $check_is_group_member = array();
                    $check_is_group_member['GroupID'] = $post_data['GroupID'];
                    $check_is_group_member['UserID'] = $post_data['LoggedInUserID'];


                    $member_data = $this->Group_member_model->getWithMultipleFields($check_is_group_member);
                    if ($member_data) {
                        $group[0]['IsMember'] = 'yes';
                        $group[0]['IsAdmin'] = $member_data->IsAdmin;
                    } else {
                        $group[0]['IsMember'] = 'no';
                        $group[0]['IsAdmin'] = 'no';
                    }


                    $group_post = $this->Post_model->getGroupPosts($post_data['GroupID'], $start, $limit);
                    if ($group_post) {
                        $return_post = array();
                        foreach ($group_post as $post) {

                            $post['IsLiked'] = 'no';

                            $check_already_like_post = array();
                            $check_already_like_post['UserID'] = $post_data['LoggedInUserID'];
                            $check_already_like_post['PostID'] = $post['PostID'];

                            $check_like = $this->Post_like_model->getWithMultipleFields($check_already_like_post);
                            if ($check_like) {
                                $post['IsLiked'] = 'yes';
                            }

                            $post['PostComment'] = 0;
                            $post_comments = $this->Post_comment_model->getMultipleRows(array('PostID' => $post['PostID']), true);
                            if ($post_comments) {
                                $post['PostComment'] = count($post_comments);
                            }

                            $post['PostLikes'] = 0;
                            $post_likes = $this->Post_like_model->getMultipleRows(array('PostID' => $post['PostID']), true);
                            if ($post_likes) {
                                $post['PostLikes'] = count($post_likes);
                            }

                            $images = $this->Post_image_model->getMultipleRows(array('PostID' => $post['PostID']), true);

                            if ($images) {
                                $post['Images'] = $images;
                            } else {
                                $post['Images'] = array();;
                            }

                            $videos = $this->Post_video_model->getMultipleRows(array('PostID' => $post['PostID']), true);
                            if ($videos) {
                                $post['Videos'] = $videos;
                            } else {
                                $post['Videos'] = array();;
                            }

                            $return_post[] = $post;

                        }


                        $group[0]['Posts'] = $return_post;
                    } else {
                        $group[0]['Posts'] = array();
                    }

                }


                if (!empty($group)) {
                    $this->response([
                        'status' => TRUE,
                        'group_info' => NullToEmpty($group[0])
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => "Something went worng \n ذهب شيء ما"
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "User not found \n المستخدم ليس موجود"
                ], REST_Controller::HTTP_OK);
            }


        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getGroupMembers_get()
    {
        $post_data = $this->input->get(); // GroupID, LoggedinUserID
        if (!empty($post_data)) {

            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $group_info = $this->Group_member_model->getGroupInfo($post_data['GroupID']);
            if ($group_info) {
                $group_members = $this->Group_member_model->getGroupMembers($post_data['GroupID'], $start, $limit);

                // checking if logged in user is admin of this group or not
                $checkIfIsAdmin = $this->Group_member_model->getWithMultipleFields(array('UserID' => $post_data['LoggedinUserID'], 'GroupID' => $post_data['GroupID'], 'IsAdmin' => 'yes'));
                if ($checkIfIsAdmin) {
                    $isAdmin = 'yes';
                } else {
                    $isAdmin = 'no';
                }

                $group_info['IsAdmin'] = $isAdmin;

                $this->response([
                    'status' => TRUE,
                    'group_info' => $group_info,
                    'group_members' => $group_members
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

            }

        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }


    public function likePost_post() // same api is being used for dislike also
    {
        $post_data = $this->input->post(); // PostID, UserID , Type (Like, Dislike)
        $type = $post_data['Type'];
        unset($post_data['Type']);
        if (!empty($post_data)) {
            if (isset($type) && strtolower($type) == 'unlike') {
                $this->Post_like_model->delete($post_data);
                $message = "Post unliked successfully \n تم حذف المشاركة بنجاح";
            } else {
                $this->Post_like_model->save($post_data);
                $message = "Post liked successfully \n تم النشر بنجاح";

                $post_detail = $this->Post_model->get($post_data['PostID'], true, 'PostID');
                if ($post_detail['UserID'] !== $post_data['UserID']) {
                    // saving notification
                    $data['Type'] = 'like';
                    $data['LoggedInUserID'] = $post_detail['UserID']; // the person whose post is this, who will receive this notification
                    $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                    $data['GroupID'] = 0;
                    $data['PostID'] = $post_data['PostID'];
                    $data['PostCommentID'] = 0;
                    $data['NotificationTypeID'] = 1;
                    $data['CreatedAt'] = time();
                    log_notification($data);
                    $user = $this->User_text_model->get($post_data['UserID'], true, 'UserID');
                    $notification_text = $user['FullName'] . " has liked your post";
                    $res = sendNotification('Hobbies', $notification_text, $data, $post_detail['UserID']);
                }

            }
            $this->response([
                'status' => TRUE,
                'message' => $message
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }


    public function reportPost_post()
    {
        $post_data = $this->input->post(); // PostID, UserID, ReportReason, ReportedOn
        if (!empty($post_data)) {
            $this->Post_report_model->save($post_data);
            $this->response([
                'status' => TRUE,
                'message' => "Post reported successfully \n تم نشر المشاركة بنجاح"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }

    public function deleteGroup_post()
    {
        $post_data = $this->input->post(); // GroupID, UserID
        if (!empty($post_data)) {
            if ($this->checkIfUserIsAdmin($post_data['GroupID'], $post_data['UserID'])) {
                $this->Group_text_model->delete(array('GroupID' => $post_data['GroupID']));
                $this->Group_model->delete(array('GroupID' => $post_data['GroupID']));
                $this->response([
                    'status' => TRUE,
                    'message' => "Group Deleted Successfully \n المجموعة المحذوفة بنجاح"
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => "User is not an admin of this group \n المستخدم ليس مسؤولاً عن هذه المجموعة"
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    /*public function getGroups_get()
    {

        $where = false;
        $first = false;
        $post_data = $this->input->get();
        if (isset($post_data['CategoryID'])) {
            $first = true;
            $where = 'groups.CategoryID = ' . $post_data['CategoryID'];
        }


        if (isset($post_data['SubCategoryID'])) {
            if ($first) {
                $where .= ' AND groups.SubCategoryID = ' . $post_data['SubCategoryID'];
            } else {
                $where = 'groups.SubCategoryID = ' . $post_data['SubCategoryID'];
            }

        }


        $start = 0;
        $limit = 10;

        if (isset($post_data['Start'])) {
            $start = $post_data['Start'];
        }
        if (isset($post_data['Limit'])) {
            $limit = $post_data['Limit'];
        }


        $groups = $this->Group_model->getGroupsDate($where, $start, $limit);

        if ($groups) {

            $this->response([
                'status' => TRUE,
                'groups' => $groups
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Groups not found'
            ], REST_Controller::HTTP_OK);
        }


    }*/

    public function getGroups_get()
    {
        $post_data = $_REQUEST; // CategoryID, SubCategoryID, CityID, Text, Start, Limit
        $where = '1=1';

        if (isset($post_data['CategoryID'])) {
            $CategoryID = $post_data['CategoryID'];
            $where .= " AND groups.CategoryID = $CategoryID";
        }

        if (isset($post_data['SubCategoryID'])) {
            $SubCategoryID = $post_data['SubCategoryID'];
            $where .= " AND groups.SubCategoryID = $SubCategoryID";
        }

        if (isset($post_data['CityID']) && $post_data['CityID'] > 0) {
            $CityID = $post_data['CityID'];
            $where .= " AND groups.GroupCityID = $CityID";
        }

        if (isset($post_data['Text']) && $post_data['Text'] != '') {
            $text = $post_data['Text'];
            $where .= " AND groups_text.Title LIKE '%$text%' ";
        }

        $start = 0;
        $limit = 10;

        if (isset($post_data['Start'])) {
            $start = $post_data['Start'];
        }
        if (isset($post_data['Limit'])) {
            $limit = $post_data['Limit'];
        }


        $groups = $this->Group_model->getGroupsDate($where, $start, $limit);

        if ($groups) {

            $this->response([
                'status' => TRUE,
                'groups' => $groups
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "Groups not found \n مجموعات غير موجودة"
            ], REST_Controller::HTTP_OK);
        }

    }


    /*public function joinGroup_post()
    {
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {
            if($this->checkUserExist($post_data['UserID'])){
            $check_user = $this->Group_member_model->getWithMultipleFields($post_data, true);


            if (empty($check_user)) {
                $post_data['CreatedAt'] = time();
                $insert_id = $this->Group_member_model->save($post_data);

                $this->response([
                    'status' => TRUE,
                    'message' => 'Group joined successfully'
                ], REST_Controller::HTTP_OK);
            } else {
                
                $update_by = array();
                $update_by['GroupID'] = $post_data['GroupID'];
                $update_by['UserID']  = $post_data['UserID'];
                
                $this->Group_member_model->update($post_data,$update_by);
                
                
                $this->response([
                    'status' => FALSE,
                    'message' => 'Updated Successfully'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            $this->response([
                    'status' => FALSE,
                    'message' => "User not found \n المستخدم ليس موجود"
                ], REST_Controller::HTTP_OK);
        }


        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }*/

    public function joinGroup_post()
    {
        $post_data = $this->input->post(); // UserID, GroupID, IsAmin
        if (!empty($post_data)) {
            if ($this->checkUserExist($post_data['UserID'])) {
                // checking if this user is already added to this group
                $check_user = $this->Group_member_model->getWithMultipleFields(array('UserID' => $post_data['UserID'], 'GroupID' => $post_data['GroupID']), true);
                if ($check_user) { // if already added to group
                    if (isset($post_data['IsAdmin'])) // if IsAdmin key is being posted
                    {
                        $update_by['GroupID'] = $post_data['GroupID'];
                        $update_by['UserID'] = $post_data['UserID'];
                        $this->Group_member_model->update(array('IsAdmin' => $post_data['IsAdmin']), $update_by);
                        if (strtolower($post_data['IsAdmin']) == 'yes') {
                            $msg = "User assigned as admin successfully \n تم تعيين المستخدم كمشرف بنجاح";
                        } elseif (strtolower($post_data['IsAdmin']) == 'no') {
                            $msg = "User removed as admin from this group successfully \n تمت إزالة المستخدم كمسؤول من هذه المجموعة بنجاح";
                        }
                        $this->response([
                            'status' => TRUE,
                            'message' => $msg
                        ], REST_Controller::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => TRUE,
                            'message' => "User is already a member of this group \n المستخدم بالفعل عضو في هذه المجموعة"
                        ], REST_Controller::HTTP_OK);
                    }
                } else { // if not already added to group

                    $group_detail = $this->Group_model->get($post_data['GroupID'], true, 'GroupID');

                    if ($group_detail['UserID'] !== $post_data['UserID']) {
                        // saving notification
                        $data['Type'] = 'group_join';
                        $data['LoggedInUserID'] = $group_detail['UserID']; // the person whose project is this, who will receive this notification
                        $data['UserID'] = $post_data['UserID']; // the person who is making this notification
                        $data['GroupID'] = $post_data['GroupID'];
                        $data['PostID'] = 0;
                        $data['PostCommentID'] = 0;
                        $data['NotificationTypeID'] = 5;
                        $data['CreatedAt'] = time();
                        log_notification($data);
                        $user = $this->User_text_model->get($post_data['UserID'], true, 'UserID');
                        $notification_text = $user['FullName'] . " has joined your group";
                        $res = sendNotification('Hobbies', $notification_text, $data, $group_detail['UserID']);
                    }

                    $post_data['CreatedAt'] = time();
                    $this->Group_member_model->save($post_data);
                    $this->response([
                        'status' => TRUE,
                        'message' => "Group joined successfully \n انضم المجموعة بنجاح"
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "User not found \n المستخدم ليس موجود"
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function leaveGroup_post()
    {
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {
            if ($this->checkUserExist($post_data['UserID'])) {
                if (1 == 1 || $this->checkIfUserIsAdmin($post_data['GroupID'], $post_data['UserID'])) { // bypassing check
                    $user_group_info = $this->Group_member_model->getWithMultipleFields($post_data);
                    // checking if this user is member of this group
                    if ($user_group_info) {
                        // if user leaving is group admin
                        if ($user_group_info->IsAdmin == 'yes') {
                            // delete him from group
                            // $this->Group_member_model->delete($post_data);
                            $fetch_by = array();
                            $fetch_by['IsAdmin'] = 'yes';
                            $fetch_by['GroupID'] = $post_data['GroupID'];

                            // check if there are any other admins of this group
                            $users = $this->Group_member_model->getMultipleRows($fetch_by);

                            // if no admins are there
                            if (!$users) {
                                // get the oldest member and make him admin
                                $old_member = $this->Group_member_model->getOldestGroupMember($post_data['GroupID']);
                                // print_rm($old_member);
                                if (!empty($old_member)) {
                                    $update = array();
                                    $update_by = array();


                                    $update['IsAdmin'] = 'yes';

                                    $update_by['UserID'] = $old_member['UserID'];
                                    $update_by['GroupID'] = $old_member['GroupID'];

                                    $this->Group_member_model->update($update, $update_by);

                                }
                            }
                        }

                        // Bilal work here on 19-10-2018
                        // removing user from group
                        $this->Group_member_model->delete($post_data);
                        $this->response([
                            'status' => TRUE,
                            'message' => "Left group successfully \n المجموعة اليسرى بنجاح"
                        ], REST_Controller::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => TRUE,
                            'message' => "This user is not a member of this group \n هذا المستخدم ليس عضوًا في هذه المجموعة"
                        ], REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response([
                        'status' => TRUE,
                        'message' => "User is not an admin of this group \n المستخدم ليس مسؤولاً عن هذه المجموعة"
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => "User not found \n المستخدم ليس موجود"
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function reportGroup_post()
    {
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {
            if ($this->checkUserExist($post_data['UserID'])) {
                $post_data['CreatedAt'] = time();
                $insert_id = $this->Report_group_model->save($post_data);


                if ($insert_id > 0) {


                    $this->response([
                        'status' => TRUE,
                        'message' => "Group reported successfully \n أبلغت المجموعة بنجاح"
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => "Something went wrong \n هناك خطأ ما"
                    ], REST_Controller::HTTP_OK);
                }

            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "User not found \n المستخدم ليس موجود"
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function sharePost_post()
    {
        $post_data = $this->input->post(); // PostID, UserID, SharedAt
        if (!empty($post_data)) {
            if ($this->checkUserExist($post_data['UserID'])) {
                $this->Post_share_model->save($post_data);
                $this->response([
                    'status' => TRUE,
                    'message' => "Post shared successfully \n تمت مشاركة المشاركة بنجاح"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => "User not found \n المستخدم ليس موجود"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "Something went wrong \n هناك خطأ ما"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }

    /*public function getPostComments_get()
    {
        $post_data = $_REQUEST; // PostID, Start, Limit
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $post_comments = $this->Post_comment_model->postComments($post_data['PostID'], $start, $limit);

            $i = 0;
            foreach ($post_comments as $post_comment) {
                $post_images = $this->Post_comment_image_model->getMultipleRows(array('CommentID' => $post_comment['PostCommentID']), true);
                $post_comments[$i]['post_comment_images'] = ($post_images ? NullToEmpty($post_images) : array());
                $i++;
            }

            $post['post_comments_count'] = ($post_comments ? count($post_comments) : 0);
            $post['post_comments'] = ($post_comments ? NullToEmpty($post_comments) : array());
            $this->response([
                'status' => TRUE,
                'comments' => $post
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }*/

    public function getPostComments_get()
    {
        $post_data = $_REQUEST; // PostID, Start, Limit
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $post_comments = $this->Post_comment_model->postComments($post_data['PostID'], $start, $limit);

            $i = 0;
            foreach ($post_comments as $post_comment) {
                $user_ids = explode(',', $post_comment['MentionedUserID']);
                $user_names = explode(',', $post_comment['MentionedUserName']);
                $users_info = array();
                $j = 0;
                foreach ($user_ids as $id) {
                    $userInfoForComment = $this->User_model->getUserInfoForComment($id);
                    if (!empty($userInfoForComment)) {
                        // $users_info[$j] = $userInfoForComment;
                        $users_info[$j]['UserID'] = $userInfoForComment['UserID'];
                        $users_info[$j]['FullName'] = $userInfoForComment['FullName'];
                        $users_info[$j]['MentionedName'] = $user_names[$j];
                    }/*else{
                        $users_info[$j]['UserID'] = "";
                        $users_info[$j]['FullName'] = "";
                        $users_info[$j]['MentionedName'] = "";
                    }*/
                    $j++;
                }


                if (empty($users_info[0])) {
                    $post_comments[$i]['mentioed_users_info'] = array();
                } else {
                    $post_comments[$i]['mentioed_users_info'] = $users_info;
                }

                $post_images = $this->Post_comment_image_model->getMultipleRows(array('CommentID' => $post_comment['PostCommentID']), true);
                $post_comments[$i]['CommentImage'] = ($post_images ? $post_images[0]['Image'] : '');
                $post_comments[$i]['CommentCompressedImage'] = ($post_images ? $post_images[0]['CompressedImage'] : '');
                $i++;
            }

            $post['post_comments_count'] = ($post_comments ? count($post_comments) : 0);
            $post['post_comments'] = ($post_comments ? NullToEmpty($post_comments) : array());
            $this->response([
                'status' => TRUE,
                'comments' => $post
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }


    public function follow_post() // being used for both following and unfollowing
    {
        $post_data = $this->input->post(); // Follower,Following (both are ids of users), Follow (1 for follow, 0 for unfollow)
        if (!empty($post_data)) {
            $follow = $post_data['Follow'];
            unset($post_data['Follow']);
            if (isset($follow) && $follow == 0) { // for unfollowing case
                $this->Follower_model->delete($post_data);
                $this->response([
                    'status' => TRUE,
                    'message' => "Unfollowed successfully \n تم الإيقاف بنجاح"
                ], REST_Controller::HTTP_OK);
            } else { // for following case
                $insert_id = $this->Follower_model->save($post_data);

                // saving notification
                $data['Type'] = 'follow';
                $data['LoggedInUserID'] = $post_data['Following']; // the person who will receive this notification
                $data['UserID'] = $post_data['Follower']; // the person who is making this notification
                $data['GroupID'] = 0;
                $data['PostID'] = 0;
                $data['PostCommentID'] = 0;
                $data['NotificationTypeID'] = 4;
                $data['CreatedAt'] = time();
                log_notification($data);
                $user = $this->User_text_model->get($post_data['Follower'], true, 'UserID');
                $notification_text = $user['FullName'] . " started following you";
                $res = sendNotification('Hobbies', $notification_text, $data, $post_data['Following']);

                if ($insert_id > 0) {
                    $this->response([
                        'status' => TRUE,
                        'message' => "Followed successfully \n يتبع بنجاح"
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => "Something went wrong \n هناك خطأ ما"
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            }


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }

    public function forgotPassword_get()
    {
        $post_data = $_REQUEST; // Email
        if (!empty($post_data)) {
            $fetch_by['Email'] = $post_data['Email'];
            $user = $this->User_model->getWithMultipleFields($fetch_by);
            if ($user) {
                $new_pass = RandomAlphaNumericString(10);
                $update['Password'] = md5($new_pass);
                $update_by['UserID'] = $user->UserID;
                $this->User_model->update($update, $update_by);
                $mail_sent = $this->sendForgotPasswordEmail($user, $new_pass);
                if ($mail_sent) {
                    $this->response([
                        'status' => TRUE,
                        'message' => "Your new password is sent at your registered email address \n يتم إرسال كلمة المرور الجديدة الخاصة بك على عنوان البريد الإلكتروني المسجل الخاص بك"
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "No user found with this email \n لم يتم العثور على مستخدم بهذا البريد الإلكتروني"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }

    }

    public function changePassword_get()
    {
        $post_data = $_REQUEST; // UserID, OldPassword, NewPassword
        if (!empty($post_data)) {
            $UserID = $post_data['UserID'];
            $old_password_posted = md5($post_data['OldPassword']);
            $new_password = $post_data['NewPassword'];
            $user_detail = $this->User_model->get($UserID, true, 'UserID');
            $old_password_from_db = $user_detail['Password'];
            if ($old_password_posted != $old_password_from_db) {
                $this->response([
                    'status' => FALSE,
                    'message' => "Wrong old password provided \n كلمة المرور القديمة خاطئة"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $update_by['UserID'] = $UserID;
                $update['Password'] = md5($new_password);
                $this->User_model->update($update, $update_by);
                $this->response([
                    'status' => TRUE,
                    'message' => "Password changed successfully. Please use the new password next time you login \n تم تغيير الرقم السري بنجاح. يرجى استخدام كلمة المرور الجديدة في المرة القادمة التي تقوم فيها بتسجيل الدخول"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }


    public function showFollowing_get()
    {
        $post_data = $_REQUEST; // UserID, Search
        if (!empty($post_data)) {

            $start = 0;
            $limit = 10;
            $search = false;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }
            if (isset($post_data['Search']) && $post_data['Search'] != '') {
                $search = $post_data['Search'];
            }

            $response['total_following'] = $this->Follower_model->getTotalFollowing($post_data['UserID']);
            $response['following'] = $this->Follower_model->getFollowing($post_data['UserID'], $start, $limit, $search);

            if ($response['following']) {
                $i = 0;
                foreach ($response['following'] as $user) {
                    $following = 0;
                    $being_followed = 0;
                    $checkIfFollow = $this->Follower_model->getWithMultipleFields(array('Follower' => $post_data['UserID'], 'Following' => $user['UserID']));
                    $checkIfFollowing = $this->Follower_model->getWithMultipleFields(array('Follower' => $user['UserID'], 'Following' => $post_data['UserID']));
                    if ($checkIfFollow) {
                        $following = 1;
                    }
                    if ($checkIfFollowing) {
                        $being_followed = 1;
                    }
                    $response['following'][$i]['Follower'] = $following;
                    $response['following'][$i]['Following'] = $being_followed;
                    $i++;
                }
            } else {
                $response['following'] = array();
            }

            $this->response([
                'status' => TRUE,
                'data' => $response
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }

    public function showFollowers_get()
    {
        $post_data = $_REQUEST; // UserID, Search
        if (!empty($post_data)) {

            $start = 0;
            $limit = 10;
            $search = false;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }
            if (isset($post_data['Search']) && $post_data['Search'] != '') {
                $search = $post_data['Search'];
            }

            $response['total_follower'] = $this->Follower_model->getTotalFollower($post_data['UserID']);
            $response['followers'] = $this->Follower_model->getFollower($post_data['UserID'], $start, $limit, $search);

            if ($response['followers']) {
                $i = 0;
                foreach ($response['followers'] as $user) {
                    $following = 0;
                    $being_followed = 0;
                    $checkIfFollow = $this->Follower_model->getWithMultipleFields(array('Follower' => $post_data['UserID'], 'Following' => $user['UserID']));
                    $checkIfFollowing = $this->Follower_model->getWithMultipleFields(array('Follower' => $user['UserID'], 'Following' => $post_data['UserID']));
                    if ($checkIfFollow) {
                        $following = 1;
                    }
                    if ($checkIfFollowing) {
                        $being_followed = 1;
                    }
                    $response['followers'][$i]['Follower'] = $following;
                    $response['followers'][$i]['Following'] = $being_followed;
                    $i++;
                }
            } else {
                $response['followers'] = array();
            }

            $this->response([
                'status' => TRUE,
                'data' => $response
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }

    public function inviteGroup_post() // This api is used to invite people to groups
    {
        $post_data = $this->input->post(); // GroupID, UserID (User who is inviting), InvitedUserID (User who is being invited), CreatedAt
        if (!empty($post_data)) {
            $saved_id = $this->Group_invite_model->save($post_data);
            if ($saved_id > 0) {
                $this->response([
                    'status' => TRUE,
                    'message' => "Group invitation sent successfully \n تم إرسال دعوة المجموعة بنجاح"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => "Group invitation failed to be sent \n فشل إرسال الدعوة الجماعية"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function invitePeople_get() // This api is to get which of my followers are invited to this group already
    {
        $post_data = $_REQUEST; // GroupID, UserID
        if (!empty($post_data)) {

            $start = 0;
            $limit = 10;
            $search = false;
            $invitePeopleArr = array();

            $response['followers'] = $this->Follower_model->getFollower($post_data['UserID'], $start, $limit, $search);
            //dump($response['followers']);
            if ($response['followers']) {
                $i = 0;
                foreach ($response['followers'] as $user) {
                    // filtering all those followers which are already group members
                    $alreadyAdded = $this->Group_member_model->getWithMultipleFields(array('GroupID' => $post_data['GroupID'], 'UserID' => $user['UserID']));
                    if (!$alreadyAdded) {
                        // checking here if this user is already invited or not
                        $IsAlreadyInvited = 0;
                        $checkIfAlreadyInvited = $this->Group_invite_model->getWithMultipleFields(array('GroupID' => $post_data['GroupID'], 'InvitedUserID' => $user['UserID']));
                        if ($checkIfAlreadyInvited) {
                            $IsAlreadyInvited = 1;
                        }
                        $invitePeopleArr[$i] = $user;
                        $invitePeopleArr[$i]['IsAlreadyInvited'] = $IsAlreadyInvited;
                        $i++;
                    }
                }
            } else {
                $invitePeopleArr = array();
            }

            $this->response([
                'status' => TRUE,
                'data' => $invitePeopleArr
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }

    public function getNotifications_get()
    {
        $post_data = $_REQUEST; // UserID, Start, Limit, Language
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $notifications = $this->User_notification_model->getNotifications($post_data['UserID'], $start, $limit, $post_data['Language']);
            if (count($notifications) > 0) {
                $i = 0;
                foreach ($notifications as $notification) {
                    $notifications[$i]['PostImage'] = '';
                    $post_image = $this->Post_image_model->get($notification['PostID'], true, 'PostID');
                    if ($post_image) {
                        $notifications[$i]['PostImage'] = $post_image['CompressedImage'];
                    }

                    // follow, followers work here
                    $following = 0;
                    $being_followed = 0;
                    if ($notification['Type'] == 'follow') {
                        $checkIfFollow = $this->Follower_model->getWithMultipleFields(array('Follower' => $notification['LoggedInUserID'], 'Following' => $notification['UserID']));
                        $checkIfFollowing = $this->Follower_model->getWithMultipleFields(array('Follower' => $notification['UserID'], 'Following' => $notification['LoggedInUserID']));
                        if ($checkIfFollow) {
                            $following = 1;
                        }
                        if ($checkIfFollowing) {
                            $being_followed = 1;
                        }
                    }
                    $notifications[$i]['Follower'] = $following;
                    $notifications[$i]['Following'] = $being_followed;
                    $i++;
                }
            }
            $this->response([
                'status' => TRUE,
                'notifications' => NullToEmpty($notifications)
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getChatRooms_get()
    {
        $post_data = $_REQUEST; // UserID
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }
            // getting all chat rooms for this user
            $chatRooms = $this->Chat_model->getChatRooms($post_data['UserID'], $start, $limit);

            // dump($chatRooms);

            if ($chatRooms) {
                // getting last message for each chat
                $i = 0;
                foreach ($chatRooms as $chatRoom) {
                    $message = $this->Chat_message_model->getLastMessageForThisChatRoom($chatRoom['ChatID']);
                    $chatRooms[$i]['ChatMessageID'] = $message['ChatMessageID'];
                    $chatRooms[$i]['SenderImage'] = $message['SenderImage'];
                    $chatRooms[$i]['ReceiverImage'] = $message['ReceiverImage'];
                    $chatRooms[$i]['SenderID'] = $message['SenderID'];
                    $chatRooms[$i]['ReceiverID'] = $message['ReceiverID'];
                    $chatRooms[$i]['SenderName'] = $message['SenderName'];
                    $chatRooms[$i]['ReceiverName'] = $message['ReceiverName'];
                    $chatRooms[$i]['Message'] = $message['Message'];
                    $chatRooms[$i]['CreatedAt'] = $message['CreatedAt'];
                    //$unread_msg = $this->Chat_message_model->getUnreadMsgCountForChat($chatRoom['ChatID']);
                    $unread_msg_count = $this->Chat_message_model->getRowsCount(array('ChatID' => $chatRoom['ChatID'], 'ReceiverID' => $post_data['UserID'], 'IsReadByReceiver' => 'no'));
                    $chatRooms[$i]['HasUnreadMessage'] = ($unread_msg_count > 0 ? 'yes' : 'no');
                    $chatRooms[$i]['UnreadMessageCount'] = $unread_msg_count;
                    $i++;
                }
                // dump($chatRooms);
                $this->response([
                    'status' => TRUE,
                    'ChatRooms' => $chatRooms
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => TRUE,
                    'ChatRooms' => array()
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function startChat_get()
    {
        $post_data = $_REQUEST; // SenderID, ReceiverID
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $chat_messages = $this->Chat_message_model->getChatMessages($post_data['SenderID'], $post_data['ReceiverID'], $start, $limit);
            if ($chat_messages) // if chat exist then get all messages of this chat and send in response
            {
                $this->response([
                    'status' => TRUE,
                    'ChatID' => $chat_messages[0]['ChatID'],
                    'ChatMessages' => $chat_messages
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                // create a new chat
                $chat_data['Type'] = 'Single';
                $chat_data['LastActivityAt'] = time();
                $chat_id = $this->Chat_model->save($chat_data);
                $this->response([
                    'status' => TRUE,
                    'ChatID' => (string)$chat_id,
                    'ChatMessages' => array()
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function deleteChatRoom_get()
    {
        $post_data = $_REQUEST; // ChatID
        if (!empty($post_data)) {
            $msg_count = $this->Chat_message_model->getRowsCount(array('ChatID' => $post_data['ChatID']));
            if ($msg_count > 0) {
                // do nothing as there are already messages within this chat room
                $this->response([
                    'status' => FALSE,
                    'message' => "Chat room can not be deleted as there are messages in this chat room \n لا يمكن حذف غرفة الدردشة نظرًا لوجود رسائل في غرفة الدردشة هذه"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $deleted_by['ChatID'] = $post_data['ChatID'];
                $this->Chat_model->delete($deleted_by);
                $this->response([
                    'status' => FALSE,
                    'message' => "Chat room deleted successfully \n تم حذف غرفة الدردشة بنجاح"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function sendMessage_post()
    {
        $post_data = $this->input->post(); // SenderID, ReceiverID, Message, CreatedAt, ChatID
        if (!empty($post_data)) {
            $post_data['IsReadBySender'] = 'yes';
            $post_data['IsReadByReceiver'] = 'no';
            $chat_message_id = $this->Chat_message_model->save($post_data);
            if ($chat_message_id > 0) // if chat message is saved successfully
            {
                // getting this msg detail
                $message = $this->Chat_message_model->getMessageDetail($chat_message_id);

                // updating last activity Time for chat
                $chat_data['LastActivityAt'] = time();

                // send pusher call here
                pusher($message, 'message', 'Chat_' . $post_data['ReceiverID'], 'Chat_' . $post_data['ReceiverID'], false);

                $this->Chat_model->update($chat_data, array('ChatID' => $post_data['ChatID']));


                /*// send notification to receiver
                $data['Type'] = 'message';
                $data['LoggedInUserID'] = $post_data['ReceiverID']; // the person who will receive this notification
                $data['UserID'] = $post_data['SenderID']; // the person who is making this notification
                $data['GroupID'] = 0;
                $data['PostID'] = 0;
                $data['PostCommentID'] = 0;
                $data['NotificationTypeID'] = 11;
                $data['CreatedAt'] = time();
                // log_notification($data);
                $user = $this->User_text_model->get($post_data['SenderID'], true, 'UserID');
                $notification_text = $user['FullName'] . " has sent you a message";
                $res = sendNotification('Hobbies', $notification_text, $data, $post_data['ReceiverID']);*/

                $this->response([
                    'status' => TRUE,
                    'message' => $message
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => "Message sending failed \n فشل ارسال الرسالة"
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function markAllMsgReadForChat_get()
    {
        $post_data = $_REQUEST; // ChatID, UserID
        if (!empty($post_data)) {
            $chat_msg_data['IsReadByReceiver'] = 'yes';
            $this->Chat_message_model->updateWhere($chat_msg_data, array('ChatID' => $post_data['ChatID'], 'ReceiverID' => $post_data['UserID']));
            $this->response([
                'status' => TRUE,
                'message' => "All messages marked as read for this chat \n جميع الرسائل وضعت عليها علامة للقراءة لهذه الدردشة",
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function checkIfUnreadMsgForUser_get()
    {
        $post_data = $_REQUEST; // UserID
        if (!empty($post_data)) {
            $unread_msg_count = $this->Chat_message_model->getRowsCount(array('ReceiverID' => $post_data['UserID'], 'IsReadByReceiver' => 'no'));
            $this->response([
                'status' => TRUE,
                'UnreadMessageCount' => $unread_msg_count,
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function getAllBadges_get()
    {
        $badges = $this->Badge_model->getJoinedData(true, 'BadgeID', 'badges.IsActive = 1');
        $this->response([
            'status' => TRUE,
            'badges' => $badges
        ], REST_Controller::HTTP_OK);
    }

    public function applyForBadge_post()
    {
        $post_data = $this->input->post(); // UserID, BadgeID, AttachmentsCount, Attachment1, Attachment2...
        if (!empty($post_data)) {
            $attachment_count = (isset($post_data['AttachmentsCount']) && $post_data['AttachmentsCount'] > 0 ? $post_data['AttachmentsCount'] : 0);
            unset($post_data['AttachmentsCount']);
            $inserted_id = $this->Badge_request_model->save($post_data);
            if ($inserted_id > 0) {
                for ($i = 1; $i <= $attachment_count; $i++) {
                    $file_name = $_FILES["Attachment" . $i]["name"];
                    $file_tmp = $_FILES["Attachment" . $i]["tmp_name"];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);

                    $filename = basename($file_name, $ext);
                    $newFileName = $filename . $i . time() . "." . $ext;
                    move_uploaded_file($file_tmp, "uploads/badge_request_attachments/" . $newFileName);

                    $badge_attachment_data['BadgeRequestID'] = $inserted_id;
                    $badge_attachment_data['Attachment'] = "uploads/badge_request_attachments/" . $newFileName;
                    $this->Badge_request_attachments_model->save($badge_attachment_data);
                }

                $this->response([
                    'status' => TRUE,
                    'message' => "Thank you for applying, we will review your request and confirm \n نشكرك على التقديم ، سنراجع طلبك ونؤكده",
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => "Badge request failed. Please try again later \n فشل طلب الشارات. الرجاء معاودة المحاولة في وقت لاحق",
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function suggestHobby_post()
    {
        $post_data = $this->input->post(); // UserID, HobbyName, Description, CategoryID
        if (!empty($post_data)) {
            $post_data['CreatedAt'] = time();
            $inserted_id = $this->Suggested_hobbies_model->save($post_data);
            if ($inserted_id > 0) {
                $this->response([
                    'status' => TRUE,
                    'message' => "Thank you for suggesting the hobby, we will review your request and confirm \n نشكرك على اقتراحك للهواية ، وسنراجع طلبك ونؤكد لك ذلك",
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => "Hobby suggestion failed. Please try again later \n فشل اقتراح هواية. الرجاء معاودة المحاولة في وقت لاحق",
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function checkIfAlreadyApplied_get()
    {
        $post_data = $_REQUEST; // UserID, Type (hobby, badge)
        if (!empty($post_data)) {
            $check = true;
            if ($post_data['Type'] == 'hobby') {
                $check = $this->Suggested_hobbies_model->getWithMultipleFields(array('UserID' => $post_data['UserID'], 'Status' => 'Pending'));
            } elseif ($post_data['Type'] == 'badge') {
                $check = $this->Badge_request_model->getWithMultipleFields(array('UserID' => $post_data['UserID'], 'Status' => 'Pending'));
            }
            if ($check) {
                $this->response([
                    'status' => TRUE,
                    'message' => "A request is already pending \n طلب معلق بالفعل",
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "All OK. Nothing applied before \n كل شيء جيد. لا شيء يطبق من قبل",
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function applyForPlatinum_get()
    {
        // we are using this API for temporary purpose for now, later we will use structure developed by sarfraz bhai sendPackageUpgradeRequest_post
        $post_data = $_REQUEST; // UserID
        if (!empty($post_data)) {
            $checkIfAlreadyApplied = $this->Model_general->getSingleRow('account_upgrade_requests_new', $post_data);
            if ($checkIfAlreadyApplied) {
                $this->response([
                    'status' => FALSE,
                    'message' => "You have already applied for this and your request is pending \n لقد قمت بالفعل بتقديم طلب لهذا وكان طلبك معلقًا",
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $post_data['CreatedAt'] = time();
                $inserted_id = $this->Model_general->save('account_upgrade_requests_new', $post_data);
                if ($inserted_id > 0) {
                    $this->response([
                        'status' => TRUE,
                        'message' => "Account upgrade request sent successfully \n تم إرسال طلب ترقية الحساب بنجاح",
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                } else {
                    $this->response([
                        'status' => TRUE,
                        'message' => "Account upgrade request failed. Please try again later \n فشل طلب ترقية الحساب. الرجاء معاودة المحاولة في وقت لاحق",
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function searchGroup_get()
    {
        $post_data = $_REQUEST; // SubCategoryID, CityID, Text
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $sub_category_id = $post_data['SubCategoryID'];
            $where = "groups.SubCategoryID = $sub_category_id ";

            if (isset($post_data['CityID']) && $post_data['CityID'] != '') {
                $cityId = $post_data['CityID'];
                $where .= " AND groups.GroupCityID = $cityId ";
            }

            if (isset($post_data['Text']) && $post_data['Text'] != '') {
                $text = $post_data['Text'];
                $where .= " AND groups_text.Title LIKE '%$text%' ";
            }

            $data = $this->Group_model->getGroupsDate($where, $start, $limit);
            $this->response([
                'status' => TRUE,
                'groups' => $data,
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function logout_get()
    {
        $post_data = $_REQUEST; // UserID
        if (!empty($post_data)) {
            $this->User_model->update(array('DeviceToken' => ''), array('UserID' => $post_data['UserID']));
            $this->response([
                'status' => TRUE,
                'message' => "User logged out successfully \n تسجيل خروج المستخدم بنجاح",
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found \n لم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    private function checkUserExist($user_id)
    {
        $get_user = $this->User_model->get($user_id, FALSE, 'UserID');
        if ($get_user) {
            return true;
        } else {
            return false;
        }
    }

    private function checkIfUserIsAdmin($group_id, $user_id)
    {
        $check_if['GroupID'] = $group_id;
        $check_if['UserID'] = $user_id;
        $check_if['IsAdmin'] = 'yes';
        $user_is_admin = $this->Group_member_model->getWithMultipleFields($check_if);
        if ($user_is_admin) {
            return true;
        } else {
            return false;
        }
    }

    private function uploadImage($key, $path)
    {
        $file_name = rand(999, 999999) . date('Ymdhsi') . $_FILES[$key]['name'];
        $file_size = $_FILES[$key]['size'];
        $file_tmp = $_FILES[$key]['tmp_name'];
        $file_type = $_FILES[$key]['type'];
        move_uploaded_file($file_tmp, $path . $file_name);
        return $file_name;
    }

    private function sendEmail($txt)
    {
        $to = "bilal.e@zynq.net";
        $subject = "MSJ Test";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <no-reply@zynq.net>' . "\r\n";
        mail($to, $subject, $txt, $headers);
    }

    private function sendWelcomeEmail($user_id, $password)
    {
        $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $user_id . '');
        $user = $user[0];
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hobbies</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}

#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>

<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi ' . $user['FullName'] . ' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Thank you for registering with Hobbies. We hope to provide you the best we can.</p>
<p style="font-family:sans-serif;font-size:14px;">Your login details are:</p>
<p style="font-family:sans-serif;font-size:14px;">Email: ' . $user['Email'] . '</p>
<p style="font-family:sans-serif;font-size:14px;">Password: ' . $password . '</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="' . base_url() . '" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Hobbies</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">hobbies.schopfen.com</a>
</span>
</td>
</tr>
</table>

</td>
</tr>
</table>

</body>
</html>
';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <no-reply@hobbies.com.sa>' . "\r\n";
        mail($user['Email'], 'Welcome at Hobbies', $message, $headers);

    }

    private function sendForgotPasswordEmail($user, $password)
    {
        $user_text = $this->User_text_model->getWithMultipleFields(array('UserID' => $user->UserID));
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <no-reply@hobbies.schopfen.com>' . "\r\n";

        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hobbies</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}

#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>

<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi ' . $user_text->FullName . ' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Forgot Your Password ?</p>
<p style="font-family:sans-serif;font-size:14px;">No worries , we have a new one for you. Please use following password to login.</p>
<p style="font-family:sans-serif;font-size:14px;font-weight: bold;">Password: ' . $password . '</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="' . base_url() . '" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Hobbies</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">hobbies.schopfen.com</a>
</span>
</td>
</tr>
</table>

</td>
</tr>
</table>

</body>
</html>
';

        mail($user->Email, 'Forgot Password', $message, $headers);
        return true;
    }

    private function sendThankyouEmail()
    {
        /*$data['content'] = '<h4>Hi Bilal Ejaz,</h4>
                                                <p>Thank you for Purchasing from MSJ Security Systems.</p>
                                                <p> Your order # 123123123 has been placed successfully.</p>';
        $message = $this->load->view('front/emails/general_email', $data, true);
        echo $message;exit();*/
        $email_data['body'] = "Dear Bilal Ejaz , Your account is suspended at MSJ. Please contact admin for details and further action.";
        $email_body = emailTemplate($email_data['body']);
        echo $email_body;
        exit();
        $order_id = $this->input->get('order_id');
        $order_details = $this->Model_order->getDetail($order_id);
        $data['order_details'] = $order_details;
        $message = $this->load->view('front/emails/order_confirmation', $data, true);
        echo $message;
        exit();
        $email_data['to'] = 'bilal.e@zynq.net';
        $email_data['subject'] = 'Order received at MSJ : Order # ' . $order_details[0]['order_track_id'];
        $email_data['from'] = 'noreply@msj.com.sa';
        $email_data['body'] = $message;
        sendEmail($email_data);
        return true;
    }

    public function test_notification_get()
    {
        // updating user to PRO
        $UserID = 78;
        $data['Type'] = 'user_type_updated';
        $data['LoggedInUserID'] = $UserID; // the person whose post is this, who will receive this notification
        $data['UserID'] = 0; // the person who is making this notification
        $data['GroupID'] = 0;
        $data['PostID'] = 0;
        $data['PostCommentID'] = 0;
        $data['NotificationTypeID'] = 8;
        $data['CreatedAt'] = time();
        $notification_text = "Your user type is upgraded to PRO now";
        $res = sendNotification('Hobbies', $notification_text, $data, $UserID);
    }

}

