<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

date_default_timezone_set('UTC');

class Api extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->methods['user_get']['limit'] = 50000; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 10000; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 5000; // 50 requests per hour per user/key
        $this->load->model('User_model');
        $this->load->model('User_text_model');
        
        $this->load->model('Api_model');
        $this->load->model('Category_model');
        $this->load->model('Model_general');
        $this->load->model('Group_model');
        $this->load->model('Group_text_model');
        $this->load->model('Group_member_model');
        $this->load->model('Report_group_model');
        
        $this->load->model('Post_model');
        $this->load->model('Post_text_model');
        $this->load->model('Post_image_model');
        $this->load->model('Post_video_model');
        $this->load->model('Post_comment_model');
        $this->load->model('Post_like_model');
        $this->load->model('Post_report_model');
       
    }

    public function test_get()
    {
        $post_data = $this->input->get('abc');
        dump($_REQUEST);
        $fields = $this->User_model->getFields();
        print_rm($fields);
        exit;
    }

    public function postTest_post()
    {
        $post_data = $this->input->get();
        dump($post_data);
    }

    public function signUp_post()
    {
        $post_data = $this->input->post(); // full_name, email, password, type (company, designer, visitor), designation (for designer type), industry (for company type), city, image, device_type, device_token
        if (!empty($post_data)) {
            if (isset($post_data['Email'])) {
                $user = $this->User_model->getWithMultipleFields(array('Email' => $post_data['Email']), true);
                if ($user) {
                    // sending back error
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Email address already exist'
                    ], REST_Controller::HTTP_OK);
                }
            }

            $parent_tbl_data = $this->User_model->getFields();
            $child_tbl_data = $this->User_text_model->getFields();
            $save_parent_data = array();
            $save_child_data = array();

            foreach ($post_data as $key => $value) {

                if (in_array($key, $parent_tbl_data)) {
                    $save_parent_data[$key] = $value;


                } else if (in_array($key, $child_tbl_data)) {
                    $save_child_data[$key] = $value;
                }

            }


            $password = '';
            if (isset($save_parent_data['Password'])) {

                $password = $save_parent_data['Password'];
                $save_parent_data['Password'] = md5($save_parent_data['Password']);
            }


            $save_parent_data['UpdatedAt'] = $save_parent_data['CreatedAt'] = date('Y-m-d H:i:s');
            $save_parent_data['RoleID'] = 2;
            $file_name = '';
            $compressed_file_name = '';
            if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {

                $file_name = $this->uploadImage("Image", "uploads/users/");
                $file_name = "uploads/users/" . $file_name;
            }
            if (isset($_FILES['CompressedImage']) && $_FILES['CompressedImage']['name'] != '') {

                $compressed_file_name = $this->uploadImage("CompressedImage", "uploads/users/compressed/");
                $compressed_file_name = "uploads/users/compressed/" . $compressed_file_name;
            }
            $save_parent_data['Image'] = $file_name;
            $save_parent_data['CompressedImage'] = $compressed_file_name;

            $insert_id = $this->User_model->save($save_parent_data);
            if ($insert_id > 0) {
                $save_child_data['UserID'] = $insert_id;

                $save_child_data['SystemLanguageID'] = 1;
                $this->User_text_model->save($save_child_data);
                $this->sendWelcomeEmail($insert_id, $password);
                $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $insert_id . '');
                $this->response([
                    'status' => TRUE,
                    'user_info' => NullToEmpty($user[0])
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'User signup failed. Please try again.'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    public function updateProfile_post()
    {
        $post_data = $this->input->post(); // UserID, FullName, Email, Password, Type (company, designer, visitor), Designation (for designer type), Industry (for company type), City, Image, CompressedImage, CoverImage, CompressedCoverImage, Gender, Bio, DeviceType, DeviceToken
        if (!empty($post_data)) {
            if (isset($post_data['Email'])) {
                $user = $this->User_model->getWithMultipleFields(array('Email' => $post_data['Email']), true);
                if ($user) {
                    // sending back error
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Email address already exist'
                    ], REST_Controller::HTTP_OK);
                }
            }

            $parent_tbl_data = $this->User_model->getFields();
            $child_tbl_data = $this->User_text_model->getFields();
            $save_parent_data = array();
            $save_child_data = array();

            foreach ($post_data as $key => $value) {

                if (in_array($key, $parent_tbl_data)) {
                    $save_parent_data[$key] = $value;


                } else if (in_array($key, $child_tbl_data)) {
                    $save_child_data[$key] = $value;
                }

            }

            $password = '';
            if (isset($save_parent_data['Password'])) {

                $password = $save_parent_data['Password'];
                $save_parent_data['Password'] = md5($save_parent_data['Password']);
            }

            $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');


            if (isset($_FILES['Image']) && $_FILES['Image']['name'] != '') {

                $file_name = $this->uploadImage("Image", "uploads/users/");
                $save_parent_data['Image'] = "uploads/users/" . $file_name;
            }
            if (isset($_FILES['CompressedImage']) && $_FILES['CompressedImage']['name'] != '') {

                $compressed_file_name = $this->uploadImage("CompressedImage", "uploads/users/compressed/");
                $save_parent_data['CompressedImage'] = "uploads/users/compressed/" . $compressed_file_name;
            }
         /*   if (isset($_FILES['CoverImage']) && $_FILES['CoverImage']['name'] != '') {

                $compressed_file_name = $this->uploadImage("CoverImage", "uploads/users/");
                $save_parent_data['CoverImage'] = "uploads/users/" . $compressed_file_name;
            }
            if (isset($_FILES['CompressedCoverImage']) && $_FILES['CompressedCoverImage']['name'] != '') {

                $compressed_file_name = $this->uploadImage("CompressedCoverImage", "uploads/users/compressed/");
                $save_parent_data['CompressedCoverImage'] = "uploads/users/compressed/" . $compressed_file_name;
            }
*/
            $update_by = array();
            $update_by['UserID'] = $post_data['UserID'];
            $this->User_model->update($save_parent_data, $update_by);
            if ($post_data['UserID'] > 0) {
                $update_by['SystemLanguageID'] = 1;
                $this->User_text_model->update($save_child_data, $update_by);
                $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $post_data['UserID'] . '');
                $this->response([
                    'status' => TRUE,
                    'user_info' => NullToEmpty($user[0])
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Update profile failed. Please try again.'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function login_post()
    {
        $data = $this->input->post(); // email, password, device_type, device_token
        if (!empty($data)) {
            $data_fetch = array();
            $data_fetch['Email'] = $data['Email'];
            $data_fetch['Password'] = md5($data['Password']);
            $user = $this->User_model->getWithMultipleFields($data_fetch, true);
            if ($user) {
                if (isset($data['DeviceType']) || isset($data['DeviceToken'])) {
                    $update_user = array();
                    $update_user_by = array();
                    $update_user_by['UserID'] = $user['UserID'];

                    if (isset($data['DeviceType'])) {
                        $update_user['DeviceType'] = $data['DeviceType'];
                        $user['DeviceType'] = $data['DeviceType'];
                    }

                    if (isset($data['DeviceToken'])) {
                        $update_user['DeviceToken'] = $data['DeviceToken'];
                        $user['DeviceToken'] = $data['DeviceToken'];
                    }

                    $this->User_model->update($update_user, $update_user_by);
                }

                if ($user['IsActive'] == 'no') {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Your account is suspended. Please contact admin for further details & action'
                    ], REST_Controller::HTTP_OK);
                } else {
                    $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $user['UserID'] . '');
                    $this->response([
                        'status' => TRUE,
                        'user_info' => NullToEmpty($user[0])
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Email or password incorrect'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

    }

    public function getUserDetails_get()
    {
        //$post_data = $this->input->get(); // user_id
        $post_data = $_REQUEST; // user_id
        if (!empty($post_data)) {
            $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $post_data['UserID'] . '');
            if ($user) {
                $this->response([
                    'status' => TRUE,
                    'user' => NullToEmpty($user[0])
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'User not found for this ID.'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function getProfile_get()
    {
        //$post_data = $this->input->get(); // user_id
        $post_data = $this->input->get(); // user_id
        if (!empty($post_data)) {
            $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $post_data['UserID'] . '');
            if ($user) {

               


                $this->response([
                    'status' => TRUE,
                    'user' => NullToEmpty($user[0])
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'User not found for this id'
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    public function categories_get()
    {
        
        $post_data = $this->input->get();
         if(isset($post_data['CategoryID'])){
             $where = 'categories.ParentID ='.$post_data['CategoryID'];
         }else{
              $where = 'categories.ParentID = 0';
         }
        
        $categories = $this->Category_model->getJoinedData(true,'CategoryID',$where);
        
        $this->response([
            'status' => TRUE,
            'categories' => $categories
        ], REST_Controller::HTTP_OK);
        

    }
    
    
    
    
    public function createGroup_post(){
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {
            $parent_tbl_data = $this->Group_model->getFields();
            //print_rm($parent_tbl_data);
            $child_tbl_data = $this->Group_text_model->getFields();
            $save_parent_data = array();
            $save_child_data = array();

            foreach ($post_data as $key => $value) {

                if (in_array($key, $parent_tbl_data)) {
                    $save_parent_data[$key] = $value;


                }else if (in_array($key, $child_tbl_data)) {
                    $save_child_data[$key] = $value;
                }

            }
             
           
            
            $save_parent_data['IsActive'] = 1;
            $file_name = '';
            $compressed_file_name = '';
            if (isset($_FILES['GroupImage']) && $_FILES['GroupImage']['name'] != '') {
                $file_name  = 'uploads/groups/'.$this->uploadImage('GroupImage','uploads/groups/');
            }
            
            if(isset($_FILES['GroupCompressImage']) && $_FILES['GroupCompressImage']['name'] != ''){
                $compressed_file_name  = 'uploads/groups/'.$this->uploadImage('GroupCompressImage','uploads/groups/');
            
            }
            $save_parent_data['GroupImage']  = $file_name; 
            $save_parent_data['GroupCompressImage']  =   $compressed_file_name;     
                    
            $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] =  $post_data['UserID'];
           
            $insert_id = $this->Group_model->save($save_parent_data);
            
            
            
            
            
            if ($insert_id > 0) {
                
                $save_child_data['GroupID'] = $insert_id;
                $save_child_data['SystemLanguageID'] = 1;
                
                $this->Group_text_model->save($save_child_data);
                
                $group_member_data = array();
                $group_member_data['UserID'] =  $post_data['UserID'];
                $group_member_data['GroupID'] =  $insert_id;
                $group_member_data['IsAdmin'] =  'yes';
                $group_member_data['CreatedAt'] = date('Y-m-d H:i:s');
                $this->Group_member_model->save($group_member_data); 
                
                
                $group = $this->Group_model->getGroupsDate('groups.GroupID = '.$insert_id.'');
                $this->response([
                    'status' => TRUE,
                    'group_info' => NullToEmpty($group[0])
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Something went wrong.'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
    public function updateGroup_post(){
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {
            $parent_tbl_data = $this->Group_model->getFields();
            $child_tbl_data = $this->Group_text_model->getFields();
            $save_parent_data = array();
            $save_child_data = array();

            foreach ($post_data as $key => $value) {

                if (in_array($key, $parent_tbl_data)  && $key != 'GroupID') {
                    $save_parent_data[$key] = $value;


                } else if (in_array($key, $child_tbl_data) && $key != 'GroupID') {
                    $save_child_data[$key] = $value;
                }

            }
            
           
            
            if (isset($_FILES['GroupImage']) && $_FILES['GroupImage']['name'] != '') {
                $save_parent_data['GroupImage'] = 'uploads/groups/'.$this->uploadImage('GroupImage','uploads/groups/');
            }
            if (isset($_FILES['GroupCompressImage']) && $_FILES['GroupCompressImage']['name'] != '') {
                $save_parent_data['GroupCompressImage'] = 'uploads/groups/'.$this->uploadImage('GroupCompressImage','uploads/groups/');
            }
           $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
           $save_parent_data['UpdatedBy'] = $post_data['UserID'];
            
           $this->Group_model->update($save_parent_data,array('GroupID' => $post_data['GroupID']));
            
            
            
            
            
           
           $update_by = array();
           $update_by['GroupID'] = $post_data['GroupID'];
           $update_by['SystemLanguageID'] = 1;
            $this->Group_text_model->update($save_child_data,$update_by);


            $group = $this->Group_model->getGroupsDate('groups.GroupID = '.$post_data['GroupID'].'');
            $this->response([
                'status' => TRUE,
                'group_info' => NullToEmpty($group[0])
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
     public function createPost_post()
    {
        $post_data = $this->input->post(); // type, text, link, user_id, image_count, video_count, image1, image2, compressed_image1, compressed_image2, video1, video2, video_thumb1, video_thumb2 etc.., CreatedAt, UpdatedAt
        if (!empty($post_data)) {
            $parent_tbl_data = $this->Post_model->getFields();
            $child_tbl_data = $this->Post_text_model->getFields();
            $save_parent_data = array();
            $save_child_data = array();

            foreach ($post_data as $key => $value) {

                if (in_array($key, $parent_tbl_data)) {
                    $save_parent_data[$key] = $value;


                } else if (in_array($key, $child_tbl_data)) {
                    $save_child_data[$key] = $value;
                }

            }

            $inserted_id = $this->Post_model->save($save_parent_data);
            if ($inserted_id > 0) {
                if (isset($save_child_data['PostText']) && $save_child_data['PostText'] != '') {
                    $save_child_data['PostID'] = $inserted_id;
                    $save_child_data['SystemLanguageID'] = 1;
                    $this->Post_text_model->save($save_child_data);
                }
                // uploading images and videos for post
                $images_count = (isset($post_data['ImageCount']) && $post_data['ImageCount'] > 0 ? $post_data['ImageCount'] : 0);
                $videos_count = (isset($post_data['VideoCount']) && $post_data['VideoCount'] > 0 ? $post_data['VideoCount'] : 0);
                $image_extensions = array("jpeg", "jpg", "png", "gif", "JPEG", "JPG", "PNG", "GIF");
                $video_extensions = array("MP4", "mp4", "3gp", "3GP", "mov", "MOV", "MPEG4", "mpeg4");


                ///////////// here
                for ($i = 1; $i <= $images_count; $i++) {
                    $file_name = $_FILES["Image" . $i]["name"];
                    $file_tmp = $_FILES["Image" . $i]["tmp_name"];
                    $compressed_file_name = $_FILES["CompressedImage" . $i]["name"];
                    $compressed_file_tmp = $_FILES["CompressedImage" . $i]["tmp_name"];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $compressed_ext = pathinfo($compressed_file_name, PATHINFO_EXTENSION);

                    $filename = basename($file_name, $ext);
                    $newFileName = $filename . $i . time() . "." . $ext;
                    move_uploaded_file($file_tmp, "uploads/posts/images/" . $newFileName);

                    // for compressed image
                    $compressed_filename = basename($compressed_file_name, $compressed_ext);
                    $compressed_newFileName = $compressed_filename . $i . time() . "." . $compressed_ext;
                    move_uploaded_file($compressed_file_tmp, "uploads/posts/images/compressed/" . $compressed_newFileName);

                    $post_image_data['PostID'] = $inserted_id;
                    $post_image_data['Image'] = "uploads/posts/images/" . $newFileName;
                    $post_image_data['CompressedImage'] = "uploads/posts/images/compressed/" . $compressed_newFileName;
                    $this->Post_image_model->save($post_image_data);
                }

                for ($i = 1; $i <= $videos_count; $i++) {
                    // for video
                    $video_file_name = $_FILES["Video" . $i]["name"];
                    $video_file_tmp = $_FILES["Video" . $i]["tmp_name"];
                    $video_thumb_file_name = $_FILES["VideoThumb" . $i]["name"];
                    $video_thumb_file_tmp = $_FILES["VideoThumb" . $i]["tmp_name"];
                    $video_ext = pathinfo($video_file_name, PATHINFO_EXTENSION);
                    $video_thumb_ext = pathinfo($video_thumb_file_name, PATHINFO_EXTENSION);

                    // for video
                    $video_filename = basename($video_file_name, $video_ext);
                    $video_newFileName = $video_filename . $i . time() . "." . $video_ext;
                    move_uploaded_file($video_file_tmp, "uploads/posts/videos/" . $video_newFileName);

                    // for video thumb
                    $video_thumb_filename = basename($video_thumb_file_name, $video_thumb_ext);
                    $video_thumb_newFileName = $video_thumb_filename . $i . time() . "." . $video_thumb_ext;
                    move_uploaded_file($video_thumb_file_tmp, "uploads/posts/videos/thumbs/" . $video_thumb_newFileName);

                    // saving in db
                    $post_video_data['PostID'] = $inserted_id;
                    $post_video_data['Video'] = "uploads/posts/videos/" . $video_newFileName;
                    $post_video_data['VideoThumb'] = "uploads/posts/videos/thumbs/" . $video_thumb_newFileName;
                    $this->Post_video_model->save($post_video_data);
                }

                //$post = $this->Post_model->getJoinedDataWithOtherTable(true, 'PostID', 'posts_text', 'posts.PostID = ' . $inserted_id . '');
                $post = $this->Post_model->getLeftJoinedDataWithOtherTable(true, 'PostID', 'posts_text', 'posts.PostID = ' . $inserted_id . '');
                $post = NullToEmpty($post[0]);
                $post_images = $this->Post_image_model->getMultipleRows(array('PostID' => $inserted_id), true);
                $post_videos = $this->Post_video_model->getMultipleRows(array('PostID' => $inserted_id), true);
                $post['post_images'] = ($post_images ? NullToEmpty($post_images) : array());
                $post['post_videos'] = ($post_videos ? NullToEmpty($post_videos) : array());
                $this->response([
                    'status' => TRUE,
                    'post' => $post
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Experience not added. Please try again.'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
    public function getPostDetail_get()
    {
        $post_data = $_REQUEST; // PostID, Start, Limit
        if (!empty($post_data)) {
            $start = 0;
            $limit = 10;

            if (isset($post_data['Start'])) {
                $start = $post_data['Start'];
            }
            if (isset($post_data['Limit'])) {
                $limit = $post_data['Limit'];
            }

            $post = $this->Post_model->getLeftJoinedDataWithOtherTable(true, 'PostID', 'posts_text', 'posts.PostID = ' . $post_data['PostID'] . '');
            $post = NullToEmpty($post[0]);
            $post_comments = $this->Post_comment_model->postComments($post['PostID'], $limit, $start);
            $post_likes = $this->Post_like_model->postLikes($post['PostID']);
            $post_images = $this->Post_image_model->getMultipleRows(array('PostID' => $post['PostID']), true);
            $post_videos = $this->Post_video_model->getMultipleRows(array('PostID' => $post['PostID']), true);
            $post['post_comments'] = ($post_comments ? NullToEmpty($post_comments) : array());
            $post['post_likes'] = ($post_likes ? NullToEmpty($post_likes) : array());
            $post['post_images'] = ($post_images ? NullToEmpty($post_images) : array());
            $post['post_videos'] = ($post_videos ? NullToEmpty($post_videos) : array());
            $this->response([
                'status' => TRUE,
                'post' => $post
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'No data found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    
    
    public function postComment_post()
    {
        $post_data = $this->input->post(); // PostID, UserID, Comment, CreatedAt, UpdatedAt
        if (!empty($post_data)) {
            $inserted_id = $this->Post_comment_model->save($post_data);
            $comment = $this->Post_comment_model->postComments($post_data['PostID'], false, false, $inserted_id);
            $this->response([
                'status' => TRUE,
                'comment' => $comment[0]
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => FALSE,
                'message' => 'No data found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }
    
    
    public function getGroupData_get(){
        $post_data = $this->input->get(); // user_id
        if (!empty($post_data)) {
            
             
        $group = $this->Group_model->getGroupsDate('groups.GroupID = '.$post_data['GroupID'].'');
        if(!empty($group)){
            $check_is_group_member = array();
            $check_is_group_member['GroupID'] = $post_data['GroupID'];
            $check_is_group_member['UserID']  = $post_data['LoggedInUserID'];


            $member_data = $this->Group_member_model->getWithMultipleFields($check_is_group_member);
            if($member_data){
                $group[0]['IsMember'] = 'yes';
                $group[0]['IsAdmin']  = $member_data->IsAdmin;
            }else{
                $group[0]['IsMember'] = 'no';
                $group[0]['IsAdmin']  = 'no';
            }


            $group_post = $this->Post_model->getJoinedData(true,'PostID','posts.GroupID = '.$post_data['GroupID']);
            if($group_post){
                $return_post = array();
                foreach($group_post as $post){
                    $images = $this->Post_image_model->getMultipleRows(array('PostID' => $post['PostID']),true);
                   
                    if($images){
                        $post['Images'] = $images;
                    }else{
                        $post['Images'] = array();;
                    }
                    
                    $videos = $this->Post_video_model->getMultipleRows(array('PostID' => $post['PostID']),true);
                    if($videos){
                        $post['Videos'] = $videos;
                    }else{
                        $post['Videos'] = array();;
                    }
                    
                    $return_post[] = $post;
                    
                }
                
                
                $group[0]['Posts'] = $return_post;
            }else{
                $group[0]['Posts'] = array();
            }
            
        }
       
        
        
        
        
       
        if(!empty($group)){
              $this->response([
            'status' => TRUE,
            'group_info' => NullToEmpty($group[0])
        ], REST_Controller::HTTP_OK);
        }else{
              $this->response([
            'status' => FALSE,
            'message' => 'Something went worng'
        ], REST_Controller::HTTP_OK);
        }
      
        
        
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    public function likePost_post()
    {
        $post_data = $this->input->post(); // PostID, UserID
        if (!empty($post_data)) {
            $this->Post_like_model->save($post_data);
            $this->response([
                'status' => TRUE,
                'message' => 'Post liked successfully'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => FALSE,
                'message' => 'No data found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }

    public function reportPost_post()
    {
        $post_data = $this->input->post(); // PostID, UserID, ReportReason, ReportedOn
        if (!empty($post_data)) {
            $this->Post_report_model->save($post_data);
            $this->response([
                'status' => TRUE,
                'message' => 'Post reported successfully'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {

            $this->response([
                'status' => FALSE,
                'message' => 'No data found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }
    }
    
    public function deleteGroup_post(){
        $post_data = $this->input->post(); 
        if (!empty($post_data)) {
            
       $this->Group_text_model->delete(array('GroupID' => $post_data['GroupID']));     
       $this->Group_model->delete(array('GroupID' => $post_data['GroupID']));
        $this->response([
            'status' => TRUE,
            'message' => 'Group Deleted Successfully'
        ], REST_Controller::HTTP_OK);
        
        
        
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
    
     public function getGroups_get(){
        
        $where = false;
        $first  = false;
        $post_data = $this->input->get();
         if(isset($post_data['CategoryID'])){
             $first = true;
            $where  = 'groups.CategoryID = '.$post_data['CategoryID'];
        }
        
        
        if(isset($post_data['SubCategoryID'])){
            if($first){
               $where .= ' AND groups.SubCategoryID = '.$post_data['SubCategoryID'];
            }else{
                $where  = 'groups.SubCategoryID = '.$post_data['SubCategoryID'];
            }
            
        }
        
        
        $groups = $this->Group_model->getGroupsDate($where);
       
        if($groups){
            
             $this->response([
            'status' => TRUE,
            'groups' => $groups
        ], REST_Controller::HTTP_OK);
        }else{
              $this->response([
            'status' => FALSE,
            'message' => 'Groups not found'
        ], REST_Controller::HTTP_OK);
        }
      
        
        
        
    }
    
    
    public function joinGroup_post(){
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {
            
        $check_user = $this->Group_member_model->getWithMultipleFields($post_data,true);    
        
       
        if(empty($check_user)){
            $post_data['CreatedAt'] = date('Y-m-d H:i:s');
            $insert_id = $this->Group_member_model->save($post_data); 
            
            $this->response([
            'status' => TRUE,
            'message' => 'Group joined successfully'
        ], REST_Controller::HTTP_OK);
        }else{
              $this->response([
            'status' => FALSE,
            'message' => 'Already a member of this group'
        ], REST_Controller::HTTP_OK);
        }
      
        
        
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    public function leaveGroup_post(){
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {
           
            $user_group_info = $this->Group_member_model->getWithMultipleFields($post_data);
            
            if($user_group_info){
                
                if($user_group_info->IsAdmin == 'yes'){
                    $this->Group_member_model->delete($post_data); 
                    $fetch_by = array();
                    $fetch_by['IsAdmin'] = 'yes';
                    $fetch_by['GroupID'] = $post_data['GroupID'];
         
                    $users = $this->Group_member_model->getMultipleRows($fetch_by);
                   
                    if(!$users){
                       $old_member = $this->Group_member_model->getOldestGroupMember($post_data['GroupID']);
                      // print_rm($old_member);
                       if(!empty($old_member)){
                           $update = array();
                           $update_by = array();


                           $update['IsAdmin'] = 'yes';

                           $update_by['UserID'] = $old_member['UserID'];
                           $update_by['GroupID'] = $old_member['GroupID'];

                           $this->Group_member_model->update($update,$update_by);

                       }
                    }
                    
                }
            }
            
        // 
         
        
         
        
       
       $this->response([
            'status' => TRUE,
            'message' => 'Leaved group successfully'
        ], REST_Controller::HTTP_OK);
        
        
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }
    
    
    public function reportGroup_post(){
        $post_data = $this->input->post(); // user_id
        if (!empty($post_data)) {
            
        $post_data['CreatedAt'] = date('Y-m-d H:i:s');
        $insert_id = $this->Report_group_model->save($post_data);    
        
       
        if($insert_id > 0){
            
            
            $this->response([
            'status' => TRUE,
            'message' => 'Group reported successfully'
        ], REST_Controller::HTTP_OK);
        }else{
              $this->response([
            'status' => FALSE,
            'message' => 'Something went wrong'
        ], REST_Controller::HTTP_OK);
        }
      
        
        
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

   

    public function forgotPassword_get()
    {
        $post_data = $_REQUEST;
        if (!empty($post_data)) {
            $fetch_by['Email'] = $post_data['Email'];
            $user = $this->User_model->getWithMultipleFields($fetch_by);
            if ($user) {
                $new_pass = RandomString();
                $update['Password'] = md5($new_pass);
                $update_by['UserID'] = $user->UserID;
                $this->User_model->update($update, $update_by);
                $mail_sent = $this->sendForgotPasswordEmail($user, $update['Password']);
                if ($mail_sent) {
                    $this->response([
                        'status' => TRUE,
                        'message' => 'your new password is sent at your registered email address'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No user found with this email'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {

            $this->response([
                'status' => FALSE,
                'message' => 'No data found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }

    }

   

    private function uploadImage($key, $path)
    {
        $file_name = rand(999, 999999) . date('Ymdhsi') . $_FILES[$key]['name'];
        $file_size = $_FILES[$key]['size'];
        $file_tmp = $_FILES[$key]['tmp_name'];
        $file_type = $_FILES[$key]['type'];
        move_uploaded_file($file_tmp, $path . $file_name);
        return $file_name;
    }

    private function sendEmail($txt)
    {
        $to = "bilal.e@zynq.net";
        $subject = "MSJ Test";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <no-reply@zynq.net>' . "\r\n";
        mail($to, $subject, $txt, $headers);
    }

    private function sendWelcomeEmail($user_id, $password)
    {
        $user = $this->User_model->getJoinedDataWithOtherTable(true, 'UserID', 'users_text', 'users.UserID = ' . $user_id . '');
        $user = $user[0];
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ArtBoards</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}

#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>

<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi ' . $user['FullName'] . ' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Thank you for registering with ArtBoards. We hope to provide you the best we can.</p>
<p style="font-family:sans-serif;font-size:14px;">Your login details are:</p>
<p style="font-family:sans-serif;font-size:14px;">Email: ' . $user['Email'] . '</p>
<p style="font-family:sans-serif;font-size:14px;">Password: ' . $password . '</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="' . base_url() . '" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">ArtBoards</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">artboards.schopfen.com</a>
</span>
</td>
</tr>
</table>

</td>
</tr>
</table>

</body>
</html>
';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <no-reply@artboards.com.sa>' . "\r\n";
        mail($user['Email'], 'Welcome at ArtBoards', $message, $headers);

    }

    private function sendForgotPasswordEmail($user, $password)
    {
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <no-reply@artboards.schopfen.com>' . "\r\n";

        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ArtBoards</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}

#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>

<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi ' . $user->FullName . ' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Forgot Your Password ?</p>
<p style="font-family:sans-serif;font-size:14px;">No worries , we have a new one for you. Please use following password to login.</p>
<p style="font-family:sans-serif;font-size:14px;font-weight: bold;">Password: ' . $password . '</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="' . base_url() . '" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">ArtBoards</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">artboards.schopfen.com</a>
</span>
</td>
</tr>
</table>

</td>
</tr>
</table>

</body>
</html>
';

        mail($user->Email, 'Forgot Password', $message, $headers);
        return true;
    }

    private function sendThankyouEmail()
    {
        /*$data['content'] = '<h4>Hi Bilal Ejaz,</h4>
                                                <p>Thank you for Purchasing from MSJ Security Systems.</p>
                                                <p> Your order # 123123123 has been placed successfully.</p>';
        $message = $this->load->view('front/emails/general_email', $data, true);
        echo $message;exit();*/
        $email_data['body'] = "Dear Bilal Ejaz , Your account is suspended at MSJ. Please contact admin for details and further action.";
        $email_body = emailTemplate($email_data['body']);
        echo $email_body;
        exit();
        $order_id = $this->input->get('order_id');
        $order_details = $this->Model_order->getDetail($order_id);
        $data['order_details'] = $order_details;
        $message = $this->load->view('front/emails/order_confirmation', $data, true);
        echo $message;
        exit();
        $email_data['to'] = 'bilal.e@zynq.net';
        $email_data['subject'] = 'Order received at MSJ : Order # ' . $order_details[0]['order_track_id'];
        $email_data['from'] = 'noreply@msj.com.sa';
        $email_data['body'] = $message;
        sendEmail($email_data);
        return true;
    }


}

