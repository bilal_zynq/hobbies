<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upgrade extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
                    'Upgrade_account_model',
                    'Model_general',
                    'User_model'
        ]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = 'Upgrade_account_model';
                
                $this->data['TableKey'] = 'AccountUpgradeRequestID';
                $this->data['Table'] = 'account_upgrade_requests';
       
        
    }
     
    
    public function index()
    {
          $parent                             = $this->data['Parent_model'];
          
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        
          /*$this->data['pending'] = $this->$parent->getAllUpgradedRequest('account_upgrade_requests.Status = "pending"');
          $this->data['approved'] = $this->$parent->getAllUpgradedRequest('account_upgrade_requests.Status = "approved"');*/

        $this->data['pending_requests'] = $this->$parent->getAllUpgradedRequestNew('account_upgrade_requests_new.Status = "Pending"');
        $this->data['approved_requests'] = $this->$parent->getAllUpgradedRequestNew('account_upgrade_requests_new.Status = "Approved"');
        $this->data['rejected_requests'] = $this->$parent->getAllUpgradedRequestNew('account_upgrade_requests_new.Status = "Rejected"');

          $this->load->view('backend/layouts/default',$this->data);
    }
   
   
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
           case 'approved':
                $this->approved();
            break;
            case 'approved_new':
                $this->approved_new();
                break;
            case 'rejected':
                $this->rejected();
                break;
                 
        }
    }
    
    
    private function approved(){


        $id = $this->input->post('id');

        $this->Upgrade_account_model->update(array('Status' => 'approved'),array('AccountUpgradeRequestID' => $id));

        $success['error']   = false;
        $success['success'] = 'Approved Successfully';
        echo json_encode($success);
        exit;

        

    }

    private function approved_new(){


        $id = $this->input->post('id');

        $this->Model_general->updateWhere('account_upgrade_requests_new', array('Status' => 'Approved'),array('AccountUpgradeRequestID' => $id));

        $result = $this->Model_general->getSingleRow('account_upgrade_requests_new', array('AccountUpgradeRequestID' => $id));
        $this->User_model->update(array('AccountTypeID' => 3), array('UserID' => $result->UserID)); // giving this user Platinum

        // saving notification
        $data['Type'] = 'user_type_updated';
        $data['LoggedInUserID'] = $result->UserID; // the person whose post is this, who will receive this notification
        $data['UserID'] = 0; // the person who is making this notification
        $data['GroupID'] = 0;
        $data['PostID'] = 0;
        $data['PostCommentID'] = 0;
        $data['NotificationTypeID'] = 9;
        $data['CreatedAt'] = time();
        log_notification($data);
        $notification_text = "Your account type is upgraded to Platinum now";
        $res = sendNotification('Hobbies', $notification_text, $data, $result->UserID);

        $success['error']   = false;
        $success['success'] = 'Request approved';
        echo json_encode($success);
        exit;



    }

    private function rejected()
    {
        $id = $this->input->post('id');

        $this->Model_general->updateWhere('account_upgrade_requests_new', array('Status' => 'Rejected'),array('AccountUpgradeRequestID' => $id));

        $success['error']   = false;
        $success['success'] = 'Request rejected';
        echo json_encode($success);
        exit;
    }
    
    
    

}