<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GroupReport extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Group_report_model');
        $this->load->model('City_text_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
    }

    public function index()
    {
        $this->data['cities'] = $this->City_text_model->getAll();
        $this->data['view'] = 'backend/groupReport/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function filterGroups()
    {
        $post_data = $this->input->post();
        if ($post_data['filter_type'] == 1) {
            $html = $this->filterByCity($post_data['city_id']);
        } else if ($post_data['filter_type'] == 2) {
            $html = $this->filterByNoOfMembers($post_data['no_of_members']);
        } else if ($post_data['filter_type'] == 3) {
            $html = $this->sortRecentlyActiveToOlder();
        } else if ($post_data['filter_type'] == 4) {
            $html = $this->sortOlderToRecentlyActive();
        } else if ($post_data['filter_type'] == 5) {
            $html = $this->filterByCreatedInSpecificPeriod($post_data['from_date'], $post_data['to_date']);
        } else if ($post_data['filter_type'] == 6) {
            $html = $this->createdOnSPecificDate($post_data['date']);
        } else if ($post_data['filter_type'] == 7) {
            $html = $this->suspendedGroups();
        } else if ($post_data['filter_type'] == 8) {
            $html = $this->reportedGroups();
        } else if ($post_data['filter_type'] == 9) {
            $html = $this->noOfAdmins();
        } else if ($post_data['filter_type'] == 10) {
            $html = $this->topActiveGroups();
        }
        echo $html;
        exit();
    }

    private function filterByCity($city_id)
    {
        $groups = $this->Group_report_model->filterByCity($city_id);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Group Owner</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['Title'] . '</td>
                                    <td>' . $group['CategoryTitle'] . '</td>
                                    <td>' . $group['SubCategoryTitle'] . '</td>
                                    <td>' . $group['GroupOnwerFullName'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No groups found in this city</span>';
        }

        return $group_html;
    }

    private function filterByNoOfMembers($max_no_of_members)
    {
        $groups = $this->Group_report_model->filterByNoOfMembers($max_no_of_members);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Group Owner</th>
                                    <th>No. of members</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['Title'] . '</td>
                                    <td>' . $group['CategoryTitle'] . '</td>
                                    <td>' . $group['SubCategoryTitle'] . '</td>
                                    <td>' . $group['GroupOnwerFullName'] . '</td>
                                    <td style="color: red;">' . $group['group_members_count'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No groups found with this criteria</span>';
        }

        return $group_html;
    }

    private function sortRecentlyActiveToOlder()
    {
        $groups = $this->Group_report_model->sortByLastActivity('DESC');
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Group Owner</th>
                                    <th>Last Post At</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['Title'] . '</td>
                                    <td>' . $group['CategoryTitle'] . '</td>
                                    <td>' . $group['SubCategoryTitle'] . '</td>
                                    <td>' . $group['GroupOnwerFullName'] . '</td>
                                    <td style="color: red;">' . ($group['LastActivityAt'] != '' ? date('d-m-Y h:i A', $group['LastActivityAt']).' UTC' : 'N/A').'</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No groups found with this criteria</span>';
        }

        return $group_html;
    }

    private function sortOlderToRecentlyActive()
    {
        $groups = $this->Group_report_model->sortByLastActivity('ASC');
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Group Owner</th>
                                    <th>Last Post At</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['Title'] . '</td>
                                    <td>' . $group['CategoryTitle'] . '</td>
                                    <td>' . $group['SubCategoryTitle'] . '</td>
                                    <td>' . $group['GroupOnwerFullName'] . '</td>
                                    <td style="color: red;">' . ($group['LastActivityAt'] != '' ? date('d-m-Y h:i A', $group['LastActivityAt']).' UTC' : 'N/A').'</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No groups found with this criteria</span>';
        }
        return $group_html;
    }

    private function filterByCreatedInSpecificPeriod($from_date, $to_date)
    {
        $groups = $this->Group_report_model->filterByCreatedInSpecificPeriod($from_date, $to_date);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Group Owner</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['Title'] . '</td>
                                    <td>' . $group['CategoryTitle'] . '</td>
                                    <td>' . $group['SubCategoryTitle'] . '</td>
                                    <td>' . $group['GroupOnwerFullName'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No groups found with this criteria</span>';
        }
        return $group_html;
    }

    private function createdOnSPecificDate($date)
    {
        $groups = $this->Group_report_model->createdOnSPecificDate($date);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Group Owner</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['Title'] . '</td>
                                    <td>' . $group['CategoryTitle'] . '</td>
                                    <td>' . $group['SubCategoryTitle'] . '</td>
                                    <td>' . $group['GroupOnwerFullName'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No groups found with this criteria</span>';
        }
        return $group_html;
    }

    private function suspendedGroups()
    {
        $groups = $this->Group_report_model->suspendedGroups();
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Group Owner</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['Title'] . '</td>
                                    <td>' . $group['CategoryTitle'] . '</td>
                                    <td>' . $group['SubCategoryTitle'] . '</td>
                                    <td>' . $group['GroupOnwerFullName'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No groups found with this criteria</span>';
        }
        return $group_html;
    }

    private function reportedGroups()
    {
        $groups = $this->Group_report_model->reportedGroups();
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Category</th>
                                    <th>Group Owner</th>
                                    <th>groupReport Count</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['Title'] . '</td>
                                    <td>' . $group['CategoryName'] . '</td>
                                    <td>' . $group['GroupOnwerFullName'] . '</td>
                                    <td style="text-align: center;color: red;">' . $group['group_reported_count'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No groups found with this criteria</span>';
        }
        return $group_html;
    }

    private function noOfAdmins()
    {
        $groups = $this->Group_report_model->noOfAdmins();
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Category</th>
                                    <th>Group Owner</th>
                                    <th>Admins Count</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['Title'] . '</td>
                                    <td>' . $group['CategoryTitle'] . '</td>
                                    <td>' . $group['GroupOnwerFullName'] . '</td>
                                    <td style="text-align: center;color: red;">' . $group['group_admins_count'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No groups found with this criteria</span>';
        }
        return $group_html;
    }

    private function topActiveGroups()
    {
        $groups = $this->Group_report_model->topActiveGroups();
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Group Name</th>
                                    <th>Category</th>
                                    <th>Group Owner</th>
                                    <th>Posts Count</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['Title'] . '</td>
                                    <td>' . $group['CategoryTitle'] . '</td>
                                    <td>' . $group['GroupOnwerFullName'] . '</td>
                                    <td style="text-align: center;color: red;">' . $group['group_posts_count'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No groups found with this criteria</span>';
        }
        return $group_html;
    }

}