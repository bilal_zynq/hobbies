<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Badge_request extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst('badge_request_attachments') . '_model',
            ucfirst('user') . '_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['TableKey'] = 'BadgeRequestID';
        $this->data['Table'] = 'badge_requests';


    }


    public function index()
    {
        $parent = $this->data['Parent_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->data['results'] = $this->$parent->getAllRequests();
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function view($id = '')
    {
        if (!checkUserRightAccess(51, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getAllRequests($id);

        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['attachments'] = $this->Badge_request_attachments_model->getMultipleRows(array('BadgeRequestID' => $id));

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/view';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {

        if (!checkUserRightAccess(51, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $save_parent_data = array();
        $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }


        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0);


        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {
            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;

        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        if (!checkUserRightAccess(51, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $this->$parent->update(array('Status' => $post_data['Status']), array('BadgeRequestID' => $post_data['BadgeRequestID']));
            if ($post_data['Status'] == 'Approved')
            {
                // updating user to PRO
                $result = $this->$parent->getAllRequests($post_data['BadgeRequestID']);
                $UserID = $result[0]->UserID;
                $this->User_model->update(array('AccountTypeID' => 2), array('UserID' => $UserID)); // giving this user PRO
                // saving notification
                $data['Type'] = 'user_type_updated';
                $data['LoggedInUserID'] = $UserID; // the person whose post is this, who will receive this notification
                $data['UserID'] = 0; // the person who is making this notification
                $data['GroupID'] = 0;
                $data['PostID'] = 0;
                $data['PostCommentID'] = 0;
                $data['NotificationTypeID'] = 8;
                $data['CreatedAt'] = time();
                log_notification($data);
                $notification_text = "Your user type is upgraded to PRO now";
                $res = sendNotification('Hobbies', $notification_text, $data, $UserID);
            }
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }


    private function delete()
    {

        if (!checkUserRightAccess(51, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];

        $deleted_by = array();
        $deleted_by['BadgeRequestID'] = $this->input->post('id');
        $this->$parent->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


}