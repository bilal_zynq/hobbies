<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();


        $this->load->Model([
            'User_model',
            'Group_model',
            'Post_model'
        ]);


        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();


    }


    public function index()
    {

        $this->data['total_users'] = 0;
        $this->data['total_posts'] = 0;
        $this->data['total_groups'] = 0;

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        // $this->data['users'] = $this->User_model->getAllJoinedData(false, 'UserID', $this->language);
        $this->data['users'] = $this->User_model->getJoinedData(false, 'UserID', 'users.RoleID = 2 AND users_text.SystemLanguageID = 1');
        if ($this->data['users']) {
            $this->data['total_users'] = count($this->data['users']);
        }

        $this->data['posts'] = $this->Post_model->getAllJoinedData(false, 'PostID', $this->language);
        if ($this->data['posts']) {
            $this->data['total_posts'] = count($this->data['posts']);
        }


        $this->data['groups'] = $this->Group_model->getAllJoinedData(false, 'GroupID', $this->language);
        if ($this->data['groups']) {
            $this->data['total_groups'] = count($this->data['groups']);
        }


        $group_posts = $this->Group_model->getGroupPosts();
        $group_members = $this->Group_model->getGroupMembers();

        $this->data['group_posts'] = $group_posts;
        $this->data['group_members'] = $group_members;

        $this->load->view('backend/layouts/default', $this->data);
    }


}