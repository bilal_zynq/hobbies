<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserReport extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('User_report_model');
        $this->load->model('City_text_model');
        $this->load->model('Badge_text_model');
        $this->load->model('Type_text_model');
        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
    }


    public function index()
    {
        $this->data['cities'] = $this->City_text_model->getAll();
        $this->data['badges'] = $this->Badge_text_model->getAll();
        $this->data['account_types'] = $this->Type_text_model->getAll();
        $this->data['view'] = 'backend/userReport/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function filterUsers()
    {
        $post_data = $this->input->post();
        if ($post_data['filter_type'] == 1) {
            $html = $this->filterByCity($post_data['city_id']);
        } else if ($post_data['filter_type'] == 2) {
            $html = $this->filterByName($post_data['name']);
        } else if ($post_data['filter_type'] == 3) {
            $html = $this->filterByEmail($post_data['email']);
        } else if ($post_data['filter_type'] == 4) {
            $html = $this->filterByDateJoined($post_data['date_joined']);
        } else if ($post_data['filter_type'] == 5) {
            $html = $this->filterByTypeOfUserAccount($post_data['account_type']);
        } else if ($post_data['filter_type'] == 6) {
            $html = $this->filterByBadge();
        } else if ($post_data['filter_type'] == 7) {
            $html = $this->filterSuspendedUsers();
        } else if ($post_data['filter_type'] == 8) {
            $html = $this->filterMostActiveUsersByPosts();
        } else if ($post_data['filter_type'] == 9) {
            $html = $this->filterMostActiveUsersByComments();
        } else if ($post_data['filter_type'] == 10) {
            $html = $this->filterUsersByNoOfGroupsJoined('DESC');
        } else if ($post_data['filter_type'] ==11) {
            $html = $this->filterUsersByNoOfGroupsJoined('ASC');
        } else if ($post_data['filter_type'] == 12) {
            $html = $this->filterUsersByNoOfGroupsCreated('DESC');
        } else if ($post_data['filter_type'] == 13) {
            $html = $this->filterUsersByNoOfGroupsCreated('ASC');
        }
        echo $html;
        exit();
    }

    private function filterByCity($city_id)
    {
        $groups = $this->User_report_model->filterByCity($city_id);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found in this city</span>';
        }

        return $group_html;
    }

    private function filterByName($name)
    {
        $groups = $this->User_report_model->filterByName($name);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                    <th>User City</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                    <td>' . $group['UserCity'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found with this name</span>';
        }

        return $group_html;
    }

    private function filterByEmail($email)
    {
        $groups = $this->User_report_model->filterByEmail($email);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                    <th>User City</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                    <td>' . $group['UserCity'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found with this email</span>';
        }

        return $group_html;
    }

    private function filterByDateJoined($date_joined)
    {
        $groups = $this->User_report_model->filterByDateJoined($date_joined);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                    <th>User City</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                    <td>' . $group['UserCity'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found with this search criteria</span>';
        }

        return $group_html;
    }

    private function filterByTypeOfUserAccount($account_type)
    {
        $groups = $this->User_report_model->filterByTypeOfUserAccount($account_type);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                    <th>User City</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                    <td>' . $group['UserCity'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found with this search criteria</span>';
        }

        return $group_html;
    }

    private function filterByBadge()
    {
        $groups = $this->User_report_model->filterByBadge();
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                    <th>User City</th>
                                    <th>Badges Count</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                    <td>' . $group['UserCity'] . '</td>
                                    <td style="color: red;">' . $group['user_badges_count'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found with this search criteria</span>';
        }

        return $group_html;
    }

    private function filterSuspendedUsers()
    {
        $groups = $this->User_report_model->filterSuspendedUsers();
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                    <th>User City</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                    <td>' . $group['UserCity'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found with this search criteria</span>';
        }

        return $group_html;
    }

    private function filterMostActiveUsersByPosts()
    {
        $groups = $this->User_report_model->filterMostActiveUsersByPosts();
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                    <th>User City</th>
                                    <th>User Posts Count</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                    <td>' . $group['UserCity'] . '</td>
                                    <td style="color: red;">' . $group['user_posts_count'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found with this search criteria</span>';
        }

        return $group_html;
    }

    private function filterMostActiveUsersByComments()
    {
        $groups = $this->User_report_model->filterMostActiveUsersByComments();
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                    <th>User City</th>
                                    <th>User Comments Count</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                    <td>' . $group['UserCity'] . '</td>
                                    <td style="color: red;">' . $group['user_comments_count'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found with this search criteria</span>';
        }

        return $group_html;
    }

    private function filterUsersByNoOfGroupsJoined($order_by)
    {
        $groups = $this->User_report_model->filterUsersByNoOfGroupsJoined($order_by);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                    <th>User City</th>
                                    <th>Groups Joined</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                    <td>' . $group['UserCity'] . '</td>
                                    <td style="color: red;">' . $group['user_joined_groups_count'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found with this search criteria</span>';
        }

        return $group_html;
    }

    private function filterUsersByNoOfGroupsCreated($order_by)
    {
        $groups = $this->User_report_model->filterUsersByNoOfGroupsCreated($order_by);
        if ($groups) {
            $group_html = '<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Gender</th>
                                    <th>Account Type</th>
                                    <th>User City</th>
                                    <th>Groups Created</th>
                                </tr>
                                </thead>
                                <tbody>';
            $i = 1;
            foreach ($groups as $group) {
                $group_html .= '<tr>
                                    <td>' . $i . '</td>
                                    <td>' . $group['FullName'] . '</td>
                                    <td>' . $group['Email'] . '</td>
                                    <td>' . $group['Gender'] . '</td>
                                    <td>' . $group['AccountType'] . '</td>
                                    <td>' . $group['UserCity'] . '</td>
                                    <td style="color: red;">' . $group['user_created_groups_count'] . '</td>
                                </tr>';
                $i++;
            }

            $group_html .= '</tbody></table>';
        } else {
            $group_html = '<span style="text-align: center;color: red;" id="error-message">No users found with this search criteria</span>';
        }

        return $group_html;
    }


}