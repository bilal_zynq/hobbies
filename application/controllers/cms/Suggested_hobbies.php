<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suggested_hobbies extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model'
        ]);

                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['TableKey'] = 'SuggestionID';
                $this->data['Table'] = 'suggested_hobbies';
    }
    
    public function index()
    {
          $parent                             = $this->data['Parent_model'];
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        
          $this->data['results'] = $this->$parent->getSuggestedHobbies();
          
          $this->load->view('backend/layouts/default',$this->data);
    }

    public function view($id = '')
    {
        if (!checkUserRightAccess(52, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getSuggestedHobbies($id);

        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/view';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;
        }
    }

    private function update()
    {
        if (!checkUserRightAccess(52, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $result = $this->$parent->getSuggestedHobbies($post_data['SuggestionID']);
        if (isset($post_data[$this->data['TableKey']])) {
            $this->$parent->update(array('Status' => $post_data['Status']), array('SuggestionID' => $post_data['SuggestionID']));
            if ($result[0]->Status !== $post_data['Status'])
            {
                // saving notification
                $data['Type'] = 'suggest_hobby';
                $data['LoggedInUserID'] = $result[0]->UserID; // the person whose post is this, who will receive this notification
                $data['UserID'] = 0; // the person who is making this notification
                $data['GroupID'] = 0;
                $data['PostID'] = 0;
                $data['PostCommentID'] = 0;
                $data['NotificationTypeID'] = ($post_data['Status'] == 'Approved' ? 6 : 7); // 6 approved, 7 rejected
                $data['CreatedAt'] = time();
                log_notification($data);
                $message = 'You suggestion for hobby '.strtoupper($result[0]->HobbyName).' is '.strtolower($post_data['Status']);
                $res = sendNotification('Hobbies', $message, $data, $result[0]->UserID);
            }
            $success['error'] = false;
            $success['success'] = lang('update_successfully');
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }
    
    private function delete(){
        
         if(!checkUserRightAccess(52,$this->session->userdata['admin']['UserID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$parent->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    
    
    

}