<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->data['site_setting'] = $this->getSiteSetting();

    }

    public function index()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));

        }

        redirect(base_url('cms/account/login'));
    }

    //User Login
    public function login()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));

        }
        $this->data['view'] = 'backend/login';
        $this->load->view('backend/login', $this->data);

    }


    public function checkLogin()
    {
        $data = array();
        $post_data = $this->input->post();
        $this->loginValidation();
        $checkUser = $this->checkUser($post_data);

        if ($checkUser != true) {
            $data = array();
            $data['success'] = 'false';
            $data['error'] = 'Email or password incorrect.';
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data['success'] = 'Login Successfully';
            $data['error'] = false;
            $data['redirect'] = true;
            $data['url'] = 'cms/dashboard';
            echo json_encode($data);
            exit();
        }
    }

    private function loginValidation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Email', 'Email', 'required');
        $this->form_validation->set_rules('Password', 'Password', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    private function checkUser($post_data)
    {
        $post_data['Password'] = md5($post_data['Password']);

        $user = $this->User_model->getUserData($post_data, $this->language);

        if (!empty($user)) {

            /*if($user->RoleID != '1')
            {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = 'You can\'t be login from here';
                echo json_encode($data);
                exit();

            }
            $user = (array)$user;*/
            $this->session->set_userdata('admin', $user);
            //$this->updateUserLoginStatus();
            return true;
        } else {
            return false;
        }

    }


    //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $user = $this->session->userdata('admin');
        $this->session->unset_userdata('admin');
        redirect($this->config->item('base_url') . 'cms/account/login');

    }
}