<?php
Class Post_like_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("post_likes");

    }

    public function postLikes($post_id)
    {
        $this->db->select('users_text.FullName,users_text.Designation,users_text.Industry,post_likes.*');
        $this->db->from('post_likes');
        $this->db->join('users_text', 'post_likes.UserID = users_text.UserID');
        $this->db->where('post_likes.PostID', $post_id);
        $this->db->order_by('post_likes.PostLikeID', 'ASC');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }else{
            return false;
        }
    }


}