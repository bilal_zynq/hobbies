<?php
Class Report_comment_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("reported_comments");
    }
    
    
    
     public function getReportedComment($system_language_code = 'EN'){
        $this->db->select('pu.FullName as CommentBy,pu.UserID as CommentByUserID,post_comments.Comment,reported_comments.PostCommentID,reported_comments.CommentReportReason,reported_comments.ReportedCommentID,users_text.FullName, users.CompressedImage, users_text.Designation, users_text.Industry, users_text.City, posts.*, posts_text.PostText');
        $this->db->from('posts');
        $this->db->join('posts_text', 'posts.PostID = posts_text.PostID', 'LEFT');
        
        $this->db->join('reported_comments', 'posts.PostID = reported_comments.PostID');
        $this->db->join('post_comments', 'post_comments.PostCommentID = reported_comments.PostCommentID');
        $this->db->join('users_text pu', 'pu.UserID = post_comments.UserID');
        
        $this->db->join('users', 'reported_comments.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


      
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }


       
        $this->db->order_by('posts.CreatedAt', 'DESC');
        
        return $this->db->get()->result();
    }
    


}