<?php
Class Post_comment_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("post_comments");

    }

    public function postComments($post_id, $start = false, $limit = false, $post_comment_id = false)
    {
        $this->db->select('users.Image,users.CompressedImage,users_text.FullName,users_text.Designation,users_text.Industry,post_comments.*');
        $this->db->from('post_comments');
        $this->db->join('users', 'post_comments.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
          $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        $this->db->where('post_comments.PostID', $post_id);
        if ($post_comment_id)
        {
            $this->db->where('post_comments.PostCommentID', $post_comment_id);
        }
        $this->db->order_by('post_comments.CreatedAt', 'ASC');
        if ($start && $limit)
        {
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }else{
            return array();
        }
    }


}