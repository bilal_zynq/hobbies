<?php

Class User_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("users");

    }


    /*
    
    public function getUserData($data,$system_language_code = false){
        
        $this->db->select('users.*,users_text.FullName');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
        $this->db->where('users.Email',$data['Email']);
        $this->db->where('users.Password',$data['Password']);
         
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
       
        
        
    }
    */

    public function getUserData($data, $system_language_code = false)
    {

        $this->db->select('users.*,users_text.FullName,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('roles', 'roles.RoleID = users.RoleID', 'Left');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
        // $this->db->where($where);
        $this->db->where('users.Email', $data['Email']);
        //$this->db->or_where('users.UName',$data['Email']);
        $this->db->where('users.Password', $data['Password']);

        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        return $this->db->get()->row_array();


    }
    
    public function getUserInfoForComment($user_id, $system_language_code = false)
    {

        $this->db->select('users.UserID,users_text.FullName');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        //$where = '(users.Email OR users.UName = "'.$data['Email'].'") AND users.Password = "'.$data['Password'].'"';
        // $this->db->where($where);
        $this->db->where('users.UserID', $user_id);
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        return $this->db->get()->row_array();


    }


    public function getPeopleYouKnow($user_array, $start, $limit, $system_language_code = 'EN')
    {


        $this->db->select('users.UserID,users_text.FullName,users_text.Designation,users_text.City,users_text.Industry,users_text.Bio,users.Image,users.CompressedImage');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        $where = '(users.UserID != ' . $user_array['UserID'] . ' AND users.RoleID = 2)';


        $this->db->where($where);

        $like = '(';
        if ($user_array['Designation'] != '') {
            $like .= 'users_text.Designation LIKE "%' . $user_array['Designation'] . '%"';
        }

        if ($user_array['Industry'] != '') {
            if ($like == '(') {
                $like .= 'users_text.Industry LIKE "%' . $user_array['Industry'] . '%"';
            } else {
                $like .= 'OR users_text.Industry LIKE "%' . $user_array['Industry'] . '%"';
            }
        }

        if ($user_array['City'] != '') {
            if ($like == '(') {
                $like .= 'users_text.City LIKE "%' . $user_array['City'] . '%"';
            } else {
                $like .= 'OR users_text.City LIKE "%' . $user_array['City'] . '%"';
            }
        }
        $like .= ')';

        $this->db->where($like);
        //  $this->db->or_like('users_text.Industry',$user_array['Industry'],'both');
        // $this->db->or_like('users_text.City',$user_array['City'],'both');


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }

        $this->db->limit($limit, $start);
        return $this->db->get()->result_array();
        //$this->db->get()->result_array();
        //echo $this->db->last_query();exit();
        // echo $this->db->last_query($where);exit;

    }

    public function searchFriends($user_id, $text, $system_language_code = 'EN')
    {
        $this->db->select('users.UserID,users_text.FullName,users_text.Designation,users_text.City,users_text.Industry,users_text.Bio,users.Image,users.CompressedImage');
        $this->db->from('users');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');

        $where = '(users.UserID != ' . $user_id . ' AND users.RoleID = 2)';
        $this->db->where($where);
        $like = '(';
        $like .= 'users_text.FullName LIKE "%' . $text . '%" OR users_text.Designation LIKE "%' . $text . '%" OR users_text.Industry LIKE "%' . $text . '%" OR users_text.City LIKE "%' . $text . '%"';
        $like .= ')';
        $this->db->where($like);
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        return $this->db->get()->result_array();
    }

}

?>