<?php

Class Group_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("groups");

    }


    public function getGroupsDate($where, $start = 0, $limit = 10, $system_language_code = false)
    {
        $this->db->select('groups.GroupID,groups.UserID,users.Image,users.CompressedImage,users_text.FullName,groups_text.Title,groups_text.Description,groups.CategoryID,groups.SubCategoryID,groups.GroupImage,groups.GroupCompressImage,groups.CreatedAt,users_text.FullName as GroupOnwerFullName,CPT.Title as CategoryTitle,CCT.Title as SubCategoryTitle,groups.GroupCityID,cities_text.Title as GroupCityTitle');
        $this->db->from('groups');
        $this->db->join('groups_text', 'groups.GroupID = groups_text.GroupID');
        $this->db->join('users', 'groups.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');

        $this->db->join('cities', 'groups.GroupCityID = cities.CityID', 'left');
        $this->db->join('cities_text', 'cities.CityID = cities_text.CityID');

        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


        $this->db->join('categories CP', 'CP.CategoryID = groups.CategoryID', 'left');
        $this->db->join('categories_text CPT', 'CP.CategoryID = CPT.CategoryID AND CPT.SystemLanguageID = 1');

        $this->db->join('categories CC', 'CC.CategoryID = groups.SubCategoryID', 'left');
        $this->db->join('categories_text CCT', 'CC.CategoryID = CCT.CategoryID  AND CCT.SystemLanguageID = 1');


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }

        if ($where) {
            $this->db->where($where);
        }

        $this->db->group_by('groups.GroupID');

        /*if ($start && $limit)
        {
            $this->db->limit($limit, $start);
        }*/

        $this->db->limit($limit, $start);

        $result = $this->db->get();

        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }else{
            return array();
        }

    }

    public function getTaskDetail($task_id, $employee_id)
    {
        $base_url = base_url();
        $sql = "SELECT t.id as task_id, p.id as project_id, p.title as project_title, t.deadline as task_deadline, t.title as task_title, t.status as task_status, t.description as task_description, e.id as responsible_person_id, CONCAT(e.first_name,' ',e.last_name) as responsible_person_name, CONCAT('$base_url',e.image) as responsible_person_image, (SELECT count(*) FROM comments c JOIN tasks ta ON c.task_id = ta.id) as task_comments_count, (SELECT count(*) FROM task_checklist tc JOIN tasks ta1 ON tc.task_id = ta1.id AND tc.employee_id = $employee_id) as checklist_count FROM tasks t JOIN task_assignees ta ON t.id = ta.task_id JOIN projects p ON t.project_id = p.id JOIN employees e ON ta.employee_id = e.id AND ta.task_id = $task_id GROUP BY t.id";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getGroupPosts()
    {
        $sql = "SELECT groups_text.GroupID, groups_text.Title, COUNT(posts.PostID) AS group_posts_count FROM groups_text LEFT JOIN posts ON groups_text.GroupID = posts.GroupID GROUP BY groups_text.GroupID ORDER BY group_posts_count DESC LIMIT 10";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function getGroupMembers()
    {
        $sql = "SELECT groups_text.GroupID, groups_text.Title, COUNT(group_members.UserID) AS group_members_count FROM groups_text LEFT JOIN group_members ON groups_text.GroupID = group_members.GroupID GROUP BY groups_text.GroupID ORDER BY group_members_count DESC LIMIT 10";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function searchGroup($where, $start, $limit)
    {
        $sql = "SELECT * FROM groups JOIN groups_text ON groups.GroupID = groups_text.GroupID $where LIMIT $start, $limit";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }


}