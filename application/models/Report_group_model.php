<?php

Class Report_group_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("report_groups");

    }
    
    
    
    
     public function getReportedGroups($system_language_code = 'EN'){
        $this->db->select('users_text.FullName, users.CompressedImage, users_text.Designation, users_text.Industry, users_text.City, groups.*, groups_text.Title,groups_text.Description');
        $this->db->from('groups');
        $this->db->join('groups_text', 'groups.GroupID = groups_text.GroupID', 'LEFT');
        
        
        
        $this->db->join('users', 'groups.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


      
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }


       
       
        
        return $this->db->get()->result();
    }
    



}

?>