<?php

Class Badge_request_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("badge_requests");

    }

    public function getAllRequests($id = '')
    {
        $this->db->select('br.BadgeRequestID, br.UserID, ut.FullName, bt.Title, bt.Description, br.Status, b.Image');
        $this->db->from('badge_requests br');
        $this->db->join('users_text ut', 'br.UserID = ut.UserID', 'LEFT');
        $this->db->join('badges b', 'br.BadgeID = b.BadgeID', 'LEFT');
        $this->db->join('badges_text bt', 'b.BadgeID = bt.BadgeID', 'LEFT');
        if ($id > 0)
        {
            $this->db->where('br.BadgeRequestID',$id);
        }
        $this->db->order_by('br.BadgeRequestID', 'DESC');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        }else{
            return false;
        }
    }

    public function getUserApprovedBadges($user_id)
    {
        $this->db->select('bt.Title, b.Image');
        $this->db->from('badge_requests br');
        $this->db->join('users_text ut', 'br.UserID = ut.UserID', 'LEFT');
        $this->db->join('badges b', 'br.BadgeID = b.BadgeID', 'LEFT');
        $this->db->join('badges_text bt', 'b.BadgeID = bt.BadgeID', 'LEFT');
        $this->db->where('br.Status','Approved');
        $this->db->where('br.UserID',$user_id);
        $this->db->order_by('br.BadgeRequestID', 'ASC');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }else{
            return array();
        }
    }

}