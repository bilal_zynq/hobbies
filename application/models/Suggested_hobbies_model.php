<?php

Class Suggested_hobbies_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("suggested_hobbies");

    }

    public function getSuggestedHobbies($id = '')
    {
        $this->db->select('sh.*, ut.FullName, ct.Title');
        $this->db->from('suggested_hobbies sh');
        $this->db->join('users_text ut', 'sh.UserID = ut.UserID');
        $this->db->join('categories_text ct', 'sh.CategoryID = ct.CategoryID');
        if ($id > 0)
        {
            $this->db->where('sh.SuggestionID',$id);
        }
        $this->db->order_by('sh.SuggestionID', 'DESC');
        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result();
        }else{
            return array();
        }
    }

}