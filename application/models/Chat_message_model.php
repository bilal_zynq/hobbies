<?php

Class Chat_message_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("chat_messages");
    }

    public function getChatMessages($sender_id, $receiver_id)
    {
        /*$sql = "SELECT cm.*, c.*, su.FullName as SenderName, ru.FullName as ReceiverName FROM chat_messages cm JOIN chats c ON cm.ChatID = c.ChatID JOIN users_text su ON cm.SenderID = su.UserID LEFT JOIN users_text ru ON cm.ReceiverID = ru.UserID WHERE ((cm.SenderID = $sender_id AND cm.ReceiverID = $receiver_id) OR (cm.SenderID = $receiver_id AND cm.ReceiverID = $sender_id)) ORDER BY cm.CreatedAt ASC LIMIT $start, $limit";*/
        $sql = "SELECT cm.*, c.*, su.UserID as SenderUserID, ru.UserID as ReceiverUserID, su.FullName as SenderName, ru.FullName as ReceiverName, us.CompressedImage as SenderImage, ur.CompressedImage as ReceiverImage FROM chat_messages cm JOIN chats c ON cm.ChatID = c.ChatID LEFT JOIN users_text su ON cm.SenderID = su.UserID LEFT JOIN users_text ru ON cm.ReceiverID = ru.UserID JOIN users us ON su.UserID = us.UserID JOIN users ur ON ru.UserID = ur.UserID WHERE ((cm.SenderID = $sender_id AND cm.ReceiverID = $receiver_id) OR (cm.SenderID = $receiver_id AND cm.ReceiverID = $sender_id)) ORDER BY cm.ChatMessageID ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
        /* $this->db->select('cm.*, c.*, su.FullName as SenderName, ru.FullName as ReceiverName');
         $this->db->from('chat_messages cm');
         $this->db->join('chats c', 'cm.ChatID = c.ChatID');
         $this->db->join('users_text su', 'cm.SenderID = su.UserID');
         $this->db->join('users_text ru', 'cm.ReceiverID = ru.UserID');
         $this->db->where('(cm.SenderID = '.$sender_id.' AND cm.ReceiverID = '.$receiver_id.') OR (cm.SenderID = '.$receiver_id.' AND cm.ReceiverID = '.$sender_id.')');
         $this->db->order_by('cm.CreatedAt', 'DESC');
         $this->db->limit($limit, $start);
         $result = $this->db->get();
         echo $this->db->last_query();exit();
         if ($result->num_rows() > 0)
         {
             return $result->result_array();
         }else{
             return array();
         }*/
    }

    public function getMessageDetail($message_id)
    {
        $sql = "SELECT cm.*, c.*, su.UserID as SenderUserID, ru.UserID as ReceiverUserID, su.FullName as SenderName, ru.FullName as ReceiverName, us.CompressedImage as SenderImage, ur.CompressedImage as ReceiverImage FROM chat_messages cm JOIN chats c ON cm.ChatID = c.ChatID LEFT JOIN users_text su ON cm.SenderID = su.UserID LEFT JOIN users_text ru ON cm.ReceiverID = ru.UserID JOIN users us ON su.UserID = us.UserID JOIN users ur ON ru.UserID = ur.UserID WHERE cm.ChatMessageID = $message_id";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getLastMessageForThisChatRoom($chatRoomID)
    {
        $this->db->select('cm.*, su.UserID as SenderUserID, ru.UserID as ReceiverUserID, sut.FullName as SenderName, rut.FullName as ReceiverName, su.CompressedImage as SenderImage, ru.CompressedImage as ReceiverImage');
        $this->db->from('chat_messages cm');
        $this->db->join('users su', 'cm.SenderID = su.UserID');
        $this->db->join('users ru', 'cm.ReceiverID = ru.UserID');
        $this->db->join('users_text sut', 'cm.SenderID = sut.UserID');
        $this->db->join('users_text rut', 'cm.ReceiverID = rut.UserID');
        $this->db->where('cm.ChatID', $chatRoomID);
        $this->db->order_by('cm.CreatedAt', 'DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return array();
        }

    }

    public function getUnreadMsgCountForChat($chat_id)
    {
        $sql = "SELECT cm.*, c.*, su.UserID as SenderUserID, ru.UserID as ReceiverUserID, su.FullName as SenderName, ru.FullName as ReceiverName 
                FROM chat_messages cm
                JOIN chats c ON cm.ChatID = c.ChatID 
                JOIN users_text su ON cm.SenderID = su.UserID 
                LEFT JOIN users_text ru ON cm.ReceiverID = ru.UserID 
                WHERE cm.ChatID = $chat_id AND cm.IsRead = 'no'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

}