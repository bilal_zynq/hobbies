<?php
Class Post_report_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("posts_reported");
    }
    
    
    
    public function getReportedPost($system_language_code = 'EN'){
        $this->db->select('posts_reported.ReportReason,posts_reported.PostReportID,users_text.FullName, users.CompressedImage, users_text.Designation, users_text.Industry, users_text.City, posts.*, posts_text.PostText');
        $this->db->from('posts');
        $this->db->join('posts_text', 'posts.PostID = posts_text.PostID', 'LEFT');
        $this->db->join('posts_reported', 'posts.PostID = posts_reported.PostID');
        $this->db->join('users', 'posts_reported.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


      
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }


       
        $this->db->order_by('posts.CreatedAt', 'DESC');
        
        return $this->db->get()->result();
    }


}