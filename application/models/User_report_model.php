<?php

Class User_report_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function filterByCity($city_id)
    {
        $sql = "SELECT users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType
          FROM users
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN types ON users.AccountTypeID = types.TypeID
          JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
          LEFT JOIN cities ON users.CityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          WHERE system_languages.IsDefault = '1' AND users.CityID = $city_id AND users.RoleID = 2";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterByName($name)
    {
        $sql = "SELECT users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType
          FROM users
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN types ON users.AccountTypeID = types.TypeID
          JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
          LEFT JOIN cities ON users.CityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          WHERE system_languages.IsDefault = '1' AND users_text.FullName LIKE '%$name%' AND users.RoleID = 2";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterByEmail($email)
    {
        $sql = "SELECT users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType
          FROM users
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN types ON users.AccountTypeID = types.TypeID
          JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
          LEFT JOIN cities ON users.CityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          WHERE system_languages.IsDefault = '1' AND users.Email LIKE '%$email%' AND users.RoleID = 2";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterByDateJoined($date)
    {
        $sql = "SELECT users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType
          FROM users
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN types ON users.AccountTypeID = types.TypeID
          JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
          LEFT JOIN cities ON users.CityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          WHERE system_languages.IsDefault = '1' AND DATE_FORMAT(FROM_UNIXTIME(users.CreatedAt), '%d-%m-%Y') = '$date' AND users.RoleID = 2";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterByTypeOfUserAccount($account_type)
    {
        $sql = "SELECT users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType
          FROM users
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN types ON users.AccountTypeID = types.TypeID
          JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
          LEFT JOIN cities ON users.CityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          WHERE system_languages.IsDefault = '1' AND users.AccountTypeID = $account_type AND users.RoleID = 2";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterByBadge()
    {
        $sql = "SELECT users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType, COUNT(badge_requests.BadgeRequestID) AS user_badges_count
          FROM badge_requests
          LEFT JOIN users ON badge_requests.UserID = users.UserID
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN types ON users.AccountTypeID = types.TypeID
          JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
          LEFT JOIN cities ON users.CityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          WHERE system_languages.IsDefault = '1' AND badge_requests.Status = 'Approved' 
          GROUP BY badge_requests.UserID ORDER BY user_badges_count DESC";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterSuspendedUsers()
    {
        $sql = "SELECT users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType
          FROM users
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN types ON users.AccountTypeID = types.TypeID
          JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
          LEFT JOIN cities ON users.CityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          WHERE system_languages.IsDefault = '1' AND users.IsActive = 0 AND users.RoleID = 2";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterMostActiveUsersByPosts()
    {
        $sql = "SELECT users.UserID, users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType, COUNT(posts.PostID) AS user_posts_count
                  FROM posts
                  LEFT JOIN users ON posts.UserID = users.UserID
                  JOIN users_text ON users.UserID = users_text.UserID
                  LEFT JOIN types ON users.AccountTypeID = types.TypeID
                  JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
                  LEFT JOIN cities ON users.CityID = cities.CityID
                  JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
                  JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
                  WHERE system_languages.IsDefault = '1'  
                  GROUP BY posts.UserID 
                  ORDER BY user_posts_count DESC";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterMostActiveUsersByComments()
    {
        $sql = "SELECT users.UserID, users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType, COUNT(post_comments.PostCommentID) AS user_comments_count
                  FROM post_comments
                  LEFT JOIN users ON post_comments.UserID = users.UserID
                  JOIN users_text ON users.UserID = users_text.UserID
                  LEFT JOIN types ON users.AccountTypeID = types.TypeID
                  JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
                  LEFT JOIN cities ON users.CityID = cities.CityID
                  JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
                  JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
                  WHERE system_languages.IsDefault = '1'  
                  GROUP BY post_comments.UserID 
                  ORDER BY user_comments_count DESC";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterUsersByNoOfGroupsJoined($order_by)
    {
        $sql = "SELECT users.UserID, users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType, COUNT(group_members.GroupMemberID) AS user_joined_groups_count
                  FROM group_members
                  LEFT JOIN users ON group_members.UserID = users.UserID
                  JOIN users_text ON users.UserID = users_text.UserID
                  LEFT JOIN types ON users.AccountTypeID = types.TypeID
                  JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
                  LEFT JOIN cities ON users.CityID = cities.CityID
                  JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
                  JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
                  WHERE system_languages.IsDefault = '1'  
                  GROUP BY group_members.UserID 
                  ORDER BY user_joined_groups_count $order_by";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterUsersByNoOfGroupsCreated($order_by)
    {
        $sql = "SELECT users.UserID, users.Gender,users.Email, users_text.FullName, cities_text.Title as UserCity, types_text.Title as AccountType, COUNT(groups.GroupID) AS user_created_groups_count
                  FROM groups
                  LEFT JOIN users ON groups.UserID = users.UserID
                  JOIN users_text ON users.UserID = users_text.UserID
                  LEFT JOIN types ON users.AccountTypeID = types.TypeID
                  JOIN types_text ON types.TypeID = types_text.TypeID AND types_text.SystemLanguageID = 1
                  LEFT JOIN cities ON users.CityID = cities.CityID
                  JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
                  JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
                  WHERE system_languages.IsDefault = '1'  
                  GROUP BY groups.UserID 
                  ORDER BY user_created_groups_count $order_by";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterByNoOfMembers($max_no_of_members)
    {
        $sql = "SELECT groups_text.GroupID, users_text.FullName as GroupOnwerFullName, groups_text.Title, COUNT(group_members.UserID) AS group_members_count, CPT.Title as CategoryTitle, CCT.Title as SubCategoryTitle, cities_text.Title as GroupCityTitle
              FROM groups
              JOIN groups_text ON groups.GroupID = groups_text.GroupID 
              LEFT JOIN group_members ON groups_text.GroupID = group_members.GroupID 
              LEFT JOIN categories CP ON CP.CategoryID = groups.CategoryID
              JOIN categories_text CPT ON CP.CategoryID = CPT.CategoryID AND CPT.SystemLanguageID = 1
              LEFT JOIN categories CC ON CC.CategoryID = groups.SubCategoryID
              JOIN categories_text CCT ON CC.CategoryID = CCT.CategoryID AND CCT.SystemLanguageID = 1
              LEFT JOIN cities ON groups.GroupCityID = cities.CityID
              JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
              JOIN users ON groups.UserID = users.UserID
              JOIN users_text ON users.UserID = users_text.UserID
              GROUP BY groups_text.GroupID HAVING group_members_count <= $max_no_of_members ORDER BY group_members_count DESC";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function sortByLastActivity($order_by = 'DESC')
    {
        $sql = "SELECT groups_text.GroupID, groups_text.Title, users_text.FullName as GroupOnwerFullName, CPT.Title as CategoryTitle, CCT.Title as SubCategoryTitle, cities_text.Title as GroupCityTitle, groups.LastActivityAt
          FROM groups
          JOIN groups_text ON groups.GroupID = groups_text.GroupID
          JOIN users ON groups.UserID = users.UserID
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN cities ON groups.GroupCityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          LEFT JOIN categories CP ON CP.CategoryID = groups.CategoryID
          JOIN categories_text CPT ON CP.CategoryID = CPT.CategoryID AND CPT.SystemLanguageID = 1
          LEFT JOIN categories CC ON CC.CategoryID = groups.SubCategoryID
          JOIN categories_text CCT ON CC.CategoryID = CCT.CategoryID AND CCT.SystemLanguageID = 1
          WHERE system_languages.IsDefault = '1' GROUP BY groups.GroupID ORDER BY groups.LastActivityAt $order_by";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function filterByCreatedInSpecificPeriod($from_date, $to_date)
    {
        // converting to timestamps
        $from_date = strtotime($from_date);
        $to_date = strtotime($to_date);

        $sql = "SELECT groups_text.GroupID, groups_text.Title, users_text.FullName as GroupOnwerFullName, CPT.Title as CategoryTitle, CCT.Title as SubCategoryTitle, cities_text.Title as GroupCityTitle
          FROM groups
          JOIN groups_text ON groups.GroupID = groups_text.GroupID
          JOIN users ON groups.UserID = users.UserID
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN cities ON groups.GroupCityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          LEFT JOIN categories CP ON CP.CategoryID = groups.CategoryID
          JOIN categories_text CPT ON CP.CategoryID = CPT.CategoryID AND CPT.SystemLanguageID = 1
          LEFT JOIN categories CC ON CC.CategoryID = groups.SubCategoryID
          JOIN categories_text CCT ON CC.CategoryID = CCT.CategoryID AND CCT.SystemLanguageID = 1
          WHERE system_languages.IsDefault = '1' AND groups.CreatedAt BETWEEN FROM_UNIXTIME($from_date) AND FROM_UNIXTIME($to_date) GROUP BY groups.GroupID";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function createdOnSPecificDate($date)
    {
        $sql = "SELECT groups_text.GroupID, groups_text.Title, users_text.FullName as GroupOnwerFullName, CPT.Title as CategoryTitle, CCT.Title as SubCategoryTitle, cities_text.Title as GroupCityTitle
          FROM groups
          JOIN groups_text ON groups.GroupID = groups_text.GroupID
          JOIN users ON groups.UserID = users.UserID
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN cities ON groups.GroupCityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          LEFT JOIN categories CP ON CP.CategoryID = groups.CategoryID
          JOIN categories_text CPT ON CP.CategoryID = CPT.CategoryID AND CPT.SystemLanguageID = 1
          LEFT JOIN categories CC ON CC.CategoryID = groups.SubCategoryID
          JOIN categories_text CCT ON CC.CategoryID = CCT.CategoryID AND CCT.SystemLanguageID = 1
          WHERE system_languages.IsDefault = '1' AND DATE_FORMAT(FROM_UNIXTIME(groups.CreatedAt), '%d-%m-%Y') = '$date' GROUP BY groups.GroupID";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function suspendedGroups()
    {
        $sql = "SELECT groups_text.GroupID, groups_text.Title, users_text.FullName as GroupOnwerFullName, CPT.Title as CategoryTitle, CCT.Title as SubCategoryTitle, cities_text.Title as GroupCityTitle
          FROM groups
          JOIN groups_text ON groups.GroupID = groups_text.GroupID
          JOIN users ON groups.UserID = users.UserID
          JOIN users_text ON users.UserID = users_text.UserID
          LEFT JOIN cities ON groups.GroupCityID = cities.CityID
          JOIN cities_text ON cities.CityID = cities_text.CityID AND cities_text.SystemLanguageID = 1
          JOIN system_languages ON system_languages.SystemLanguageID = users_text.SystemLanguageID
          LEFT JOIN categories CP ON CP.CategoryID = groups.CategoryID
          JOIN categories_text CPT ON CP.CategoryID = CPT.CategoryID AND CPT.SystemLanguageID = 1
          LEFT JOIN categories CC ON CC.CategoryID = groups.SubCategoryID
          JOIN categories_text CCT ON CC.CategoryID = CCT.CategoryID AND CCT.SystemLanguageID = 1
          WHERE system_languages.IsDefault = '1' AND groups.IsActive = 0 GROUP BY groups.GroupID";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function reportedGroups()
    {
        $sql = "SELECT report_groups.*, groups_text.Title, COUNT(report_groups.GroupID) AS group_reported_count 
                FROM report_groups 
                JOIN groups_text ON report_groups.GroupID = groups_text.GroupID 
                JOIN groups ON groups_text.GroupID = groups.GroupID 
                GROUP BY report_groups.GroupID ORDER BY group_reported_count DESC";

        $sql1 = "SELECT report_groups.*, groups_text.Title, users_text.FullName AS GroupOnwerFullName, categories_text.Title as CategoryName, COUNT(report_groups.GroupID) AS group_reported_count 
                  FROM report_groups 
                  LEFT JOIN groups_text ON report_groups.GroupID = groups_text.GroupID 
                  LEFT JOIN groups ON groups_text.GroupID = groups.GroupID 
                  LEFT JOIN users_text ON groups.UserID = users_text.UserID 
                  LEFT JOIN categories ON groups.CategoryID = categories.CategoryID
                  JOIN categories_text ON categories.CategoryID = categories_text.CategoryID AND categories_text.SystemLanguageID = 1 
                  GROUP BY report_groups.GroupID 
                  ORDER BY group_reported_count DESC";

        $query = $this->db->query($sql1);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function noOfAdmins()
    {
        $sql1 = "SELECT groups_text.Title, users_text.FullName AS GroupOnwerFullName, categories_text.Title as CategoryTitle, group_members.GroupID, COUNT(group_members.GroupMemberID) AS group_admins_count 
                  FROM group_members 
                  LEFT JOIN groups_text ON group_members.GroupID = groups_text.GroupID 
                  LEFT JOIN groups ON groups_text.GroupID = groups.GroupID 
                  LEFT JOIN users_text ON groups.UserID = users_text.UserID 
                  LEFT JOIN categories ON groups.CategoryID = categories.CategoryID
                  JOIN categories_text ON categories.CategoryID = categories_text.CategoryID AND categories_text.SystemLanguageID = 1 
                  WHERE group_members.IsAdmin = 'yes' GROUP BY group_members.GroupID 
                  ORDER BY group_admins_count DESC";

        $query = $this->db->query($sql1);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function topActiveGroups()
    {
        $sql1 = "SELECT users_text.FullName AS GroupOnwerFullName, categories_text.Title as CategoryTitle, groups_text.Title, posts.GroupID, COUNT(posts.PostID) AS group_posts_count 
                  FROM posts 
                  LEFT JOIN groups_text ON posts.GroupID = groups_text.GroupID 
                  LEFT JOIN groups ON groups_text.GroupID = groups.GroupID 
                  LEFT JOIN users_text ON groups.UserID = users_text.UserID 
                  LEFT JOIN categories ON categories.CategoryID = groups.CategoryID
                  JOIN categories_text ON categories.CategoryID = categories_text.CategoryID AND categories_text.SystemLanguageID = 1
                  GROUP BY posts.GroupID 
                  ORDER BY group_posts_count DESC";

        $query = $this->db->query($sql1);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
}