<?php
Class Module_rights_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("modules_rights");

    }
    
    
    
    
    public function getModulesWithRights($role_id,$system_language_code,$where = false){
        
            
            $this->db->select('modules.ModuleID,modules.Slug,modules.IconClass,modules.ParentID,modules_text.Title as ModuleTitle,roles.RoleID,roles_text.Title as RoleTitle,modules_rights.*');
            $this->db->from('modules');
            $this->db->join('modules_text','modules_text.ModuleID = modules.ModuleID');
            $this->db->join('system_languages','system_languages.SystemLanguageID = modules_text.SystemLanguageID' );
            
            $this->db->join('modules_rights','modules.ModuleID = modules_rights.ModuleID');
            $this->db->join('roles','roles.RoleID = modules_rights.RoleID');
            $this->db->join('roles_text','roles.RoleID = roles_text.RoleID');
            
            
            
            if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
            }else
            {
                    $this->db->where('system_languages.IsDefault','1');
            }
            
            $this->db->where('modules.Hide','0');
            $this->db->where('roles.RoleID',$role_id);
            
            
            if($where){
                $this->db->where($where);
            }
            
            $this->db->group_by('modules.ModuleID');
            
            $this->db->order_by('modules.SortOrder','ASC');
            $result = $this->db->get();
           //echo $this->db->last_query();exit;
            return $result->result();
    }

    
}