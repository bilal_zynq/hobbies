<?php

Class Post_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("posts");
    }

    public function postsFeed($user_id, $start, $limit, $system_language_code = 'EN')
    {
        $this->db->select('users_text.FullName,users.CompressedImage, users_text.Designation, users_text.Industry, users_text.City, posts.*, posts_text.PostText');
        $this->db->from('posts');
        $this->db->join('posts_text', 'posts.PostID = posts_text.PostID', 'LEFT');
        $this->db->join('users', 'posts.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


        $this->db->join('user_followers a', '(posts.UserID = a.Following AND posts.PostForAudience = "private" AND a.Follower = ' . $user_id . ') OR (posts.PostForAudience = "public") ');
        // $this->db->join('user_followers b', 'posts.UserID = b.Following AND posts.PostForAudience = "public"','left');
        //$this->db->where('user_followers.Follower', $user_id);


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }


        $this->db->group_by('posts.PostID');
        $this->db->order_by('posts.CreatedAt', 'DESC');
        $this->db->limit($limit, $start);
        return $this->db->get()->result_array();

    }
    
    
     public function postData($post_id, $system_language_code = 'EN')
    {
        $this->db->select('users_text.FullName, users.CompressedImage, users_text.Designation, users_text.Industry, users_text.City, posts.*, posts_text.PostText');
        $this->db->from('posts');
        $this->db->join('posts_text', 'posts.PostID = posts_text.PostID', 'LEFT');
        $this->db->join('users', 'posts.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


       
        


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }


        $this->db->where('posts.PostID',$post_id);
       
        
        return $this->db->get()->row_array();

    }
    

    public function postsFeedReverse($user_id, $last_post_id, $system_language_code = 'EN')
    {
        $this->db->select('users_text.FullName, UCASE(users.Type) as UserType, users.CompressedImage, users_text.Designation, users_text.Industry, users_text.City, posts.*, posts_text.PostText');
        $this->db->from('posts');
        $this->db->join('posts_text', 'posts.PostID = posts_text.PostID', 'LEFT');
        $this->db->join('users', 'posts.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


        $this->db->join('user_followers a', '(posts.UserID = a.Following AND posts.PostForAudience = "private" AND a.Follower = ' . $user_id . ' AND posts.PostID > ' . $last_post_id . ') OR (posts.PostForAudience = "public" AND posts.PostID > ' . $last_post_id . ') ');
        // $this->db->join('user_followers b', 'posts.UserID = b.Following AND posts.PostForAudience = "public"','left');
        //$this->db->where('user_followers.Follower', $user_id);


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }


        $this->db->group_by('posts.PostID');
        $this->db->order_by('posts.CreatedAt', 'DESC');
        return $this->db->get()->result_array();
    }


    public function getGroupPosts($group_id, $start, $limit, $system_language_code = 'EN')
    {
        $this->db->select('users_text.FullName, users.CompressedImage, users_text.Designation, users_text.Industry, users_text.City, posts.*, posts_text.PostText');
        $this->db->from('posts');
        $this->db->join('posts_text', 'posts.PostID = posts_text.PostID', 'LEFT');
        $this->db->join('users', 'posts.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');


        // $this->db->join('user_followers a', '(posts.UserID = a.Following AND posts.PostForAudience = "private" AND a.Follower = ' . $user_id . ') OR (posts.PostForAudience = "public") ');
        // $this->db->join('user_followers b', 'posts.UserID = b.Following AND posts.PostForAudience = "public"','left');
        //$this->db->where('user_followers.Follower', $user_id);


        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }

        $this->db->where('posts.GroupID', $group_id);
        ///$this->db->group_by('posts.PostID');
        $this->db->order_by('posts.PinnedPost', 'DESC');
        $this->db->order_by('posts.CreatedAt', 'DESC');
        $this->db->limit($limit, $start);
        return $this->db->get()->result_array();

    }

	public function postsDetail($post_id, $user_id, $system_language_code = 'EN')
    {
        // $this->db->select("users_text.FullName, users.CompressedImage, users_text.City, posts.*, posts_text.PostText,IF(post_likes.PostLikeID IS NULL, 'no', 'yes') as IsLiked");
        $this->db->select('users_text.FullName, users.CompressedImage, users_text.City, posts.*, posts_text.PostText,IF(post_likes.PostLikeID IS NULL, 0, 1) as IsLiked');
        $this->db->from('posts');
        $this->db->join('posts_text', 'posts.PostID = posts_text.PostID', 'LEFT');
        $this->db->join("post_likes", "posts.PostID = post_likes.PostID AND post_likes.UserID = $user_id", "LEFT");
        $this->db->join('users', 'posts.UserID = users.UserID');
        $this->db->join('users_text', 'users.UserID = users_text.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');
        $this->db->where('posts.PostID', $post_id);

        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }

        $this->db->group_by('posts.PostID');
        $this->db->order_by('posts.CreatedAt', 'DESC');

        $result = $this->db->get();
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }else{
            return array();
        }

    }
}