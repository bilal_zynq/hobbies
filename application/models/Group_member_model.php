<?php

Class Group_member_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("group_members");

    }


    public function getOldestGroupMember($group_id)
    {
        $this->db->select('*');
        $this->db->from('group_members');
        $this->db->where('GroupID', $group_id);
        $this->db->order_by('GroupMemberID', 'ASC');
        $this->db->limit(1);
        return $this->db->get()->row_array();


    }


    public function totalGroupJoined($user_id)
    {
        $this->db->select('COUNT(UserID) as TotalGroup');
        $this->db->from('group_members');
        $this->db->where('UserID', $user_id);
        // $this->db->order_by('GroupMemberID','ASC');
        //$this->db->limit(1);
        return $this->db->get()->row()->TotalGroup;


    }


    public function getGroupInfo($group_id)
    {
        $this->db->select('groups.*,groups_text.Title,users.UserID,users_text.FullName,users.Image,users.CompressedImage,users.CoverImage,users.CompressedCoverImage');
        $this->db->from('groups');
        $this->db->join('groups_text', 'groups.GroupID = groups_text.GroupID');
        $this->db->join('users', 'groups.UserID = users.UserID');
        $this->db->join('users_text', 'users_text.UserID = users.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');
        $this->db->where('groups.GroupID', $group_id);

        return $this->db->get()->row_array();
    }


    public function getGroupMembers($group_id, $start, $limit)
    {
        $this->db->select('group_members.CreatedAt as GroupJoinTime, group_members.IsAdmin,users.UserID,users_text.FullName,users.Image,users.CompressedImage,users.CoverImage,users.CompressedCoverImage');
        $this->db->from('group_members');

        $this->db->join('users', 'group_members.UserID = users.UserID');
        $this->db->join('users_text', 'users_text.UserID = users.UserID');
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');
        $this->db->where('group_members.GroupID', $group_id);
        $this->db->limit($limit, $start);
        return $this->db->get()->result_array();
    }


}

?>