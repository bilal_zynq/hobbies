<?php
Class Chat_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("chats");
    }

    public function getChatRooms($user_id, $start, $limit)
    {
        $sql = "SELECT cm.*, c.* FROM chat_messages cm 
                JOIN chats c ON cm.ChatID = c.ChatID 
                WHERE (cm.SenderID = $user_id OR cm.ReceiverID = $user_id)
                GROUP BY cm.ChatID 
                ORDER BY c.LastActivityAt DESC LIMIT $start, $limit";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
}