<?php

Class User_notification_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_notifications");
    }

    public function getNotifications($user_id, $start, $limit, $language = 'en')
    {
        $this->db->select("user_notifications.*, users_text.FullName as UserName, users.CompressedImage as UserImage, IF ('$language' = 'en', notification_types.NotificationTextEn, notification_types.NotificationTextAr) AS NotificationText, groups_text.Title as GroupName");
        $this->db->from('user_notifications');
        $this->db->join('notification_types', 'user_notifications.NotificationTypeID = notification_types.NotificationTypeID');
        $this->db->join('users', 'user_notifications.UserID = users.UserID', 'LEFT');
        $this->db->join('users_text', 'user_notifications.UserID = users_text.UserID', 'LEFT');
        $this->db->join('groups_text', 'user_notifications.GroupID = groups_text.GroupID', 'LEFT');
        $this->db->where('user_notifications.LoggedInUserID', $user_id);
        $this->db->group_by('user_notifications.UserNotificationID');
        $this->db->order_by('user_notifications.UserNotificationID', 'DESC');
        $this->db->limit($limit, $start);
        $result = $this->db->get();
        // echo $this->db->last_query();exit();
        return $result->result_array();
    }

}