<?php
    Class City_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("cities");

        }


    public function getAllCities($language_code,$district_id,$is_active = false){
        $this->db->select('cities.*,cities_text.Title,districts_text.Title As DistrictTitle');
        $this->db->from('cities');
        $this->db->join('cities_text','cities.CityID = cities_text.CityID');
        $this->db->join('system_languages','system_languages.SystemLanguageID = cities_text.SystemLanguageID');
        $this->db->join('districts','districts.DistrictID = cities.DistrictID','Left');
        $this->db->join('districts_text','districts_text.DistrictID = districts.DistrictID');
        
        $this->db->where('system_languages.ShortCode',$language_code);
        if($is_active){
           $this->db->where('cities.IsActive',$is_active); 
        }
        
        $this->db->where('cities.Hide',0);
        if($district_id){
           $this->db->where('cities.DistrictID',$district_id); 
        }
        return $this->db->get()->result();
        
        
    }
        
        
    }
    
    
    