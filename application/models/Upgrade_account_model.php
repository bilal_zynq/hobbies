<?php
Class Upgrade_account_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("account_upgrade_requests");

    }
    
    
    
    
    public function getAllUpgradedRequest($where = false,$system_language_code = false){
        $this->db->select('account_upgrade_requests.*,users_text.FullName,types_text.Title');
        $this->db->from('account_upgrade_requests');
        
        
        $this->db->join('users','users.UserID = account_upgrade_requests.UserID','left');
        $this->db->join('users_text','users_text.UserID = users.UserID');
        
        $this->db->join('types','types.TypeID = account_upgrade_requests.TypeID','left');
        $this->db->join('types_text','types_text.TypeID = types.TypeID');
        
        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }
        
        $this->db->where($where);
        return $this->db->get()->result();
        
    }

    public function getAllUpgradedRequestNew($where = false,$system_language_code = false){
        $this->db->select('account_upgrade_requests_new.*,users_text.FullName');
        $this->db->from('account_upgrade_requests_new');


        $this->db->join('users','users.UserID = account_upgrade_requests_new.UserID','left');
        $this->db->join('users_text','users_text.UserID = users.UserID');

        $this->db->join('system_languages', 'system_languages.SystemLanguageID = users_text.SystemLanguageID');
        if ($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        } else {
            $this->db->where('system_languages.IsDefault', '1');
        }

        $this->db->where($where);
        return $this->db->get()->result();

    }

    
}